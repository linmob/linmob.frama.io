+++
title = "Push notifications in (mobile) Linux"
date = "2024-03-17T16:00:00Z"
draft = false
[taxonomies]
tags = ["push", "notifications", "battery", "sleep", "Unified Push", "Web Push API", "Ubuntu Touch",]
categories = ["guest post", "software"]
authors = ["darkdragon-001"]
+++

An introduction into push notifications, why we want it on Linux, and an overview over the current options.

<!-- more -->

## What is it?

**push** refers to the service _pushing_ notifications to the client immediately when they available.

**pull** refers to the client regularly _pulling_ the service for newly available notifications.

## Why do we want it?

As communication is a very important feature for (Linux) phones, instant notifications for (web) calls and messages in your favorite apps is usually desired.

## What is the problem?

Technically, push notifications are usually realized by an open TCP connection. As they time out at some point, they need to be refreshed regularly.
When every app creates their own connection, the device wakes up very often and consumes a lot of energy. This renders mobile devices with very limited battery pretty much unusable.

A much more energy saving way to handle this is a single connection which all applications can use for notifications. This allows the device to save a lot of energy as it can stay in sleep mode most of the time.

In the remainder of the post, we will refer to this single common push service.

## What is the current state?

### UnifiedPush

An open standard, [UnifiedPush](https://unifiedpush.org/), is currently evolving. It contains different distributors (for example [ntfy.sh](https://ntfy.sh/) and [NextPush on NextCloud](https://codeberg.org/NextPush/uppush)) and a lot of [Android apps](https://unifiedpush.org/users/apps/) are already using it.

On Linux, the situation does not look as good:
- There has been a short [Mastodon conversation about UnifiedPush on GNOME](https://social.librem.one/@agx/106674637106532626) in mid 2021.
- [Volker Krause presented a concept for push notifications in KDE](https://www.volkerkrause.eu/2022/11/12/kde-unifiedpush-push-notifications.html) in the end of 2022 which resulted in the [KUnifiedPush](https://invent.kde.org/libraries/kunifiedpush) library.

Not much really happened since then.

### WebPush

As one needs internet to communicate anyways, using the mobile websites seems like a viable option. With the [Progressive Web Apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) standard and the [Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) standard, all necessary building blocks are in place.

- **Firefox** [supports WebPush](https://support.mozilla.org/en-US/kb/push-notifications-firefox) but [does not support PWAs](https://bugzilla.mozilla.org/show_bug.cgi?id=1407202).
- **GNOME Web (Epiphany)** [recently added PWA support](https://gitlab.gnome.org/GNOME/epiphany/-/issues/931) but [does not support the Web Push API](https://gitlab.gnome.org/GNOME/epiphany/-/issues/1871) because [WebKit is missing support](https://bugs.webkit.org/show_bug.cgi?id=245190).

### Ubuntu Touch (UBports)

Ubuntu Touch Clickable apps [support push notifications](https://docs.ubports.com/en/latest/appdev/guides/pushnotifications.html). Unfortunately only a very tiny fraction of apps is available in Ubuntu Touch as it is missing Flatpak support (the team is open to add Flatpak, if someone comes up with a good enough implementation, though).

## Where do we go from here?

How can we drive this forward as a community? [Feedback, ideas and comments are welcome - please reply on Mastodon!](https://fosstodon.org/@linmob/112112220441388723)
