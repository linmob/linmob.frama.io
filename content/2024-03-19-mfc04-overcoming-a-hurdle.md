+++
title = "MCF04: Overcoming a hurdle"
date = "2024-03-19T08:27:04Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "firefox", "hacking"]
categories = ["projects"]
authors = ["Peter"]
+++

One of the reasons I decided to not just contribute, but also blog about what I do, is that I tend to forget important details. It happened again, so here's the detail I forgot - it's to not misread an instruction.

<!-- more -->

This is it:
<blockquote>The debugger will only listen on localhost by default. If you know what you are doing, you may set <code>devtools.debugger.force-local</code> to <code>false</code>, so it listens on all interfaces. Otherwise you'll need something like an SSH tunnel.</blockquote>

Future me, this is what that means if you don't want to waste time, but just be set up now: __Set this value__ accordingly, but be aware that this may make things insecure, so better unset it again if this is your daily driver or just don't care if the device is only for development.

### Doing

With this done, I was able to land two MRs:

* [Hide 'Firefox View' in CSS for current Firefox (can't be hidden in about:config on Firefox 123) (!43) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/43)
* [Follow up to MR41: hide identity-permission-box in urlbar (!42) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/42)

The first one is interesting for users on the current release of Firefox, annoyed by the Firefox View button to the left of your tab.

The other one is about the thing that had frustrated me before writing the [previous post in this series](https://linmob.net/mfc03-overdue-failures/), a huge useless button (unless you want to have autoplay on websites) that clutters the URL bar.

Also, I have been going to through two issues (and left comments):

* [HTML date picker calendar view missing 7th column. (#63) · Issues · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/63)
* [Allow opening native link dialog not adaptive (#65) · Issues · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/65)

### Next steps

First, I need to take another look at fenix - [user0 did the awesome thing and fixed the tab switcher of Firefox 123](https://codeberg.org/user0/mobile-config-firefox/commit/f2381fd45eeef10ffb173a56f8106e6d68b981be).

Then, over the following days:

- figure out how to do verified/signed commits,
- go through more issues and see if they persist/are fixed in the fenix branch,
- tackle the one not fixed in fenix and fix them there and upstream,
- prioritize the others and then talk to ollieparanoid whether fixes should be based on master or wip.

See you next time!
