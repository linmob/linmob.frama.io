+++
title = "Weekly GNU-like Mobile Linux Update (6/2023): Work on dozing support for GNOME Settings daemon and more FOSDEM 2023 talk recordings"
draft = false
date = "2023-02-13T23:08:00Z"
[taxonomies]
tags = ["FOSDEM 2023", "Sailfish OS", "Dozing support", "Mobian", "Plasma Mobile","Lomiri","Ubuntu Touch","PinePhone Pro","Librem 5",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

_Also:_ Purism shipping a better WiFi card in future Librem 5 units, Lomiri now all packaged up in Debian unstable, more Sailfish 4.5 release content, Mobian shipping a working PinePhone Pro camera and "generic" Snapdragon 845 images + an image for the Fairphone 4. _And do listen to that podcast!_

<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#82 Software Performance](https://thisweek.gnome.org/posts/2023/02/twig-82/)
- Dino blog [Dino 0.4 Release](https://dino.im/blog/2023/02/dino-0.4-release/)
- GTK Blog: [Updates from inside GTK](https://blog.gtk.org/2023/02/09/updates-from-inside-gtk/)
- [Andrii Zymohliad 🇺🇦🦀: "Just released #WatchMate v0.4.1…"](https://fosstodon.org/@azymohliad/109845051410034195)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: The best Plasma 5 version ever… again!](https://pointieststick.com/2023/02/10/this-week-in-kde-the-best-plasma-5-version-ever-2/)
- KDE Announcements: [KDE Ships Frameworks 5.103.0](https://kde.org/announcements/frameworks/5/5.103.0/)

#### Ubuntu Touch
* [Codemaster Freders: "Fresh out of the lab: Intel Discord Linux app running on Ubuntu Touch aarch64, powered by box64"](https://mastodon.social/@fredldotme/109843166337548547)
* [Marius Gripsgard: "#Lomiri on debian is now fully my daily driver 🥰 it's coming together and it's finally in the official archive, but some minor changes needed to get this stability"](https://fosstodon.org/@mariogrip/109843421683519637)
* [sunweaver: "Celebration time: All #Lomiri packages we need for #Debian 12 have landed in Debian unstable. Now we'll need to make sure that all pkgs migrate to Debian testing. Then comes the fine-polishing and then Debian will have a new Operating Environment highly suitable for touch based devices such as tablets and phones. Cudos to all the developers and contributors over at the #UBports Foundation."](https://fosstodon.org/@sunweaver/109828351339862872)

#### Sailfish OS
- [Sailfish Community News, 9th February, Yrityspäivät](https://forum.sailfishos.org/t/sailfish-community-news-9th-february-yrityspaivat/014466)
- Jolla Blog: [Sailfish OS 4.5 Struven Ketju](https://blog.jolla.com/struven-ketju/)

#### Distributions
* [Mobian: "Good news everyone! We've officially switched to "generic" SDM845 images with firmware being extracted by droid-juicer at runtime! Tested on both devices, GPU, wifi and modem seem to work just fine. We're also shipping a SM7225 image for the Fairphone 4. …"](https://fosstodon.org/@mobian/109851915093343974)

#### Stack
- Phoronix: [Hangover Project Restarted To Run Windows 32-bit/64-bit Apps On ARM64/PPC64 & More](https://www.phoronix.com/news/Wine-Hangover-2023-Open-Source)

#### Linux
- Phoronix: [Linux 6.2-rc8 Released - Stable Kernel Release Next Week](https://www.phoronix.com/news/Linux-6.2-rc8-Released)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-02-10](https://matrix.org/blog/2023/02/10/this-week-in-matrix-2023-02-10)
- Matrix.org: [FOSDEM 2023: Matrix’s first physical devroom](https://matrix.org/blog/2023/02/09/finally-a-hybrid-conference-that-worked)

### Worth noting

* [Cédric Bellegarde: "I'm working on adding Dozing support to gnome-settings-daemon. Made some progress today, here we can see Geary (patched for this task) pulling emails in 12 seconds, allowing system to sleep again.…"](https://floss.social/@gnumdk/109841335519249387). _This is the most important news of the week, I think._
* [A-wai: "Using a selection of patches from linux-next, a few ones from the linux-media list, @libcamera 0.0.4 and @rmader patches for libaperture and gnome-camera, I was finally able to get a cat picture out of the @PINE64 #PinePhonePro on @mobian 📷 …"](https://fosstodon.org/@awai/109853464619416132) _And the best part is: [You can try this at home, easily!](https://fosstodon.org/@awai/109856871978811760)_
* [Jacopo Mondi: "@libcamera has recently received an implementation proposal of an auto-focus algorithm for Rockchip RK3399. A great occasion to continue the work done at @fosdem to support the @PINE64 PinephonePro cameras."](https://social.kernel.org/notice/ASXjXY33rUh9mfZ0z2)
* [Best phone that doesn't use the Android kernel? - r/mobilelinux](https://www.reddit.com/r/mobilelinux/comments/10v4ya9/comment/j7ffl83/). _Join the discussion!_

### Worth reading
- wongkee's Blog: [Pinephone Pro and libcamera on mobian](https://wongkee.ca/2023/02/13/pinephone-pro-and-libcamera-on-mobian/).
- LINux on MOBile: [Convergent Windows for KDE Plasma](https://linmob.net/convergent-windows-for-kde-plasma/). _Great post, thanks plata!_
- Purism: [Shipping new SparkLAN Wifi cards with Librem 5](https://puri.sm/posts/shipping-new-sparklan-wifi-cards-with-librem-5/) _Frankly, this is a really good decision. To quote from a draft for an unfinished Librem 5 review: "Two obvious problems of the Librem 5 both contain the word 'pine' - one is PINE64, whose devices are way more affordable, and the other one is the Redpine Wifi/BT chip, which gets hot and really hurts battery life". I tried this card when it was first discussed in the Librem 5 Matrix room, and even without proper software support and without me connecting both antennas, it was better in many ways - while I can't find my Sparklan card right now, I still am sure that this is a good move by Purism._
  - [Chris Vogel: "My #Librem5 had been delivered with a Redpine..."](https://chrichri.ween.de/o/d8105239154d41a0b28a3588c190df6e)
  - Phones (Librem 5),- Purism community: [New Post: Shipping new SparkLAN Wifi cards with Librem 5](https://forums.puri.sm/t/new-post-shipping-new-sparklan-wifi-cards-with-librem-5/19424) _Related forum thread._
- Purism: [New Librem 5 USA Phone Customers Can Opt-in for 30-days of free AweSIM, Purism’s Privacy-First Prepaid Cellular Plan](https://puri.sm/posts/new-librem-5-usa-phone-customers-can-opt-in-for-30-days-of-free-awesim-purisms-privacy-first-prepaid-cellular-plan/)
- Purism: [Expand the Librem 5 Hardware with The Breakout Board](https://puri.sm/posts/expand-the-librem-5-hardware-with-the-breakout-board/)
- Switzer et al.: [Junkyard Computing: Repurposing Discarded Smartphones to Minimize Carbon](https://dl.acm.org/doi/pdf/10.1145/3575693.3575710).
- Felipe Borges: [FOSDEM with GNOME was a blast!](https://feborg.es/fosdem-with-gnome-was-a-blast/)
- Volker Krause: [FOSDEM 2023](https://www.volkerkrause.eu/2023/02/11/fosdem-2023-recap.html)
- LINux on MOBile: [Attending FOSDEM 2023 for fun and profit](https://linmob.net/attending-fosdem-2023/). _Boring!_


### Worth listening 
- postmarketOS Podcast: [#28 FOSDEM 2023 Special](https://cast.postmarketos.org/episode/28-FOSDEM-2023-special/)
  - Lemmy - postmarketOS: [#28 FOSDEM 2023 Special](https://lemmy.ml/post/753317)

### Worth watching
- RTP: [I2P Browser For Pinephone / Linux Devices (ready to try)](https://tube.tchncs.de/w/3374fd95-2291-4eb9-890e-3007d4b716d8)
- Włodzimierz Ciesielski: [Samsung Galaxy A3 (2015) with postmarketOS: ssh session](https://www.youtube.com/watch?v=SBbWgDu4ahM)
- Włodzimierz Ciesielski: [Samsung Galaxy A3 (2015) with postmarketOS: Phosh performence](https://www.youtube.com/watch?v=WcgywfQdHEc)
- Lup Yuen Lee: [Apache NuttX Real-Time Operating System for PINE64 PinePhone](https://www.youtube.com/watch?v=kGI_0yK1vws)
- Equareo: [Ubuntu Touch 20.04 beta on Fairphone 4, waydroid](https://www.youtube.com/watch?v=074lcFKloLw)
- Continuum Gaming: [Microsoft Continuum Gaming E351: Sailfish OS 4.5 – Update Walkthrough in real time](https://www.youtube.com/watch?v=5vBZlLyN-9c)
- Purism: [Expand the Librem 5 Hardware with The Breakout Board](https://www.youtube.com/watch?v=Xv0gXfxCKEA)
- Nicco Loves Linux: [Can a GNOME Linux Phone be Daily Driven?](https://tube.kockatoo.org/w/7e5cae8f-1977-4026-b2f1-6911bf55e3c5), [YouTube](https://www.youtube.com/watch?v=7lHH9yb75qc). _I'd recommend another distro and visiting LinuxPhoneApps.org, but that's just me, I guess :-)_


#### FOSDEM 2023
* [FOSDEM 2023 - Lomiri Mobile Linux in Desktop mode](https://fosdem.org/2023/schedule/event/lomiri/)
* [FOSDEM 2023 - AMENDMENT Sharp photos and short movies on a mobile phone](https://fosdem.org/2023/schedule/event/sharp_photos/)
* [FOSDEM 2023 - Mobian: to stable... and beyond!](https://fosdem.org/2023/schedule/event/mobian_to_stable_and_beyond/)
* [FOSDEM 2023 - What's new in the world of phosh?](https://fosdem.org/2023/schedule/event/phosh/)
* [FOSDEM 2023 - Ondev2: Distro-Independent Installer For Linux Mobile](https://fosdem.org/2023/schedule/event/ondev2_installer/)
* [FOSDEM 2023 - Sailing into the Linux port with Sony Open Devices ](https://fosdem.org/2023/schedule/event/sailfish/)
* [FOSDEM 2023 - AMENDMENT Writing a convergent application in 2023 with Kirigami](https://fosdem.org/2023/schedule/event/convergent_kirigami_apps/)
* [FOSDEM 2023 - Where do we go from here?](https://fosdem.org/2023/schedule/event/future_of_mobile/)
* [FOSDEM 2023 - Bluetooth state in PipeWire and WirePlumber](https://fosdem.org/2023/schedule/event/bt_pipewire/)
* [FOSDEM 2023 - Modern Camera Handling in Chromium](https://fosdem.org/2023/schedule/event/om_chromium/)
* [FOSDEM 2023 - Covid Exposure Notification Out in the Open](https://fosdem.org/2023/schedule/event/publiccode_dpg_covid_exposure/)

### Thanks

Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)
