+++
title = "Looking Back at 2024 and a Glimpse at 2025"
date = "2025-01-04T23:49:37Z"
draft = false
[taxonomies]
tags = ["Phosh", "Plasma Mobile", "Sxmo", "postmarketOS", "Mobian", "nlnet", "LinuxPhoneApps", "Ubuntu Touch", "Sailfish OS", "Gecko", "Android Translation Layer", "libcamera", "Megapixels", "grants", "burnout",]
categories = ["commentary", "personal",]
authors = ["Peter"]
+++

Properly reviewing a year is a lot of work.
This post is not that, it's just a look back with a tiny glimpse into the future.

<!-- more -->

## Highlighting some things so that they are not missed

Let's start with some projects that may not have gotten enough exposure here. _If your great project is not on this list, but would have deserved to - I am sorry, it was not intentional, and thank you for all your hard work!_

### Android Translation Layer

- [Android Translation Layer on Gitlab](https://gitlab.com/android_translation_layer/android_translation_layer)

Just look at the logo, it's a green variant of the Wine logo. Like Wine, this is not an emulator, but an environment to execute Android apps on Linux. This means: Unlike with [Waydroid](https://waydro.id/), you don't need to run a full LineageOS in a container to just run an app. It's early days, but [NewPipe works](https://flathub.org/apps/net.newpipe.NewPipe), and reportedly, [WhatsApp can be used with it](https://lemmy.kde.social/post/2232894), too.

### 81voltd

- [81voltd on gitlab](https://gitlab.postmarketos.org/modem/81voltd)

Like it or not, planned obsolescence has been increasing through standard phone calling now being carrier-dependent, proprietary garbage in more and more markets. While this leads to more normies switching to stuff like calls via Signal and co. (which also is not totally easy to pull off on (mobile) Linux), 81voltd is a project that makes VoLTE calling possible on mainline before we have a proper implementation in ModemManager. 

(At least this is not just an issue over here, but many carrier ROMs and custom ROM users share the joy of phone calls being potentially better sounding, but also a lot less reliable.)

### libcamera and Megapixels 2

- [libcamera.org](https://libcamera.org/)
- [Megapixels](https://gitlab.com/megapixels-org)/[libmegapixels](https://libme.gapixels.me/), [Megapixels 2.0.0-alpha1 announcement](https://blog.brixit.nl/megapixels-2-0-alpha-release/)


Yes, these two kind of compete, but they've both progressed meaningfully, supporting more and more cameras and fixing bugs, improving image quality etc. Is there still room for improvement? Hell yeah! But boy are things better than they were a year ago.


### The Gecko 91 Port for Sailfish

As Jolla is [now de-russified](https://jolla.com/content/uploads/2023/11/Former_leadership_buys_Jolla_Business_Pressrelease_271123_.pdf?x25612), and Sailfish OS 5 is slowly rolling out, 
I want to highlight a very important community contribution that is part of number 5: [flypigs port of Gecko 91 as a newer browser engine for Sailfish OS's Gecko-based browser](https://flypig.co.uk/list?&list_id=1200&list=blog) makes it more compatible with the ever progressing (or en****ifying?) web, it's also rather well documented. So if you think it should be 102, 115 or 128 instead of 91... There's not much holding you back but time and effort.

### Snaps on Ubuntu Touch

[Snap support on Ubuntu Touch](https://open-store.io/app/snapz0r.fredldotme) has matured. It may not be perfect yet (IMHO, it needs a Flatseal-style companion feature to easily set environment variables to make apps scale well), but it's definitely worth playing with it and some tinkering.

Also, once Ubuntu Touch 24.04 lands, running apps via Libertine (the container thingy you can install .deb packages in), is going to make more sense than ever before. 

That said, if you are looking for something to do in 2025, it would be great to see 
1. Ubuntu Touch apps outside Open-Store as distro packages or
2. flatpaks and some Qt 6/Kirigami or libadwaita apps as click-packages in Open-Store. 

Coming up with a well-documented proof-of-concept may unlock a lot of potential!


### Plasma (Mobile) 6

[Plasma 6 landed](https://plasma-mobile.org/2024/03/01/plasma-6/), and has seen two more point releases since. It's pretty good these days! I've used it (even dailied it for a bit) a lot more than I ever used 5.x during the past year, and can only  encourage giving it a spin! Does it have bugs? Sure, but what does not?


### Continued development of Phosh

Despite [Purism's obvious struggles](https://puri.sm/posts/pureos-optional-subscription-added-to-advance-development/), Phosh development has continued as a [community effort](https://phosh.mobi/), and it shows. If you are on a quickly moving distribution, you will by now have noticed that on the last day of 2024, Phosh gained easy wallpaper support among other things: Just set one in GNOME Settings. 

While I quickly searched for the necessary `gsettings` commandos to unset the wallpaper and return to the OLED-pleasing solid black[^1], other features like

- the new file picker portal that finally ends the days of painfully using gtk-file-picker or shipping a patched version of GTK and makes opening and saving files as easy as it should be,
- the Quick Settings that allow you to connect to a different WiFi network, Bluetooth device, or toggle Dark Mode or Mobile Data,
- increasing the hight of the virtual keyboard to reduce hand pain/typos 
can't be underrated. 

Thank you so much, everybody!


## Things to look forward to in 2025

### Cell Broadcast support

Landed in Phosh, not yet released in ModemManager/libmm-glib).

### Phrog

- Phrog being adopted in distributions (Mobian already uses phog) could free users from entering a keyring password every time.

### ... and of course, a lot more

To just give you an idea, here's a list of relevant projects that have been awarded a grant by nlnet. Please note, that there are other organisations that offer grants, so this just a small selection to give you an idea:

- [NLnet; Security audit of Sailfish FOSS components](https://nlnet.nl/project/Sailfish-FOSS-audit/)
- [NLnet; Mainline Linux on ARM Chromebooks](https://nlnet.nl/project/MT818x_MT819x-firmware/)
- [NLnet; Grate project](https://nlnet.nl/project/Grate/)
- [NLnet; Better support for display notches and cutouts in Phosh](https://nlnet.nl/project/Phosh-Notch/)
- [NLnet; Organic Maps сonvergent UI for Linux with Qt Quick/Kirigami](https://nlnet.nl/project/OrganicMaps-ConvergentUI/)
- [NLnet; Empowering Mobilizon](https://nlnet.nl/project/Empowering-Mobilizon/)
- [NLnet; Integration of Waydroid on mobile GNU/Linux](https://nlnet.nl/project/Waydroid-linuxmobile/)
- [NLnet; Free and open source NPU Drivers](https://nlnet.nl/project/Rockchip-NPU-driver/)
- [NLnet; OpenIMSd](https://nlnet.nl/project/VoLTE-Qualcom/)
- [NLnet; OpenAGPS](https://nlnet.nl/project/OpenAGPS/)
- [NLnet; Verso Views](https://nlnet.nl/project/Verso-Views/)
- [NLnet; Webview library with Verso for Tauri](https://nlnet.nl/project/Verso-WebView/)
- [NLnet; Enhancing Firefox for Linux on Mobile](https://nlnet.nl/project/Firefox-linuxmobile/)
- [NLnet; FOSS Warn](https://nlnet.nl/project/FOSS-Warn/)
- [NLnet; UnifiedPush](https://nlnet.nl/project/UnifiedPush/)
- [NLnet; tslib](https://nlnet.nl/project/tslib/)
- [NLnet; postmarketOS daemons](https://nlnet.nl/project/postmarketOS-daemons/)
- [NLnet; Wayland input method support](https://nlnet.nl/project/WaylandInput/)

... and if you have a project, make sure to apply for a grant. The worst thing that can happen is that you've wasted a few hours on that grant application.


## Personal Notes (Peter)

Aside from daily-ing Plasma for quite a stretch of time (OnePlus 6/FP5),and before that, Sxmo at the beginning of the year (PinePhone Pro)), I've settled on daily driving the Pixel 3a (with Phosh). Lots of things work on this device on mainline, and battery life has been really good on mine. I like the fact that the devices is relatively tiny and narrow. It feels good in the hand, and the display is not hampered by notches. Despite always recommending stable, I still use postmarketOS edge on it, with mostly flatpaks - I am considering a switch to 24.12.

In 2024 my contribution to Mobile Linux has continued to shift; moving from reporting to contributing to software. And I may have followed "this is interesting, so let's try something with this" a bit too much. Paired with less work hours on $dayjob, I attempted to ramp up my time spent on #LinuxMobile starting in September - in addition to basically following [this meme](https://i.kym-cdn.com/photos/images/original/001/985/007/1d8.jpg) for years. As $dayjob did not happen to getting less stressful, I started feeling burnt out in November, and it only got worse in December. Hence, I kept my schedule clean, did not apply for talks, a really peaceful, almost boring 38C3. Only time will tell what recovery looks like, but changes are ahead.

The goal for the beginning of the year is to keep taking it easy, finishing up works in progress, and handing of responsibilies, meaning: 

Want to co-maintain this blog? Please get in touch.[^2]

Want to maintain LinuxPhoneApps.org? Please get in touch.[^3] 

Now, there's no hurry; I don't expect to abandon the projects altogether if I can help it. But a certain slow-down has been evident here and on LinuxPhoneApps.org, and it's likely going to stay as boring and slow. And, vacation in 2025 will mean: Not working on personal projects and call it "relaxing". 

## Concluding thoughts

Let's make the best of this year! Have a great year, don't be afraid to try things, and ... try to not burn yourselves out in the process!



[^1]: If you must know, the following worked for me:
~~~
gsettings set org.gnome.desktop.background picture-options 'none'
gsettings set org.gnome.desktop.background primary-color "#000000"
~~~

[^2]: Mail, Fediverse, Matrix - whatever you prefer. Just say hi, and we'll figure it out together.

[^3]: Same here, I already have someone to ask in mind - these days things take me forever, so some more days may pass before I even ask.
