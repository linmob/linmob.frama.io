+++
title = "MFC15: Two Merge(d) requests and a Pull Request"
date = "2024-06-20T21:31:47Z"
[taxonomies]
tags = ["mobile-config-firefox", "Firefox"]
categories = ["projects"]
authors = ["Peter"]
+++

In the [previous post of this series](https://linmob.net/mcf14-almost-128/), I wrote:

> What would be next, provided I had the time? Besides fixing Private Browsing and keeping track and taking care of changes in Firefox that negatively affect the mobile-config-firefox user experience? 
<!-- more -->

... and: The "Besides" things are now fixed.

### Requests

The details can be found in the Merge Request, which have already been merged by the amazing Ollie:

1. [appMenu.css: Adjust appMenu.css for Firefox 127 (!50) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/50) 
2.  [tabmenu.css: Fix private browsing mode (!51) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/51)

The first one was simple, it's just an ID change, and since ESR 128 is around the corner, I've made room for the Translate feature.

The second was surprisingly easy. I don't really know why did not manage to reduce the private browsing indicator before, I think I was approaching it in a too complicated way. Is this fix beautiful? Nope. But it does what is supposed to do, on ESR 115, 127 and Nighly 129a1.

So that's all, right? Nope, I've also created a [pull request to Emma's fenix branch of mobile-config-firefox](https://codeberg.org/user0/mobile-config-firefox/pulls/6). This way, users who prefer the Fenix experience, will have the fix soon, too.

### Conclusion

That's it for now. This may well be the last post in this series before the July 9th release of Firefox 128, unless something comes up. Once a release is made, I may look into helping with packaging (for non-postmarketOS distros), so that these fixes land in time (as I find that more important than spending the scarce resource time on tiny, further improvements).
