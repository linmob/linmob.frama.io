+++
title = "MCF13: Continuing"
date = "2024-04-17T21:40:23Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox"]
categories = ["projects"]
authors = ["Peter"]
+++


Yesterday, I finally did spend some time on mobile-config-firefox again.

<!-- more -->

### A Tiny Merge Request

I have submitted a tiny [Merge Request](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/49), that was inspired by listening to [postmarketOS podcast // #39 INTERVIEW: magdesign (of Cycling the World Fame)](https://cast.postmarketos.org/episode/39-Interview-magdesign/).

### Yesterdays New Branches

Also, I created a bunch of new branches on my Codeberg repo, as I try to figure out what's next for me to do.

- [experiment-minimal-changes](https://codeberg.org/1peter10/mobile-config-firefox/src/branch/experiment-minimal-changes) _I've just tried to delete things, and see if things still somewhat work. I may delete even more going forward, not putting the UI on the bottom allows for removing a bunch of CSS. So far, this is nothing serious, but it may help towards [Make moving the address bar to the bottom optional (#51)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/51) and [Figure out path to upstreaming changes (#28)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/28). While I am glad to have learned that [someone applied for a grant to work on this](https://postmarketos.org/blog/2024/04/14/pmOS-update-2024-04/#grant-applications), please note that this is not me :-)_

- [experiment-userContent-rebase-fenix](https://codeberg.org/1peter10/mobile-config-firefox/src/branch/experiment-userContent-rebase-fenix) _This branch was created to be able to [quickly grasp what user0 has changed in her fenix branch compared to what's upstream](https://codeberg.org/1peter10/mobile-config-firefox/commit/36ecc256babdf9636bc2f99cead9c06a69df12b5). This may be helpful when tackling some of the userContent related issues, which are

  - [about pages improvements (#17)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/17),
  - [Cannot change language in portrait mode (#60)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/60) (it won't help here, it's present, and IIRC it's style-attributes to the element that enforce a min-width of 600px). I also merged this branch into my [next](https://codeberg.org/1peter10/mobile-config-firefox/src/branch/next) branch.

### What's next?

I honestly don't really know. I have spent some time with the private browsing indicators, which have been renewed by Mozilla so that the [current implementation](https://gitlab.com/postmarketOS/mobile-config-firefox/-/blob/master/src/userChrome/tabmenu.css?ref_type=heads#L12-L17) for private browsing in mobile-config-firefox is broken. So far, [I have failed to find a fix for current](https://linmob.net/mcf11-private-browsing-struggles/). I will try again this week, and if I keep failing, we could always just hide these inidcators for good - but I would first open an issue. So: If you see a new issue about that, and it's been authored by me, I must have kept failing.

What else?
Let's have a look at the open issues I have not mentioned yet in this post:

- [Consider additional privacy options (#68)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/68): This one is new, and I could easily implement it - but I want to see further discussion first.
- [Disable Use hardware acceleration when available (#56)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/56): I won't be able to contribute anything useful to that one, sorry. 
- [FF 113 support? (#52)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/52): Should be closed, IMHO.
- [strict bound more panel object to fix flicker (#49)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/49): I have to force myself to check if we have all of these implemented, and if so suggest closing it.
- [Add History to trop-down menu (#45)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/45): Since the height should be increased for the translation feature anyway, this could be done while doing that - but what's the benefit? I wonder if unhiding Firefox View after making its contents mobile friendly might not be the better way.
- [Tabs in firefox portrait mode theme should be a third or less of the minimum width that they are now (#42)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/42): I tried to [explain my viewpoint](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/42#note_1831348998). I definitely want to spend time bringing the tabcounter over from fenix, it's useful.  
- [Menu bar moves to top when rotating to landscape (#37)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/37): As a former PinePhone Pro with Keyboard user, I think the more important question is: How can we make the experience better in landscape and waste less space? IMHO, true-mobile-mode is only nice in portrait (there's enough vertical space), but really, really good in landscape.
- [Feature request: add close buttons in all the tabs (#34)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/34): We could do that. I think I'll make a MR for the fun of it, it's not really useful with how tab-switching works, but for "tab-bar-swipers", why not?
- [Bookmarks bar does not show in portrait mode (#33)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/33): Um, no idea what to do with that one. Close it, maybe?
- [context menu flickering on link long-press (#32)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/32): Isn't this more of a compositor issue? I'll need to investigate.
- [Pinephone Pro small address bar (#30)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/30): Should be closed, IMHO.
- ["Save image as" dialog unusable (#16)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/16): I so far failed to reproduce this.
- [Address bar: do not show "https://www." (#14)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/14): I've [outlined](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/14#note_1830810846) a potential fix. Awaiting further discussion.
- ["Customize Firefox" needs rework (#13)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/13): Personal priority: super low. Might be fun to play with though :)
- [Download confirmation popup almost unusable (#12)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/12): I don't understand how this can be reproduced, if it still can.

That's it for now, thank you for reading! If you have suggestions for what I should do, please get in touch! :-)