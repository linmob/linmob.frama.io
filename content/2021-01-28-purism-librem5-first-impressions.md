+++
title = "Purism Librem 5: First impressions"
aliases = ["2021/01/28/purism-librem5-first-impressions.html"]
author = "Peter"
date = "2021-01-28T22:12:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["Purism", "Librem 5", "first impressions", "Unboxing", "Phosh", "Video content", "Software"]
categories = ["impressions"]
+++

_These were intended to be some rough notes accompanying my Unboxing video, which you can watch on [LBRY](https://lbry.tv/@linmob:3/unboxing-the-purism-librem-5-evergreen:e) or [YouTube](https://www.youtube.com/watch?v=5WdnPw537nU). It got a bit longer and thorough._

<!-- more -->

Yesterday I finally received a Librem 5. It’s not the one I ordered in October 2017 &mdash; I purchased it from a not too happy backer.

The box is fairly large, it contains a charger, a USB-C to C cable, nice wired headphones and an instruction manual.

### Hardware

The phone also is fairly large and thick (153 x 75 x 15.5mm), and not only that, it‘s quite heavy.[^1] A portion of the weight can be attributed to the 17.1 Wh battery that Purism decided to include to deliver all day battery life (I can’t say whether that worked yet and will report back), and another contributor is the metal frame that makes the device feel sturdy. The backplate is made of plastic and wiggles a little bit, negatively affecting the solid overall impression of the device. A nanoSIM card and a microSDXC card can be put into the device using a tray on the right side of the device, which is easy to open thanks to a nice pin that’s also in the box.

On the left side of the device we find the three kill switches for the LTE modem, WiFi and Bluetooth and the microphone/cameras respectively. They are easy to use and still feel like they won’t be triggered accidentally while the phone is in the pocket.[^2]

If you want to read the specs, head over to [LinuxSmartphones.com](https://linuxsmartphones.com/phones/purism-librem-5-specs/) or the [comparison in the Purism forums](https://forums.puri.sm/t/comparing-specs-of-upcoming-linux-phones/6827).

### Software

Turning the phone on is an easy and fast process, PureOS is a fast booter. The welcome wizard is fast and simple, the Librem 5 generally feels quite snappy. The setup is sane: SSH is disabled by default, which makes perfect sense for a phone that targets _"normal people"_ and wants to be secure.[^7]

The number of preinstalled apps is small: Calls, Chatty (SMS+XMPP), Contacts, GNOME Web (which is surprisingly fast on the Librem 5) are joined by  Calculator, Chess (why?),Clocks, Geary (for email), PureOS Store (for Updates and additional Software),  Settings, Terminal, Text Editor, Usage and Weather.

Immediately notable omissions are a media and/or music player, a calendar, a Maps app, and [considering the promises of the 2017 crowdfunder](http://web.archive.org/web/20170831231354/https://puri.sm/shop/librem-5/) any Matrix support.[^3] As the hardware enablement for the Librem 5’s cameras is yet to happen, the omission of a camera app makes perfect sense. 

PureOS Store is a fork of the GNOME Software app, and while it does not fit the screen perfectly yet, Purism do a good job in recommending apps and games (e.g. Password Safe, Lollypop, Animatch) that are useful and work nicely on the Librem 5.
But it is not perfect: A search for Firefox leads to nothing, `sudo apt install firefox-esr` gets that job done though. It's the same for Cawbird, the twitter client.

Also, as an [App List maintainer](https://linmobapps.frama.io) I must say that Purism really should go a ahead and package some more GTK/Libhandy apps:  `apt search` on PureOS Amber returns nothing for 
* giara,
* notorious, or
* ~~tootle~~ gnome-podcasts,[^6]

and for other apps [that I use on my PinePhone](https://linmob.net/2021/01/09/my-setup-with-danctnix-archlinuxarm.html), like eog, gnome-todo and gnome-maps, it's similar: Only outdated, or non phone-optimized versions of these programs are in the repositories. _I really suggest the Purism team to package up all libhandy/GTK apps that are beyond a "WIP" state and add them to the `amber-phone` repo. Maybe the community can help out here with a few instructions._[^6]

While many of these apps are available as Flatpaks on Flathub (which has to be enabled manually), the unpatched[^4] Flathub GTK runtime makes these apps work worse than if they were packaged for PureOS.

Regarding PureOS, it's worth noting that PureOS Amber is essentially a FSF-endorsed libre Debian 10 Buster. I look forward to the day when PureOS switches the Librem 5 over to "Byzantium", which is their adaption of Debian 11 Bullseye &mdash; the idea of using old software on a nascent platform just feels very foreign to me. Currently, running Byzantium is not recommended, but I am certainly going to try [Mobian](https://www.mobian-project.org) on the Librem 5 once I get to it.[^5]

### Usage

While I managed to add my Nextcloud in Settings > Online Accounts, the contacts would not show up in contacts, which is something that worked fine on multiple PinePhone distributions for me. So I had to call my Mum who's number I still recall from memory, and that worked fine &mdash; the call quality is okay. SMS work too, but this is just early testing and I can't say whether it is truly reliable yet.

The smaller 5.7" 18:9 screen makes me more convinced that [Squeekboard](https://source.puri.sm/Librem5/squeekboard), the virtual keyboard, could use some 5 to 10 percent extra height to be easier to type on with my relatively large fingers. 

The Librem 5 also gets a bit warm once the CPU is in use or during phone calls, but not uncomfortably hot. It feels quite similar to how the PinePhone used to feel in that way. The only hardware issue I have experienced is that the Wifi/Bluetooth chip does not always work, but toggling it using the kill switches usually gets it to life fairly quickly. BTW: Did I mention the Librem 5 supports 5 GHz Wifi networks?

### Conclusion

All in all, the Librem 5 is a nice phone, and I look forward to using it more and to see how Purism manage to develop this product further &mdash; there's still a lot of work to be done on the software side as I wrote above.

The hardware, while thick and heavy, is definitely faster than the PinePhone. It looks like that I'll actually need to get into benchmarking, because it's not just the CPU and the 300MHz higher clockspeed from what it feels like.

I guess that most people would like an answer to the question "Should I get this instead of the PinePhone?", but I can't really give that just yet, sorry.

_If you want to see what this phone looks like, check out "[Purism Librem 5: Visual Impressions](https://linmob.net/2021/01/30/purism-librem5-visual-impressions.html)"._


[^1]: Purism say it weighs 268 grams, and that feels true. Unfortunately, I don’t own a pair of scales, so I can’t offer exact measurements.

[^2]: Unlike the PinePhone they feel useful on the Librem 5.

[^3]: For text-based conversations, https://hydrogen.element.io works fine in GNOME Web. I installed it as a web application immediately.

[^4]: Purism patched GTK3 to make it work better with the phone.

[^5]: I would love to run DanctNIX mobile on this device, but I doubt that I'll be able to pull that off.

[^6]: _Added January 29, 2021, 7:04 am UTC:_ As Purism developer [Sebastian Kryzszkowiak points out in the DevTube comments](https://devtube.dev-wiki.de/videos/watch/66ea3360-2b2a-4e3e-8796-de7ae271ac1b;threadId=9392), _Tootle_ has been added in the hours between writing and publication of this article, and _Giara_ apparently has been in `amber-phone-staging` for a while (by 3pm UTC the same day it now has made it to `amber-phone`. So Purism developers are definitely actively packaging; which is laudable especially given the size of their team. That said: As a user, I always wish for more packaged software, which is why I inserted Gnome Podcasts.

[^7]: This sentence contained the term SSL instead of SSH in a previous version, which obviously was a typo. The process of setting up SSH for now requires using the terminal, which could be made easier in the future.
