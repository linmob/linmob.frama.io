+++
title = "MCF10: The Bug and the Bugfix"
date = "2024-04-02T23:15:27Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "bugfix", "Firefox"]
categories = ["projects"]
authors = ["Peter"]
+++

Sometimes you win, and sometimes you learn something - turns out, 4.3.0 was such a learning opportunity.

<!-- more -->

### The shock

Picture this: You boot up a Linux phone after a while (in my case it was my Droidian Pixel 3a), and you figure: Hey, let's try upstream mobile-config-firefox on this one, and see how it does. And then you run into a bug: The unified extensions menu (you know, that puzzle-piece icon) flickers.

I hastily reached for my postmarketOS Phosh PinePhone. Did I miss something? No... it appears to be fine over here. I figured: Is this a 200% vs. 300% scaling thing? Have I not tested this enough on the OnePlus 6? I briefly confirmed, that that device is also not affected. Then, the Librem 5 with PureOS. It's also affected. 

### The fix

The fix is a one-liner, as can you [can see here](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/48/diffs), and it's merged and released by now in 4.3.1.

The upside is that this fix has also an upside for distributions not affected (I could not reproduce the issue on Arch Linux ARM, too): It fixes the Translation popup's width, too.

The downside is that I have no idea why this happens. So far, I've only been able to reproduce it on Debian-based distributions, and the best guess is, that it may be due to GTK patches. I don't know whether the issue also affected Fedora, Mobile NixOS, OpenSuSE, OpenMandriva or other distributions - I welcome feedback on this, but don't feel like putting in hours to confirm this, as I won't realistically be able to test every change I make on all these distributions. But: I'll test my changes on a Debian-based distribution from now on before commiting them.

### What else did I do?

I've also [made sure that 4.3.0 lands in DanctNIX Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/pull/621) (and I'll do the same for 4.3.1 soon); and I've considered learning something new and entirely different by at least attempting creating a Merge Request for 4.3.1 to land in PureOS (this would envolve [rebasing patches](https://source.puri.sm/Librem5/debs/firefox-esr-mobile-config/-/tree/pureos/byzantium/debian/patches) and learning what all these other fun files in the [debian folder](https://source.puri.sm/Librem5/debs/firefox-esr-mobile-config/-/tree/pureos/byzantium/debian) are for) - however, the [state of a previous Merge Request is giving me pause](https://source.puri.sm/Librem5/debs/firefox-esr-mobile-config/-/merge_requests/6).

### What's next?

I've outlined the next steps before in [07](https://linmob.net/mcf07-its-been-too-long/#what-s-next). I still need to get the nightly Snap run somewhere (no, Snaps don't run on Droidian) and my Librem 5 needs space to freed up in order to install the Snap on PureOS. Speaking of different variants of Firefox: I also want to look into installing and documenting using mobile-config-firefox with LibreWolf.

All that said, I'll likely first toy around more with various things to keep this fun. If you want to follow what I am doing, and don't mind things being somewhat broken at times, you may follow [my aptly named "next" branch](https://codeberg.org/1peter10/mobile-config-firefox/src/branch/next). If you do, please let me know what you think - feel free to file issues or send emails!
