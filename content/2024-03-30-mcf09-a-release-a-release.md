+++
title = "MCF09: A Release! A Release!"
date = "2024-03-30T10:38:10Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "LinuxMobile"]
categories = ["projects"]
authors = ["Peter"]
+++

Another Merge Request got merged, and ollieparanoid tagged a release!

<!-- more -->

Since I try to stay off my connected devices when talking to other people in what some people call "the meatspace", the release almost hit me by surprise, when I checked the upstream repo last night: 

Not only had ollie merged [popups.css: Fix various popup menus and installing extensions (!47) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/47), but he had also tagged a release (4.3.0), coming soon to your distribution (at least on postmarketOS edge). Why on earth did he do that? Let's have a look at [MCF07: It's been too long](https://linmob.net/mcf07-its-been-too-long/#what-s-next):

> After the next MR, which again, will be put together once the current one is merged, I will be taking a bit of a break, as I need to spend some time on LinuxPhoneApps.org. This might be an opportunity to make a release of mobile-config-firefox ;-)

Looks like I really need to be more careful when I write these posts, and joke around less ;-).  I won't bore me (with writing) and you (with reading) what's in there, just go and [read the release notes](https://gitlab.com/postmarketOS/mobile-config-firefox/-/tags/4.3.0).

### What's next?

As outlined before, there's a lot I want to experiment with and do with regard to improving mobile-config-firefox. Smaller bug fixes (to filed an unfiled bugs), are of course a priority over large refactoring - just to keep motivated alone. E.g., private browsing windows still feel pretty broken, and that translation popup is still to wide (one fix for that is surprisingly simple, and might lead to further cleanup of popups.css) - and somewhat disruptive, so that I think that something like [this](https://codeberg.org/1peter10/mobile-config-firefox/src/commit/f9dc97147179b09f455e4d0732f588359c32b357/src/mobile-config-autoconfig.js#L319-L322) should be considered. While we're at  `about:config` stuff: [Feedback on not showing `https://` in URL bar](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/14#note_1830810846) (and thus showing more of the URL) is very welcome! And I still need to setup the Nightly snap and test with that ... likely that will happen on a Mobian install on the Shift6mq.

As you can see, while I must stop, it's really hard for me to even take a short break from this fun ride that is essentially CSS hacking my favorite web browser!

But, these grant applications don't get finished without putting in the time (and deadlines are approaching), and these [app listings](https://linuxphoneapps.org/apps/) don't update themselves (they [somewhat do](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/tree/main/checkers), but the human review takes some time), so I better stop for a bit.

### What I gained from this so far

This release is also a good opportunity to look at what some people call "personal growth". I have learned a few things, about git, e.g., [how to do signed commits](https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html) (and I now finally have a GPG key for peter@linmob.net, for those who care about these things) or how to [squash multiple commits into one](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/46#note_1829553297) (and that `--force` helps pushing the result; and more. Aside from git, I've also become way more familiar with the Firefox Developer Tools, which has also been useful at my dayjob (I am something like an internal customer happiness person (this is not what my role is called, but it's how I interpret it) for a proprietary web-based content management system, which will soon ship a bad CSS hack I came up with in production).

I have also fixed the most annoying bugs in the [zola-post-composer.html](https://linmob.net/zola-post-composer.html) HTML/JS thing I came up with while at 37c3 and finally made it (somewhat of) a part of linmob.net, so if you always wanted to write something for LINMOB.net but were intimidated by all that front matter and markdown, I welcome you to [give that thing a try](https://linmob.net/zola-post-composer.html) and report annoyances you encounter - it enabled me to write all these posts quicker and faster.

With that, read you some time next week with more progress and considerations on `mobile-config-firefox`! _Happy Holidays!_