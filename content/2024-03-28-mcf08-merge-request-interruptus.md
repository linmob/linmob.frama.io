+++
title = "MCF08: Merge Request Interruptus"
date = "2024-03-28T23:54:00Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "LinuxMobile"]
categories = ["projects"]
authors = ["Peter"]
+++

I wrote I would write today, so here we go: I hit another bug I noticed last minute.

<!-- more -->

After the Sxmo lesson last time, I felt quite safe, as I had tested that. This day was quite busy, and so I had not had time to prepare the text. In an attempt for last minute due dilligence, I figured: Lets not look at my notes on the other computer, I'll figure the issues that can be closed by just looking at the issue tracker.

That did not go well. I attempted to confirm [Some dialogs do not show up (#40) · Issues · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/40) and noticed: Um, that (since the unified extensions menu can no longer be toggled off) somewhat useless Overflow Menu is totally broken. I've since found some kind of a hotfix (hiding #overflowMenu-customize-button helps a lot).

_That's it, I'll experiment some more tomorrow, hope to not find further issues and finish the Merge Request._



