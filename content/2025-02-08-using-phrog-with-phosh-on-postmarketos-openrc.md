+++
title = "Using Phrog 🐸 with Phosh on postmarketOS (openrc)"
date = "2025-02-08T11:44:17Z"
updated = 2025-02-15
draft = false
[taxonomies]
tags = ["postmarketOS", "Alpine Linux", "phrog", "greetd", "Phosh",]
categories = ["howto", "software"]
authors = ["Peter"]
[extra]
update_note = "Adding a use case and add footnote regarding systemd in postmarketOS and a warning for users of postmarketOS 24.12"
+++

At [FOSDEM 2025](https://linmob.net/fosdem2025/), I had the opportunity to take care and demo some devices running Mobian at the [LinuxOnMobile](https://linuxonmobile.net/) stand[^1]. When handling the Poco F1 with Mobian, I was once again reminded that having [Phog](https://salsa.debian.org/DebianOnMobile-team/phog) as a greeter is IMHO a great choice, as it means that you don't need to unlock your keyring manually.[^2]

<!-- more -->

This morning, on holidays in South Tyrol on the first day without plans but with clouds, I had the opportunity to finally try [phrog](https://github.com/samcday/phrog) on my [Pixel 3a](https://wiki.postmarketos.org/wiki/Google_Pixel_3a_(google-sargo)) running postmarketOS edge (with Phosh, still openRC).

### What the 🐸? 

[phrog/🐸](https://github.com/samcday/phrog), like phog, is a mobile-device greeter for Phosh. It uses [libphosh](https://gitlab.gnome.org/guidog/libphosh-rs) and is written in Rust. As the README states, 

> phrog uses Phosh to conduct a [greetd](https://sr.ht/~kennylevinsen/greetd/) conversation.

You do not really need to understand the technical details to use it. Just think of it as something like [gdm](https://gitlab.gnome.org/GNOME/gdm): You log in with it, and if you want, you can also choose a different graphical session, e.g., log into sway instead of phosh, assuming you have sway installed. Also, there's another use-case that may be relevant for device used by multiple users: With phrog, you easily login with different users. So if this was holding you back from going for that Mobile Linux family tablet, don't fret anymore!

### Install and setup

<mark>Do not try this on a systemd-install of postmarketOS with Phosh![^3] If you are on 24.12, the instructions do not work - you will have to tinker a lot as becomes obvious when comparing package file lists for [3.21/24.12](https://pkgs.alpinelinux.org/contents?name=greetd-phrog&repo=community&branch=v3.21&arch=x86_64) and [edge](https://pkgs.alpinelinux.org/contents?name=greetd-phrog&repo=community&branch=edge&arch=x86_64).

If your device contains valuable data, make a backup first. It's recommended [to enable the sshd service before experimenting](https://wiki.postmarketos.org/wiki/SSH), so that you can fix your install if something should go wrong!</mark>


Most of the install process is in [Phrog's readme](https://github.com/samcday/phrog/blob/main/README.md):

~~~~
sudo apk add greetd-phrog
~~~~

Then configure greetd to use Phrog (you can also edit `/etc/conf.d/greetd` and just add the line starting with `cfgfile=` if you prefer it that way):

~~~~
cat <<HERE | sudo tee -a /etc/conf.d/greetd
cfgfile="/etc/phrog/greetd-config.toml"
HERE
~~~~

After that, make sure greetd is run at startup: 

~~~~
sudo rc-update add greetd
~~~~

If you want to make sure that worked, you can now reboot. greetd and Phrog will already be run, in fact, you should already be able to briefly see something black and green, to then end up in Phosh's standard login screen.

To really use phrog, there's one more step:

~~~~
sudo rc-update del tinydm
~~~~

This disables tinydm, the [default display manager on postmarketOS](https://wiki.postmarketos.org/wiki/Display_manager) that usually starts Phosh on postmarketOS. As now greetd/phrog starts Phosh, tinydm no longer needed.

### Enjoy

Now, when you start your phone or log out of Phosh, your screen will look something like this:

<figure>
<img src="/media/2025/pixel3a-with-phrog.jpg" alt="A Google Pixel 3a running Phrog, showing a green bar containing the user's name and a gray menu at the bottom for session selection, with the word Session on the left and the word Phosh on the right, next to a downwards pointing triangle indicating the option to choose a different session." title="A Google Pixel 3a running Phrog" style="height:auto; margin:0;">
<figcaption>A Google Pixel 3a running Phrog.</figcaption>
</figure>

### Undoing (just in case)


If you, unlike me, should not like the Phrog experience, you can undo it by re-enabling tinydm

~~~~
sudo rc-update add tinydm
~~~~

and disabling greetd:

~~~
sudo rc-update del greetd
~~~~

Afterwards, you can uninstall greetd-phrog.

[^1]: Which I put there myself and was not well prepared for (think of offline demos beforehand, e.g., games, Waydroid or an Android Translation Layer app next time) ... it improved comparatively on day 2, but that's a different story. Praise Phosh for these days supporting no-pin unlock and having that Caffeine quick setting, though - demoing was way harder without that, when I last did [prepare demo devices for real](https://linmob.net/lit-2023-stand-experiences/).

[^2]: On Phosh, that is; and only if the keyring password matches your login password. When booting a Plasma Mobile image for the first time, and I see Phog greet(d) me, I am always quite confused and wonder: Wait, but I downloaded and flashed Plasma Mobile, right? Fortunately, that visual confusion does not last after entering the pin/password to login.

[^3]: Unless, of course, you know how to do the necessary systemd-service magi. At least initially, Phosh may [keep requiring tinydm with systemd](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests/6150), alternatively, [it may actually get all this greetd goodness by default on systemd and openrc](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests/6106).
