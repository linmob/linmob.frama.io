+++
title = "Weekly GNU-like Mobile Linux Update (41/2024): Successful moves and XMPP calling"
date = "2024-10-13T21:45:27Z"
draft = false
[taxonomies]
tags = ["Phosh", "Sxmo", "Sailfish OS", "Ubuntu Touch", "Mobile Friendly Firefox", "Kirigami", "Plasma desktop", "mikroPhone",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

This Week: postmarketOS moved to their own gitlab, a new app for XMPP calls, Plasma 6.2 bringing Alpine apks to Discover, improved Kirigami Addons, news from the GNOME Foundation, updates to user0's Mobile Friendly Firefox customizations and more!
<!-- more -->
_Commentary in italics._


### Worth Reading
- srini's blog: [Distrohopping on Librem5](http://srinicame.blogspot.com/2024/10/distrohopping-on-librem5.html)
- The Privacy Dad: [PostmarketOS (Phosh) on the PinePhone in 2024](https://theprivacydad.com/postmarketosphosh-on-the-pinephone-in-2024/)
- JFs Dev Blog: [Running OpenWRT on the Pine64 LoRa Gateway](https://codingfield.com/blog/2024-10/running-openwrt-on-the-pine64-lora-gatewat/)
- Anjan Momi Homepage: [Introducing Honeybee: Calling via XMPP](https://momi.ca/posts/2024-10-09-honeybee.html). _CLI, baby!_
- Liliputing: [mikroPhone project is designing a smartphone with an emphasis on openness and privacy](https://liliputing.com/mikrophone-project-is-designing-a-smartphone-with-an-emphasis-on-openness-and-privacy/)

#### Fluff
- Purism: [True Choice for Smartphone Privacy and Security](https://puri.sm/posts/true-choice-for-smartphone-privacy-and-security/)

### Worth Watching
- FogyVerse: [PostmarketOS in 120 Seconds](https://www.youtube.com/watch?v=Dsk0xPgnBq8)
- CyberPunkEdu: [Kali Nethunter Pro Phosh Overview and Tips - OnePlus 6t fajita sdm845 (2024-10-04)](https://www.youtube.com/watch?v=nTe8f_3_7Dc)
- Level 2 Jeff: [This device makes Meshtastic the BEST off-grid tech](https://www.youtube.com/watch?v=2Ry-ck0fhfw)
- mutantCybernetics: [Charging vs system power consumption #mutantc #18650 #retrogaming](https://www.youtube.com/watch?v=cUc8waOczpg)
- EasyAudio: [Cyberthink: The Story of a Cursed Cyberdeck (ThinkCentre M73 Tiny + Upgrade)](https://www.youtube.com/watch?v=FQPUUy8m054)

### Worth Noting
- Chris: [Waking my #Librem5 from suspend when a new #matrix message triggers a notification. Starring:…](https://chrichri.ween.de/o/481d44c6609f4f44ab8ba5ab9c0bd480)
- user0: [" Mobile-Friendly-Firefox (Friendly-Fox) Updates: Firefox-ESR 128 update! The Tab Manager Menu is better than ever, now featuring container tab indicators thanks to X-m7 . Another wonderful development is that this new version of Firefox includes a fix for a pesky bug. With the new fix, the Tab Manager Menu is now stable, and with the addition of container tab indicators, Firefox on mobile Linux devices possesses yet another feature to show off which is not available on Android/iOS devices. I hope everyone enjoys the new update!…"](https://fedia.io/m/FirefoxCSS/p/925838)
- user0: ["Mobile-Friendly-Firefox (Friendly-Fox) Updates: Support for Firefox-ESR 115 ends with this release (Version 2.11.2).…"](https://fedia.io/m/FirefoxCSS/p/924891)
- Purism community: [Simple Firefox customizations for the Librem 5](https://forums.puri.sm/t/simple-firefox-customizations-for-the-librem-5/26602)
- sovtechfund: ["Would you work for free? 🤯For 33% of the respondents to our open source maintainer survey, this is reality: they are not paid or not paid enough to make a living.With the new Fellowship program, we are investing directly in the people behind the code by paying maintainers of important open source components for their work. Applications are accepted until October 20th. Find more insights about the maintainer survey on our website."](https://mastodon.social/@sovtechfund/113288250296961457)
- okias: ["Fixed one #Mesa3D test and enabled tests in the #Alpine packaging. With the next releases, we can be sure no regressions sneak into Alpine or #postmarketOS. For now, some tests are failing on older 32-bit ARM, but otherwise, everything looks good!"](https://floss.social/@okias/113297471029600905)
- flypig: ["The latest #SailfishOS community newsletter from @jolla is now out, packed full of news!Info about the C2 📱 Sailfish OS 5.0 🐟 and the Sailathon 2024 hackathon in Prague ⛵ 🇨🇿  Plus of course the repository 🐙 and app 📲 roundups!https://forum.sailfishos.org/t/sailfish-community-news-10th-october-2024-factory-visit/20284"](https://mastodon.social/@flypig/113282134556280620)
- pinchartl: ["Nice improvements to the #libcamera #camshark development application, showing live graphs of camera algorithms internal data. Debugging 3A issues is getting much easier."](https://floss.social/@pinchartl/113279429934434377)
- Blort: ["Stoked that #Verso Browser (based on @servo 's #Servo engine ) got their application greenlit by @nlnet !https://nlnet.nl/project/Verso-Views/It's very early days, but we really need work being done on a #FOSS browser with a non #Blink / #Chromium render engine, as a lifeboat for the burning ship that is @mozilla #Firefox."](https://social.tchncs.de/@Blort/113276541376506416)
- trini: [#U-Boot 📢 -- U-Boot v2024.10 has been released: https://lists.denx.de/pipermail/u-boot/2024-October/566393.html and the next branch will be merged to master soon. Thanks for the hard work everyone!](https://floss.social/@trini/113266733874990567)
- r/MobileLinux: [RISC-V mainboard for pinephone?](https://www.reddit.com/r/mobilelinux/comments/1fyl4n3/riscv_mainboard_for_pinephone/)
- Lemmy - linuxphones: [mikroPhone project is designing a smartphone with an emphasis on openness and privacy](https://lemmy.ml/post/21142142)
- #LinuxMobile: [@me Nice write up. To wake up the phone one can also do `ntfy &lt;topic&gt; send-sms.sh` and then mark the number that sends the SMS as silent in #chatty.The SMS can be sent from the UP server via an old phone or via services like text-belt.(that's what I'm using atm to get my devices to sleep longer).#LinuxMobile #UnifiedPush #ntfy](https://social.librem.one/@agx/113298881183607701)
- r/linux: [We need a real GNU/Linux (not Android) smartphone ecosystem](https://old.reddit.com/r/linux/comments/1fx5fq0/we_need_a_real_gnulinux_not_android_smartphone/)
- r/linux: [What phone to buy](https://old.reddit.com/r/linux/comments/1fzl8tn/what_phone_to_buy/)
- r/linux: ["Google is preparing to let you run Linux apps on Android, just like Chrome OS"](https://old.reddit.com/r/linux/comments/1g1esfs/google_is_preparing_to_let_you_run_linux_apps_on/)

#### Events
- igalia: ["#XDC2024, THE #OpenSource Graphics conference, starts tomorrow in Montreal 🇨🇦 with no less than 6 contributions from Igalians. Full schedule: https://indico.freedesktop.org/event/6/timetable/#all.detailed Please come say hi if you're around! YouTube live streams here: https://www.youtube.com/c/XOrgFoundation"](https://floss.social/@igalia/113272895676026882)
- kde: ["What would you ask our KDE goal champions?"Can I create a KDE app in [insert favorite programming language here]?", "Will you support my [insert exotic input device here]?", "Can I get a job as a KDE contributor?"...Let us know in the comments what you want to ask our champions and we will make sure your question gets answered.Join KDE's live-streamed AUA on Oct. 20 at 18:00 UTC:  https://tube.kockatoo.org/w/2tAyknEQc8EhL2AyoAUE8M@kde@lemmy.kde.social"](https://floss.social/@kde/113284361449327279)
- craftyguy: [It's official! #postmarketOS will be attending #SeaGL this year on Nov 8-9 in #seattleCome hang out with Arnavion and I at our table and see some phones running real Linux. There will even be a pmOS talk by @anjan Hope to see folks there!https://seagl.org#seagl2024](https://freeradical.zone/@craftyguy/113284709833204374)

### More Software News

#### Gnome Ecosystem
- This Week in GNOME: [#169 Wrapped Boxes](https://thisweek.gnome.org/posts/2024/10/twig-169/)
- GNOME Foundation: [Update from the Board: 2024-10](https://foundation.gnome.org/2024/10/07/update-from-the-board-2024-10/)
- GNOME Foundation: [2024-2025 budget and economic review](https://foundation.gnome.org/2024/10/10/budget-and-economic-review/)
  - pabloyoyoista: ["It might have gone a bit under the radar given there has been a lot of #gnomefoundation related news these last weeks, but in the transition to gitlab's wiki, there's now a section with Board's policies: https://gitlab.gnome.org/Teams/Board/-/wikis/Policies#board-member-policies. We hope that helps with transparency and build more trust on directors' goals and duties. As a bonus, they should help new candidates next year understand what being a member is. Thanks @allanday for the great work on it!#GNOME"](https://social.treehouse.systems/@pabloyoyoista/113293980407771801)
- hughsie: [Making it easy to generate fwupd device emulation data](https://blogs.gnome.org/hughsie/2024/10/11/making-it-easy-to-generate-fwupd-device-emulation-data/)
  - hughsie: [tl;dr: we've added some new UI to gnome-firmware to make recording devices easier.](https://mastodon.social/@hughsie/113289582746112396)

#### Phosh
- Guido Günther: [The OSK panel in mobile settings is becoming more complete. After adding layout selection last cycle, selecting the default text completer is on its way now too. The data is provided by the OSK so new completers are picked up automatically.](https://social.librem.one/@agx/113284617342891265)
- Guido Günther: [MR: https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/merge_requests/161Related p-o-s MR: https://gitlab.gnome.org/guidog/phosh-osk-stub/-/merge_requests/162](https://social.librem.one/@agx/113284629766978971)

#### Plasma Ecosystem
- Nate Graham: [This week in Plasma: 6.2 has been released!](https://pointieststick.com/2024/10/11/this-week-in-plasma-6-2-has-been-released/)
  - Phoronix: [KDE Developers Fixing Initial Bugs From Plasma 6.2](https://www.phoronix.com/news/KDE-Plasma-6.2-Fixing-Bugs)
- KDE Blogs: [This Week in KDE Apps](https://blogs.kde.org/2024/10/13/this-week-in-kde-apps/)
- KDE Announcements: [KDE Ships Frameworks 6.7.0](https://kde.org/announcements/frameworks/6/6.7.0/)
- KDE Announcements: [KDE Gear 24.08.2](https://kde.org/announcements/gear/24.08.2/)
- KDE Announcements: [Plasma 6.2](https://kde.org/announcements/plasma/6/6.2.0/)
- Volker Krause: [KDE Android News (October 2024)](https://www.volkerkrause.eu/2024/10/12/kde-android-news-oct-2024.html)
- Carl Schwan: [Kirigami Addons 1.5](https://carlschwan.eu/2024/10/10/kirigami-addons-1.5/)
- Qt Blog: [Qt for Python release: 6.8 is out now!](https://www.qt.io/blog/qt-for-python-release-6.8)
- kde: ["Explore the KDE Stack at our in-depth training session.Follow @ervin on a deep dive into KDE Frameworks and how Plasma uses them, and learn how to build Qt and KDE apps.Where: https://meet.kde.org/b/far-lb5-yjy-xarWhen: Oct. 20, 14:00 UTC#qt #cpp #programming #dev #kdegoals@kde@lemmy.kde.social"](https://floss.social/@kde/113276942214195123)

#### Sxmo
- fdlamotte: ["I've received a refurbished #pixel3a ...I have installed #postmarketos / #sxmo on it and it "mostly works" even with no device profileI now need to figure out how to tweak callaudio to make calls ... it works on gnome mobile, but on sxmo, it fails to switch to "call mode" ...  on pavucontrol I only see a "Voice calls" profile (same on gnome mobile but works there)I should have some time next week to try and figure out, but by then, if someone has an idea, they are welcome ;)"](https://mamot.fr/@fdlamotte/113278389342512926)
- fdlamotte: ["If I compare #pixel3a with #oneplus6 on #sxmo #op6 has gps, status led, tristate button and better cpu#p3a has better sound in calls, cameras starting to work, no notch, smaller sizeTwo very good phones to use on #linuxmobile I won't have much time to play this weekend, but I've kept my sim in the #pixel3a for now ;)"](https://mamot.fr/@fdlamotte/113294347711727887)
- #sxmo: [Small update on #sxmo on #pixel3a * Calls work very well with the forecoming ucm profiles* sensors working through iio proxy, sxmo scripts to use them for proximity works* wifi and 4g data ok* front camera works in megapixel preview, won't take photo but this should evolve quickly* gps enabled but not sending data/status (as with op6 from wrong android rom)](https://mamot.fr/@fdlamotte/113294323285204926)
- PINE64official (Reddit): [Anyone also getting PinePhone GPU crashes on Sway/SXMO?](https://www.reddit.com/r/PINE64official/comments/1fynpbv/anyone_also_getting_pinephone_gpu_crashes_on/)

#### Sailfish OS
- Community News - Sailfish OS Forum: [Sailfish Community News, 10th October 2024 - Factory Visit](https://forum.sailfishos.org/t/sailfish-community-news-10th-october-2024-factory-visit/20284)
- Jolla Blog: [Sailathon 2024: A journey of Innovation, Community Collaboration and Meetup in Prague](https://blog.jolla.com/sailathon-2024-a-journey-of-innovation-community-collaboration-and-meetup-in-prague/)
- flypig's Gecko log: [My Browser History](https://www.flypig.co.uk/list?list_id=1203&list=gecko)
- flypig's Gecko log: [Reviewing My Browser History](https://www.flypig.co.uk/list?list_id=1202&list=gecko)
- #sailfishOS: [Test drive the new browser for #SailfishOS by @flypig Thanks a lot for it 😍](https://kanoa.de/@decorum/113258538328704395)
- piggz: [A somewhat more impressive screenshot of a Qt6/Kirigami2 application running on #SailfishOS. @kde](https://fosstodon.org/@piggz/113265431062472043)
- piggz: ["So, we got audio working in Qt6 on #SailfishOS this evening, meaning my @kde kirigami #subsonic client basically works!"](https://fosstodon.org/@piggz/113279670531898310)

#### Ubuntu Touch
- UBports - Development - Issues: [Tracking issue for minor release: 20.04 OTA-6](https://gitlab.com/ubports/development/project-management/-/issues/11)

#### Distributions
- postmarketOS: [🚚 The migration to our self-hosted gitlab instance at https://gitlab.postmarketos.org is mostly done. See the status update here: https://status.postmarketos.org/issues/2024-10-06-migration-to-gitlab.postmarketos.org/Accounts are being approved. Let us know if you run into issues in the postmarketos-devel channel!](https://fosstodon.org/@postmarketOS/113262603826083874)
- newbyte: ["If you use #pmbootstrap when working with #postmarketOS, I'd welcome any testing results for migrating your work directory with https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/merge_requests/2429. Just checkout the branch and try to run $ pmbootstrap init and it'll prompt you for whether to migrate. Even successful test results are nice to see as they build confidence in that it works properly!"](https://mastodon.nu/@newbyte/113271720369989891)
- postmarketOS pmaports issues: [Epic: Lomiri UI support](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues/3241)
- postmarketOS pmaports Merge Requests: [Draft: device-postmarketos-trailblazer: add support for pine64 pinephone](https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests/5703)
- postmarketOS Wiki New Pages: [Steam](https://wiki.postmarketos.org/wiki/Steam)
- postmarketOS Wiki Recent Changes: [GNOME apps](https://wiki.postmarketos.org/index.php?title=GNOME_apps&diff=64673&oldid=64436)

#### Apps
- LinuxPhoneApps.org: Apps: [Calculator](https://linuxphoneapps.org/apps/dev.edfloreshz.calculator/)
- LinuxPhoneApps.org: Apps: [Uno Calculator](https://linuxphoneapps.org/apps/uno.platform.uno-calculator/)
- LinuxPhoneApps.org: Apps: [< polycule >](https://linuxphoneapps.org/apps/business.braid.polycule/)
  - braid: ["< polycule > #matrix client is now available on #AlpineLinux and #postmarketOS @postmarketOS !It's build for touch and keyboard input, perfectly adopts to handheld devices on Linux and finally offers you a geeky but yet Linux mobile optimized [matrix] experience as well as nice accessibility integration.https://pkgs.alpinelinux.org/packages?name=polycule#PolyculeClient #LinuxMobile"](https://alpaka.garden/@braid/113276065629145919)
- LinuxPhoneApps.org: Apps: [Rosary](https://linuxphoneapps.org/apps/io.github.roseblume.rosary/)
- postmarketOS: Apps by Category: [CalcProgrammer1: /* Game Hubs */  add Steam](https://wiki.postmarketos.org/index.php?title=Applications_by_category&diff=64606&oldid=prev)

##### FEX Gaming Corner
- @CalcProgrammer1: [Subnautica runs on postmarketOS with FEX!  Performance isn't great but it's sort of playable, feels like 20 FPS or so.  Defaulted to medium graphics preset at 1280x800.  Had to enable a 4GB swapfile to get it in game.  Pretty sure this game uses DXVK.#postmarketOS #steam #gaming #phosh #linuxmobile #mobilelinux](https://mastodon.social/@CalcProgrammer1/113297007168343797)
- @CalcProgrammer1: [Half Life 2 Lost Coast benchmark got 34 FPS on FEX-Emu running on OnePlus 6T with default settings, 200% screen scaling on Phosh.#postmarketOS #phosh #steam #gaming #linuxmobile #mobilelinux](https://mastodon.social/@CalcProgrammer1/113290755230066133)
- @CalcProgrammer1: [Half Life 2 Deathmatch works great on #postmarketOS #Phosh with FEX-Emu and Distrobox!  Runs smoothly on my Xiaomi Pad 5 Pro with the pogo pin keyboard and a Bluetooth mouse!](https://mastodon.social/@CalcProgrammer1/113289858528609830)
- @CalcProgrammer1: [SKYRIM!  It runs at like 10fps, barely even considerably playable but it is working!  I think it's CPU bottlenecked by emulation as lowering resolution didn't help.  Running on GNOME Desktop this time.  Still using FEX Emu on Distrobox on postmarketOS on Xiaomi Pad 5 Pro.#postmarketOS #steam #gaming #linuxmobile #mobilelinux](https://mastodon.social/@CalcProgrammer1/113297557180117697)

#### Kernel
- phone-devel: [[RFC PATCH v2 1/2] mfd: 88pm886: add the RTC cell](http://lore.kernel.org/phone-devel/20241012193345.18594-1-balejk@matfyz.cz/)
- phone-devel: [Re: [PATCH] arm64: dts: qcom: sm6350: Fix GPU frequencies missing on some speedbins](http://lore.kernel.org/phone-devel/172831116178.468342.13422970631298352794.b4-ty@kernel.org/)
- phone-devel: [Re: [PATCH] arm64: dts: qcom: qcm6490-fairphone-fp5: Add thermistor for UFS/RAM](http://lore.kernel.org/phone-devel/172831116179.468342.218023456132949403.b4-ty@kernel.org/)
- Phoronix: [DRM_Log Continues To Be Worked On As New Boot Logger For Kernel Messages](https://www.phoronix.com/news/DRM_Log-Linux-v4)
- Phoronix: [Linux 6.13 To Drop Some Old & No Longer Maintained Staging Drivers](https://www.phoronix.com/news/Linux-6.13-Dropping-Old-Drivers)
- Phoronix: [Arm Exploring IO_uring For Graphics Drivers For Better Performance & Synchronization](https://www.phoronix.com/news/DRM-Graphics-Drivers-IO_uring)

#### Stack
- Phoronix: [Mesa 24.3 Allows Rusticl On Asahi Gallium3D By Default](https://www.phoronix.com/news/Rusticl-Asahi-Default-Mesa-24.3)
- Phoronix: [Mesa NVK Vulkan Driver Adds VK_KHR_fragment_shading_rate Support](https://www.phoronix.com/news/NVK-Fragment-Shading-Rate)
- Phoronix: [Wayland Protocols 1.38 Brings System Bell, FIFO & Commit Timing Protocols](https://www.phoronix.com/news/Wayland-Protocols-1.38)
- Phoronix: [Apple Silicon On Linux Now More Capable For Gaming With Latest Mesa + Steam On FEX](https://www.phoronix.com/news/Apple-Silicon-Linux-Gaming)
- Phoronix: [Open3D Engine "O3DE" 24.09 Released With Performance Improvements & More](https://www.phoronix.com/news/Open-3D-O3DE-24.09)

#### Matrix
- Matrix.org: [This Week in Matrix 2024-10-11](https://matrix.org/blog/2024/10/11/this-week-in-matrix-2024-10-11/)
- Matrix.org: [Matrix v1.12 release](https://matrix.org/blog/2024/10/07/matrix-v1.12-release/)
- haui: ["The fact that #mautrix bridges work this good is insane. I ran into my first disconnect in 6 months on the mautrix/whatsapp bridge. I got a proper error message, logged back in and my stuff works again.I dont think people who can operate a matrix server have  any trouble running a mautrix bridge.There are also bridges for discord, facebook messenger, instagram, signal, telegram and others.https://github.com/mautrix"](https://mastodon.giftedmc.com/@haui/113276684769091843)


### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

