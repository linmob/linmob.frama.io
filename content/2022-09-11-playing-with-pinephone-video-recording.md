+++
title = "Sunday Morning Experiment: Video recording on the PinePhone"
date = "2022-09-11T13:59:00Z"
updated = "2023-10-09T18:45:00Z"
draft = false 
[taxonomies]
tags = ["PinePhone", "PinePhone Camera", "Video Recording", "ffmpeg", "tinkering",]
categories = ["projects", "howto"]
authors = ["peter"]
[extra]
update_note = "Added Update 2023-10-09 section about GUI for video recording, added Update 2023-10-03 section about a published video recorded on PinePhone."
+++
This morning, while in the process of a lengthy sunday-morning wake-up process, I figured: Let's see if I can't get that PinePhone video recording script I downloaded recently to work on postmarketOS. After being successful, I felt that documenting how I did this might be helpful.

<mark>Just to be clear: This article is about the AllWinner A64 powered original PinePhone, NOT the PinePhone Pro</mark>

<!-- more -->
## The issue at hand
PinePhone video recording? Why would you want that? I don't really know either. On the other hand, it somewhat upset [^1] me when I read the title of a recent reddit post on the topic: [When are they going to add video recording to the default OS? It’s ridiculous it can’t take video out of the box 📦](https://teddit.net/r/PinePhoneOfficial/comments/x6q14m/when_are_they_going_to_add_video_recording_to_the/).

I knew that people had managed to record video with the PinePhone before, but personally I had never bothered. I had downloaded scripts, but device names did not match up, and I moved on. After all, 720p at 30FPS would not be enough for todays content creation for YouTube & co.

I left some comments on the thread and browsed [another reddit thread](https://teddit.net/r/PINE64official/comments/olyqor/how_i_record_video_on_my_pinephone_in_tolerable/) (a video recording success story), and downloaded [a fairly recent script](https://svn.calcforge.org/viewvc/pinephone-videorec/record.sh?view=markup)[^2], which records video in 720p, adjusts rotation and compresses it afterwards by reencoding it to VP9 and opus in a .webm container. Being made for Manjaro, it (as I had expected) did not work right out of the box after installing the necessary packages:

~~~
sudo apk add ffmpeg v4l-utils
~~~

I gave up then, and tried again this morning.

## Adjusting the script

The first adjustments I had to make were to line 8 and 9. I figured this out by using the well known and simple command `ls` :

~~~~
ls /sys/bus/iio/devices
~~~~
listed three devices: iio:device0, iio:device1, and iio:device3.

I then used `ls` to show the content of these "device folders" and figured out that it had to be iio:device1 on postmarketOS and changed the script accordingly.

Now, the script failed differently, something could not be linked. I did not even know where, so I added some echo statements before line 30 (echo "media-ctl does its magic now") and line 32 (echo "ffmpeg takes over").

And... it was media-ctl, sadly - I had hoped for ffmpeg, since I have used it a lot more.

I ended up reading through `media-ctl --help` to understand the error better. Apparently, device names or IDs were different here, too. To solve this, I went a way that did not require much thinking, working around my utter lack of knowledge and my not wanting to get out of bed: Just compiling [Megapixels](https://gitlab.com/postmarketOS/megapixels) with meson and ninja right on the device. I had to install a lot of packages[^3] for that, but after successfully running

~~~
meson build && cd build
ninja
~~~

I could now run `megapixels-list-devices` which told me what I needed to do: Adjusting lines 30 and 31 was necessary: `3-003c` and `3-004c` had to be changed to `4-003c` and `4-004c` respectively.

Now, I could run the script with a simple `./record.sh test`, but had forgot the way to skip recording to move on to ffmpeg (it's pressing `q`, as documented on the corresponding post on the [Manjaro forums](https://forum.manjaro.org/t/pinephone-video-recording-scripts/112993)). I looked that up, and did again. After pressing `q`, ffmpeg did it's job, which took multiple minutes. I plugged my PinePhone into its power supply and went to take a shower.[^4]


## Results

While I won't share my 21 second long video (privacy, you know), here are some metrics: 

~~~
-rw-r--r--    1 user     user      380.6M Sep 11 12:44 test.mkv
-rw-r--r--    1 user     user        7.2M Sep 11 13:15 test.webm
~~~

As you can see, video recording at 720p leads to quite large files quickly, and encoding, to free up storage, takes a lot of time. I think this could be improved by trying different encodings - x264 may be an option, as that has at least hardware support for accelerated decoding. 

The result is quite okay. I think that adding a desktop file and maybe a minimal GUI (e.g. using [yad](https://sourceforge.net/projects/yad-dialog/)) could bring this to a more user-friendly state rather quickly. But that's more than a simple sunday morning thing.

## Other projects

There are more options to record video that may be better starting points (this is not a complete list, just the projects that I know of): 
* Martijn Braam's original [python-pinecamera](python-pinecamera) script (a precursor to Megapixels),
* Adam Plumbs [pinecam](https://gitlab.com/adamplumb/pinecam).

## Conclusions

While the hardware is obviously limiting the use of video recording on the PinePhone, it's doable. Maybe this post inspires someone to take it on as a seperate project, or to integrate it into Megapixels. Maybe, you, dear reader, are the one to make it happen!


## Update, 2023-10-03: A video recorded on PinePhone

While I have not looked into this since publishing this blog post, YouTube user Flax published a video which was actually recorded on PinePhone: 
[Pinephone video recording - YouTube](https://www.youtube.com/watch?v=XvI6Qp5qXN8)

Here's the script they came up with/used: [record.sh - Pastebin.com](https://pastebin.com/LaiUS1Wg)

## Update, 2023-10-09: A GUI for video recording

[alaraajavamma](https://fosstodon.org/@alaraajavamma/111183535498991759) has put together recent video recording scripts and asimple GUI for using them which you can find on [GitLab](https://gitlab.com/Alaraajavamma/pinephone-gui-for-video-recording). Great job!




[^1]: The "When are they" is really not okay IMHO - I read questions like this as "Why can't the people that work on this hardware unpaid in their free time not work on the feature I want, without me even asking for it?".

[^2]: Since neither the [code hosting page](https://svn.calcforge.org/viewvc/pinephone-videorec/) nor the [posts on reddit](https://www.reddit.com/user/Kevin_Kofler.compact) or the [Manjaro forums](https://forum.manjaro.org/t/pinephone-video-recording-scripts/112993) indicate a license, I'll just share my adjustments to the script and not my adjusted script here.

[^3]: alpine-sdk, meson, ninja, gcc, cc, gtk4.0-dev, zbar-dev...

[^4]: I've added the a hint to my first `echo` statement so that I'll know next time.
