+++
author = "peter"
aliases = ["2011/05/no-gingerbread-update-for-the-acer-stream.html"]
date = "2011-05-02T15:40:00Z"
layout = "post"
title = "No Gingerbread Update for the Acer Stream"
[taxonomies]
categories = ["software", "commentary"]
tags = ["acer", "Acer Iconia Tab A500", "Acer Stream", "Acer support", "Android 2.3 Gingerbread", "Android problems", "Android vs Apple", "free software", "software updates"]
authors = ["peter"]
+++
According to Acer Germany's twitter account <a href="http://twitter.com/#%21/AcerDeutschland">@AcerDeutschland</a> there won't be a Gingerbread Update for the <a href="http://www.linmob.net/search/label/Acer%20Stream">Acer Stream</a>. While it certainly makes sense for Acer to discontinue support for the &#8220;Stream&#8221;, which didn't sell that well (just have a look how few posts about the Acer Stream Android forums contain), this is another sad Android support story.
<!-- more -->
Personally I really think the &#8220;Stream&#8221; is a nice device, that probably suffered from a not too handsome design - but most likely more from a poor marketing. Acer are in fact shooting themselves in their foot by discontinuing the support for a device that's not yet a year old - knowing that they discontinued the support for their &#8220;Stream&#8221; so soon, how can I possibly recommend their Acer Iconia Tab A500 which got <a href="http://www.engadget.com/2011/04/26/acer-iconia-tab-a500-review/">positive</a> <a href="http://thisismynext.com/2011/04/27/acer-iconia-tab-a500-review/">reviews</a> (BTW: when I went hands on at a local electronics store earlier today, I liked it, too)? I can't and I doubt others can. Software Updates are very important, not only to enable the user to run the latest apps, but also to fix possible vulnerabilities - who feels comfortable using a smartphone with known vulnerabilities, knowing these won't be fixed?

Especially if Android phone manufaturers really want to compete with Apple, they should provide software Updates for at least two years. Everything else is pathetic and a disaster for the whole &#8220;Android&#8221; brand.
