+++
title = "MCF11: Private Browsing Struggles"
date = "2024-04-05T21:47:19Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "LinuxMobile"]
categories = ["projects"]
authors = ["Peter"]
+++

In the days since the last progress report, I did not have much time (thanks, day job ... not). I tried to tackle fixing the private browsing menu, but have not suceeded yet.

<!-- more -->

### Yes, private browsing is visually broken

At first, I embarked to find a quick and dirty fix. I first [hid the label](https://codeberg.org/1peter10/mobile-config-firefox/commit/f570e50f6559f385894014ee02764ae9af720fc8), then got rid of the [inline-margins and reduced the width of the tiny logo](https://codeberg.org/1peter10/mobile-config-firefox/commit/70a453d96fbd5f23b5ff0ed346d1d8f01164589c). 

Worst case, this maybe something to go back to. But I was not happy with it, it looks weird. So I decided [to try something different](https://codeberg.org/1peter10/mobile-config-firefox/commit/d69306682472a658bd971e131e6dfce62b764d1a): Recognizing private browsing, and only applying the "padding-right: 30px" applied to #titlebar when private browsing is not active. I then tweaked this a bit more, but as the [commit message says](https://codeberg.org/1peter10/mobile-config-firefox/commit/0981c9d42b7ab1714447a47059e37cb3bb83844a), this only works on ESR 115, not 124. 

Fun fact: For current ESR (and thus for you, if you use it and use private mode a lot), there's a way easier non-CSS way:

Just enter `about:config` and change

~~~
browser.privatebrowsing.enable-new-indicator
browser.privatebrowsing.enable-new-logo
~~~

to `false`, and you should be fine with the current relase of mobile-config-firefox.

Sadly, for 124 this no longer works; my next avenue is trying to find a way that might involve [`:has()`](https://developer.mozilla.org/en-US/docs/Web/CSS/:has), to hide apply the padding-right when the logo is not there, or to add in the private browsing icon with `:after`somehow, somewhere.

### One positive thing

To also share something somewhat helpful, here's a finding that may help other who explore the Firefox UI. I came across a link in [this upstream issue](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/49), and found where to look up the HTML that makes up Firefox's chrome when not in dev-tools for a minute:

- [gecko-dev/browser/base/content/navigator-toolbox.inc.xhtml at master · mozilla/gecko-dev · GitHub](https://github.com/mozilla/gecko-dev/blob/master/browser/base/content/navigator-toolbox.inc.xhtml#L99-L102) (that's the navigator-toolbox, with the fun stuff I played with this week highlit),
- [gecko-dev/browser/base/content/browser-allTabsMenu.inc.xhtml at master · mozilla/gecko-dev · GitHub](https://github.com/mozilla/gecko-dev/blob/master/browser/base/content/browser-allTabsMenu.inc.xhtml) the allTabsMenu (a.k.a, the tab switcher),

and there's a lot more.

### What else happened?

- [Guido Günther has a way better theory for the 4.3.0 bug: IIRC the root cause here is in phoc/wlroots and you see it on Droidian and PureOS because they use older versions. .…"](https://fosstodon.org/@agx@librem.one/112206629009613698)
- blort [asked a very valid question](https://fosstodon.org/@Blort@social.tchncs.de/112204729529070518) I should have answered earlier (and will likely tackle in a future post), but for now, [read through my answers](https://fosstodon.org/@linmob/112205791104178781) in the thread.


That's it for today. More soon!







