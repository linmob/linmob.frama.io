+++
title = "MCF06: Some progress and a call for feedback"
date = "2024-03-23T12:09:57Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "Mobile Linux"]
categories = ["projects"]
authors = ["Peter"]
+++

Sometimes things just work out well; and you can fix things quickly.

<!-- more -->

### The Date picker

In the [previous installment](https://linmob.net/mcf05-a-fix-and-some-planning/) of this series, I had written 

> I will try to tackle [HTML date picker calendar view missing 7th column. (#63)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/63) first.

and ... I did, and could not resist [sharing a spoiler on Mastodon](https://fosstodon.org/@linmob/112135636419853033).

I've since made a [pull request to fix this issue (+ one more, fenix specific issue) on Codeberg](https://codeberg.org/user0/mobile-config-firefox/pulls/4), that tackles the issue slightly differently, with less lines of code; and I will likely propose a similar fix for upstream soon (I've decided to set up commit signing first.).

### ... and other Popups

As quick successes are always highly motivating, I kept at it. And I managed to find a fix for the issues that I found most annoying personally: Popup menus that do not open properly.

~~~~
/* Making menus usable again */
.panel-viewstack {
    height: 100vh !important;
    max-height: calc(100vh - 100px) !important;
}
~~~~

This change makes the protections menu (shield icon) and the site information menu (padlock icon) and the unified extensions menu (puzzle icon) open properly on mobile.

Combined with a [some code from user0's awesome fenix branch](https://codeberg.org/user0/mobile-config-firefox/src/branch/fenix/src/userChrome/15_dynamic_popups_pro.css#L68-L74), you can use addon menus properly, too - it often requires horizontal and vertical scrolling to reach all the buttons, but hey: Usable addons!

### Call for Feedback

That said, I would like to get some feedback on this, before I submit the MR (I will take the time to describe better, what the code does and set up commit signing, I promise).

So, here's a visual:

 <video width="360" height="740" controls>
  <source src="/media/2024/20240323-firefox-addon-fix.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 

Interested? You may/should try it for yourself:

~~~~
$ git clone https://codeberg.org/1peter10/mobile-config-firefox.git
$ cd mobile-config-firefox
$ git checkout experiment-panels
$ make 
$ sudo make FIREFOX_DIR=/usr/lib/firefox-esr install
~~~~

If you are using current Firefox instead of Firefox ESR, change the last line to:

~~~~
$ sudo make install
~~~~

Please leave your feedback by [replying to the post on fosstodon](https://fosstodon.org/@linmob/112144722624535743) or via email, thank you!

### Closing words

What's next, aside from finishing this up, after hopefully having had some feedback?
Likely boring issue reading, as [outlined in the previous post](https://linmob.net/mcf05-a-fix-and-some-planning/#planning-ahead). It may be tuesday before this series continues.

See you then! 





