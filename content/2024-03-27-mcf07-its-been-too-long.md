+++
title = "MCF07: It's been too long"
date = "2024-03-27T13:37:21Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "LinuxMobile"]
categories = ["projects"]
authors = ["Peter"]
+++

Time for another short post on my efforts to improve [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox), the project that makes desktop Firefox work on small form-factor Linux devices.

<!-- more -->

### Progress

#### Merge and Pull Requests

Currently, there is one Merge Request pending upstream:

- [Fix bookmarks menu on current Firefox (124) (!46) · Merge Requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/46), a tiny fix to the bookmarks dialog that pops up when you hit that start in the URL bar, which was harder than it looks like - porting over the implementation in the fenix branch seemed to work, but then I noticed last minute problems on Sxmo, where the dialog covered the keyboard, so I had to solve it differently.

Two pull requests for fenix have been merged since the last post:

- [#4 - Fix date picker and spacing - user0/mobile-config-firefox - Codeberg.org](https://codeberg.org/user0/mobile-config-firefox/pulls/4),
- [#5 - Fix 'allow this extension to run in private mode' dialog - user0/mobile-config-firefox - Codeberg.org](https://codeberg.org/user0/mobile-config-firefox/pulls/5).

#### Feedback on [MCF06](https://linmob.net/mcf06-some-progress-and-a-call-for-feedback/#call-for-feedback)

Also, feedback on the fixes proposed in the last post has been positive, so the next MR hitting upstream will be including these; I've decided to start the MR branch only when the current MR has been merged into master.

I've since added a few more fixes since, that fix [Cannot install add-ons in portrait mode (#58) · Tickets · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/58) on ESR and current (124). I aim to include these into the next MR upstream, too.

#### Work on open issues

I have also started a document, in which I've sorted the open issues by the following criteria: 

- Are adjustments to userChrome, userContent or about:config needed?
- Is the issue solved in user0's fenix branch?
- Is this an issue I think I can solve/that can be solved with mobile-config-firefox?

I will not share my notes at this point, as they definitely need polish to be widely understandable.

As part of that, I have left a few comments on issues, and aim to comment on more:
[Address bar: do not show "https://www." (#14) · Tickets · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/14#note_1830810846)
* [Menus are unusable after switching from landscape to portrait on phosh (#4) · Tickets · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/4#note_1829119413)
* [Tabs in firefox portrait mode theme should be a third or less of the minimum width that they are now (#42) · Tickets · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/42#note_1831348998).


### What's next?

After the next MR, which again, will be put together once the current one is merged, I will be taking a bit of a break, as I need to spend some time on [LinuxPhoneApps.org](https://linuxphoneapps.org/). This might be an opportunity to make a release of mobile-config-firefox ;-)

I plan to create separate branch to understand fenix better, based upon then current upstream master branch. For that, I will also first into deduplicating a userChrome.css file compiled of all the fenix files, as there's a bunch of stuff specified multiple times, which makes the way fenix actually achieves what it does harder to understand. The resulting files will then be matched up with the current code, to hopefully have clearer diffs of what's different where.

Why go through all of that? Because there are some things I just fail to understand. In fenix, the extensions can use almost the entire height of the screen in portrait, which makes using even more complex extensions like uMatrix a breeze. I have managed make it so that add-ons open again, but I fail to make it so that the entire screen is being used.

Another interesting thing I hope to find the time to is to work on [Make moving the address bar to the bottom optional (#51) · Tickets · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/51). It's definitely challenging, but I look forward to seeing how many lines of userChrome.css can be dropped if we break out all the customizations that are necessary for a bottom address bar (which, is a choice I stand by, as 6" phones are just too big for having all the bottoms up top — unless you actually like breaking your phone).

Despite all the "I'll take a break" rhetoric, I hope to post on Thursday. Writing these posts really helps me sort my thoughts.


