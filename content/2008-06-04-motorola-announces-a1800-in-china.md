+++
title = "Motorola announces A1800 in China"
aliases = ["2008/06/motorola-announces-a1800-in-china.html"]
date = "2008-06-04T13:06:00Z"
[taxonomies]
categories = ["hardware"]
tags = ["a1800", "Motorola",]
authors = ["peter"]
+++
If you are interested in new cellphones as much as I am, you'll have already heard of Motorolas A1800 mobile phone.

There is a<a href="http://translate.google.de/translate?u=http%3A%2F%2Fwww.motorola.com.cn%2Fa1800%2Fspecifications.asp&amp;sl=zh-CN&amp;tl=en&amp;hl=de&amp;ie=UTF-8"> product page</a> on Motorolas' chinese website and a <a href="http://www.youtube.com/watch?v=sYJyc8-n6Q4">chinese tv commercial on youtube</a>.

Seems to be an interesting device, but not for europe, as there are no CDMA (only WCDMA) networks here and I am not sure whether this phone is capable of GSM dual sim.

It features a 3 megapixel camera, memory extension via microSD and AGPS, to name the really interesting ones, its looks remind me of the Ming.

<a href="http://translate.google.de/translate?u=http%3A%2F%2Fpinguinsmoveis.com%2F&amp;sl=es&amp;tl=en&amp;hl=de&amp;ie=UTF-8">Source</a>

BTW: I will write something about further A910 plans soon.

__Update:__ Motorola officials say, that the __A1800 does not run Linux__ (but hey, the <a href="http://www.motorola.com.cn/a1600/">A1600</a> and the <a href="http://direct.motorola.com/hellomoto/ZN5/">ZN5</a> do ;) ).
