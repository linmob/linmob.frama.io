+++
title = "Enter Beepy, Esc"
draft = false
date = 2023-12-02
[taxonomies]
authors = [ "Peter",]
categories = [ "hardware", ]
tags = [ "Beepy", "RaspBerry Pi", "QWERTY", "handheld", "BeepBerry", "low power displays",]
[extra]
edit_post_with_images = true
+++

_This is a blog post that has been in draft state since early September, ... and since "finishing it properly" is unlikely, here it is with an overhauled conclusion and additional links._

Beepy is a tiny hardware device, that was originally called "BeepBerry", [see Weekly Update 20](https://linmob.net/weekly-update-20-2023/#hardware).[^1] Beepy is a joint project by [Beeper](https://www.beeper.com/) (who provide a Matrix-based chat app that bridges many other services, and can be used with any Matrix app that supports E2EE) and [SQFMI](https://sqfmi.com/), known for the eInk watch [Watchy](https://watchy.sqfmi.com/).

<!-- more -->

## What's a Beepy?

Now, what is it? As its original name suggests, it's a gadget in a classic blackberry form factor, it even uses a keyboard originally made for Blackberry devices. That keyboard is located below a wider-than-high "landscape orientation" screen, which is 400 px wide and 240 px high. Low resolution aside, it's only greyscale, since this is a sharp memory LCD, commonly used in calculators. It's also not backlit or frontlit, meaning, this is a device for daytime use.[^2]

<figure>
    <img src="Beepy_(Shot_on_PinePhone).jpg" alt="Beepy, next to a 3D printed case, photographed on PinePhone" title="Beepy, next to a 3D printed case, photographed on PinePhone. Yes, it's been so long that I forgot username and password." style="height:auto; margin:0;">
    <figcaption>Beepy, next to the 3D printed OG case.</figcaption>
</figure>

You may wonder why technical details like SoC/CPU and RAM are still not mentioned. That's because Beepy is just a board around a RP2040 MCU, that you may plug a Raspberry Pi Zero, Zero 2 or a compatible SBC in via a solderless header (see the official [specs page](https://beepy.sqfmi.com/docs/hardware/specs) for further details).

### Boards that can/could power Beepy.

I have been using a boring Raspberry Pi Zero W, that I already had and did not use.[^3] It was possible to order Beepy with a Pi with a later shipping date. They will be/are being shipped with a Raspberry Pi Zero 2 W, which is a lot more powerful due to having four ARM Cortex A53 cores instead of single ARMv6 core.

There are other options that one might want to consider:

- [Banana Pi BPI-M2 Zero](https://wiki.banana-pi.org/Banana_Pi_BPI-M2_ZERO), featuring an AllWinner [H3](https://linux-sunxi.org/H3) (Quad-Core ARM Cortex A7 (ARMv7)) SoC,
- [Mango Pi MQ-Pro](https://mangopi.org/mqpro), featuring an AllWinner D1 (Single-Core C906 (RISC-V 64-bit)) SoC,
- [Mango Pi MQ-Quad](https://mangopi.org/mangopi_mqquad), featuring an AllWinner [H616](https://linux-sunxi.org/H616 SoC) (Quad-Core Cortex-A53 (ARMv8)) SoC,
- ... and many more RPi Zero clones.

IMHO, when choosing a single board computer most important two questions to ask are:

- how much performance do I actually need? and
- how complicated is this going to be?

The latter should then spawn a few follow-up questions like:
How well is that SBC supported? How many distributions support it? Can you boot a (close to) mainline Linux kernel, or does it require dealing with (potentially outdated) vendor-provided images?

All in all, for most people, one of the Raspberry Pi options is likely the best choice, especially given the current state of the software.

### Getting started / Diving into the software side of things

When getting started, I followed the official getting started guide and had bad luck - just check out [my Mastodon thread](https://fosstodon.org/@linmob/110962857882621663). I thus seriously recommend following forks of this guide:

- [Official Guide](https://beepy.sqfmi.com/docs/getting-started)
- [Fork](https://github.com/ardangelo/beepy-docs/blob/main/docs/getting-started.md)
- and of course there's a helpful pinned message on Discord[^4], which you can't really access in the bridged Matrix channels.

_Advice: Don't mix and match guides, unless you feel like troubleshooting. Not all combinations of the keyboard driver and the keyboard/RP2040 firmware work well together, you may end up with eeeee on a short e press, making plain login almost impossible._

In most cases (early on it were all cases), you start with Raspberry Pi OS Lite, follow a "getting started guide", and then build upon it by customizing your environment, e.g., by installing and configuring tmux in a way so that it shows you the current battery level, installing a few apps (mostly <abbr title="Command Line Interface">CLI</abbr>, but a few people have toyed with lightweight GUI frameworks such as LVGL).

If you want to use Beeper (or any Matrix server of your choice) on the device, you can then follow [this guide](https://beeper.notion.site/Beepy-Beeper-Client-Setup-Tutorial-a2200b76f8764813bf7a70e9f69f46b3) to setup [gomuks](https://maunium.net/go/gomuks/) properly. I have not done this yet, as procrastinating with [moon-buggy](https://www.seehuhn.de/pages/moon-buggy.html) is so much more fun.

To get an idea of what's happening, you may want to search common code forges (terms: beepy, beepberry), or you have to take part in the Discord.

I have not really done much - I have adopted a .tmux.conf featuring battery info, and played around with "light mode" (dark text on "white" background), but that's pretty much it.

### DIY: Cases

I had the [O.G. case (the v1) 3d printed](https://github.com/sqfmi/beepy-hardware/tree/main/3D/beepy-cases/v1) printed[^5] - I do not recommend it. [This should be a better choice.](https://github.com/a8ksh4/beepberry-hardware/tree/snap_case/3D/beepberry-cases/snap_flat/)  

### Flaws

Aside from the messiness of the ecosystem (see above), and the limitations of a 400x240px greyscale display, Beepy, in it's current form, has a few more flaws.

#### Slow charging

By default, Beepy is built to charge it's 2000mAh battery at 100mA, which at least by my flawed math suggests that a full charge is going to take about 20 hours. This can be fixed, as the following blog post explains, but you better be comfortable with (de-)soldering:

* [Beepy charge mods for infinite battery life - M0YNG.uk](https://m0yng.uk/2023/09/Beepy-charge-mods-for-infinite-battery-life/)

The post also contains mods for wireless charging, if that's your thing.

#### Battery life

This device features a Sharp Memory LCD and a 2000mAh battery, so you may think, that battery life is going to be a non-issue, especially with a 1st-gen Pi Zero. At least with a Raspberry Pi's, it is not really - these devices do not support a suspend mode, which almost makes the aforementioned Banana Pi BPI-M2 Zero with it's [AR100](https://linux-sunxi.org/AR100) core and [working beta Crust support](https://github.com/crust-firmware/crust) an attractive choice.[^6]

If you wonder: How long does it last? I honestly did not do measurements, it's less than a day, I'd say - this section just has the purpose of telling you: No, this is not like your E-Reader, you can't just let it sit and expect to push its button and still have a charge. 


### Conclusion

Beepy is a fun thing, that can be hacked upon in various ways, and that - currently - needs to be hacked upon to be truly useful (assuming you can imagine a use case for this device, given it's outlined limitations), especially regarding battery life. You will definitely need to bring some time to make it work for you. It's definitely alive, as projects such as [these two](https://retro.social/@kelbot/111461402781079133) show. You can do fun things, like 3d print a case, hack on software and hardware features, and last, but not least, play `moon-buggy` on it.

Personally, I have decided that I should pass it on to someone who has more time and patience - so if you're interested, and in the EU, contact me via [email](mailto:beepy@linmob.net?subject=I%20am%20interested%20in%20your%20beepy!&body=Hi%2C%0A%0Aplease%20send%20me%20your%20Beepy.%20I%20offer%20you%20...%20EUR%20for%20it.) or find it on eBay in a few days. :-)


[^1]: The name change is due to obvious legal reasons - Blackberry is still alive and kicking, they just do other things these days.

[^2]: It has been pointed out, SQFMI and Beeper could have opted for a lit display instead.

[^3]: It had been deployed as a simple networked audio player. Eventually, the system had bugged out, and I did not redeploy it. 

[^4]: Boy, do I dislike Discord! TL;DR: This [eight second short video](https://piped.kavin.rocks/watch?v=oHhgllqSKro). Longer: I get that it somewhat of a standard modus operandi for many open source projects these days, despite its proprietary nature. [Enough has been said about why it's bad](https://drewdevault.com/2021/12/28/Dont-use-Discord-for-FOSS.html). My main gripes are 1) that there's no "client freedom", 2) that it keeps useful information from the open web and thus forces people to join it, 3) that it's an unorganized mess that is somewhat searchable, but not well structured. Usually I deal with Discord through Beeper, and that has been mostly fine, as the projects I've been following previously were smaller/less noisy, or heavily bridged (PINE64). The bridging works less good here - people frequently link to posts in other channels, which you can't really follow in the bridged environment. 

[^5]: I did this using a local 3d printing service, and it was the first I time I did such a thing.

[^6]: "Almost" because a) I am not aware of anyone having tried this board with Beepy, and b) I don't know how well this board is support in mainline Linux and u-boot.
