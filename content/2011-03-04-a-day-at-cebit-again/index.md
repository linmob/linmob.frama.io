+++
title = "A day at CeBIT. Again."
aliases = ["2011/03/day-at-cebit-again.html"]
date = "2011-03-04T08:11:00Z"
[taxonomies]
categories = ["events"]
tags = ["ASUS Eee Pad MeMO", "ASUS Eee Pad Transformer", "CeBIT 2011", "EFL", "Hanvon A116", "Huawei", "impressions", "LiMo",  "MeeGo", "open source", "personal", "platforms", "Yifang M707"]
authors = ["peter"]
+++

I am starting this very subjective write up sitting on a chair in the Webciety&#8217;s Bloggers Lounge and I feel pretty tired and exhausted - it's just as it has been the years before. You walk around all day and spend time out there, go hands on with the few devices you are interested here, talk to booth people&#8230; There are great moments and less great - this time there were few less great ones, even the sun was shining all the time, which I never experienced in CeBIT seasons Hannover.
<!-- more -->

After having strolled around many booths in many halls,  I visited a place that is a must for a guy writing about Linux, the Open Source park, which is a rather small area full of people that show off their more or less popular projects. Among many business related projects (including some I've never heard of, as I am a little bit ashamed to admit) and more popular ones like Firefox 4 and LibreOffice I stumbled on a small Enlightenment corner and there on <a href="http://en.wikipedia.org/wiki/Carsten_Haitzler">Carsten "raster" Haitzler</a>, who, as some of you may remember, was once involved into the Openmoko project. I talked to him for quite some time, first in german (as his father's german) then in english about Linux, Android, toolkits on mobile devices. As you can read on his <a href="http://www.rasterman.com/index.php?page=News">website </a>as well, he's with Samsung now, who are investing heavily into Enlightenment to build a platform on - which will actually be <a href="https://linmob.net/2011/02/mwc2011-day-1.html">LiMo r4</a> compliant (there is EFL in LiMo r4, not only GTK+, cairo, clutter and so on). He showed me some EFL demos on an obviously Samsung built device on par with Galaxy S (using Hummingbird and an (approx.) 3.7" WVGA screen) - most likely a developer model of the never released<a href="http://www.slashgear.com/samsung-i8330-aka-vodafone-h2-limo-phone-leaks-2687073/"> Samsung H2 / GT-i8330 hardware</a>, which was supposed to be released during last summer, but didn't make it (at least on Vodafone in Europe). I think I will write a dedicated article about what we talked about as soon as possible, because it really was great to talk about the downfall of MeeGo (back to Moblin, mostly - a fact he wasn't too unhappy about, working on LiMo, which is certainly a rival to MeeGo on handsets.

Besides that, I didn't hang out for too long in CeBIT's OpenSource paradise (it was so crowded!) and headed on to webciety to get a little rest while listening to some panels I didn't plan to listen too (at the one I had on my schedule, one about tablets with the MeetMobility guys + 2 others (a Qualcomm representive + @petweetpetweet) I arrived late, just to get a rest after all these tablet hands ons (in fact I used my partly broken (headset jack) Palm Pre to capture a hands on with the Hanvon A116 tablet, but I will have to edit it before uploading, so don't expect that before saturday) and walking (I liked both ASUS Android tablets I wrote about recently (the MeMO and the Transformer, of which the latter really features amazing build quality and a really great finish), and then went on to have a look at some more devices before coming back late to the panel I just mentioned. After that, I managed to talk to<a href="http://twitter.com/sascha_p"> @sascha_p</a> for a few minutes, exchange business cards and head on to Huawei, were I had a look at their Android devices, entry level and up before "winning" a Vodafone 246 dumb phone at the stand of a popular german PC magazine, which was about the last thing I did before heading out to catch my train.

If you want to, have a look at the photos I took:

{{ gallery() }}

Edit 2023-07-23: Turns out, I also took a video of some Windows Phone device:
<video src="https://media.linmob.net/2011-03-04-a-day-at-cebit-again/cimg0048.mp4" alt="Windows Phone 7?" style="margin:0 auto" controls></video>
