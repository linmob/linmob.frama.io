+++
title = "MCF05: A fix and some planning"
date = "2024-03-21T18:56:46Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "MobileLinux"]
categories = ["projects"]
authors = ["Peter"]
+++

No, I did not stop to set up "verified commits". I kept fixing issues instead.
Fortunately, next there are tougher nuts to crack, and I should also do the responsible thing, and plan ahead.

<!-- more -->

Diving in head first is so much more fun though. Looking at my mental todo list, I've assigned high priority to making all the menus that pop up, be it the unified extension menu (the one with the puzzle piece as a logo), or the protections and the site information menu (left of the URL in urlbar) usable and less glitchy.

Unfortunately, the recent MR is not really progress in that regard. It's related though: I was reading my way through [06_popups.css](https://codeberg.org/user0/mobile-config-firefox/src/commit/f2381fd45eeef10ffb173a56f8106e6d68b981be/src/userChrome/06_popups.css), when I stumbled upon a fix for [Allow opening native link dialog not adaptive (#65)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/65).

In my Merge Request [popups.css: Add fix for #65 (!44)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/44), I've simplified it down a bit more, attempting to make it so that it would work outside the form-factor media query, i.e., be more upstreamable. (It's already merged, thanks Ollie - I'll write a better commit message next time!)

I think this has to be a goal for future changes to: Make them so, that they change as little as necessary and are as generic as possible; after all, I want to respect [Figure out path to upstreaming changes (#28) · Issues · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/28) while trying to improve overall usability and fix open issues.

### Planning ahead

So what's next:
 Because I can't help myself, I will try to tackle [HTML date picker calendar view missing 7th column. (#63)](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/63) first. After that, I'll want to go through open issues and

- try to identify and eliminate possible duplicate issues (I just ran into this with [#30](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/30))
- confirm issues with `master` and `fenix` on Firefox 124 and ESR 115,
- group by similarity and order by priority (highly subjective),
- attempt fixing confirming bugs, one by one, while trying to fix similar issues at once with as few lines of code as possible,
- also, I want to have a look at the patches [Debian](https://salsa.debian.org/DebianOnMobile-team/firefox-esr-mobile-config/-/tree/debian/latest/debian/patches) and [PureOS](https://source.puri.sm/Librem5/debs/firefox-esr-mobile-config/-/blob/pureos/byzantium/debian/patches/) are applying, and see whether there's something in there, that might be worth upstreaming (or has a positive effect on present issues).

Of course, there are also other things I need to do (e.g., modify [abrigde](https://github.com/Jieiku/abridge) for this blog - I've chosen it for the featureset, not the looks - parts of which are also highly relevant for LinuxPhoneApps.org), so expect activity to slow down over time to then pick up again before Firefox 128 (the next ESR) lands.

See you soon!
