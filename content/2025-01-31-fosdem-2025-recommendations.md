+++
title = "FOSDEM 2025 Recommendations"
date = "2025-01-31T11:47:58Z"
draft = false
[taxonomies]
tags = []
categories = ["events", "shortform"]
authors = ["Peter"]
+++

Like last year, we have another [dedicated page for FOSDEM](https://linmob.net/fosdem2025/), listing what you may not want to miss.

If something is wrong or missing, please get in touch - otherwise enjoy!

<!-- more --> 

And maybe meet you there in person.
