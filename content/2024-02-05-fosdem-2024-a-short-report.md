+++
title = "FOSDEM 2024: A Short Report"
date = "2024-02-05T20:02:23Z"
updated = 2024-02-06
draft = false
[taxonomies]
tags = ["FOSDEM 2024", "Linux on Mobile", "Genode", "Mobian", "Sailfish OS", "Flutter", "Droidian"]
categories = ["events", "internal", "personal"]
authors = ["Peter"]
[extra]
update_note = "Added a sixth footnote to not forget about keyd, and a seventh about potential improvements."
+++

_This is just a loose recollection of events, mostly for myself to not forget things._

<!-- more -->

FOSDEM 2024 was my second FOSDEM I attended in person, and my first conference with a talk.[^1]. This made it very different for me, and made it, as I had way less time to prepare than I had hoped for (due to work and personal life events) quite stressful and had a negative impact on the coverage of FOSDEM on linmob.net and the Fediverse.[^2]

### Talks on FOSS on Mobile

I will not go into too much detail about the talk in this post[^3], just [watch it or go through the slides](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/) and please, get in touch if you have feedback or have an idea how to enable more activity and collaboration in the Mobile Linux App realm. Since I was on about 3 hours into the FOSS on Mobiles devroom schedule, I may have not been super attentive - and I was really done at the end.

While my talk was quite surface-level and could have used more videos/screenshots and less swearing, others went deep into the weeds of hardware quirks and software development. 

From the top of my head, the Genode talk once again was a favorite. (it could have used a longer time slot - I would love to maybe see the full thing as it was planned on a video platform (and, selfishly, need to contact Norman about which software creates these beautiful slides)). I think it's just amazing how they basically build their entire world with a small team, and how well things do work![^4]

I also very much enjoyed Oren Klopfers Talk, who really visualized the difficulties he had to overcome when porting Ubuntu Touch 20.04 to PINE64 devices very well. Speaking of Ubuntu Touch: It was sad to not have Marius Gripsgård on stage (I hope all is well and if not, best wishes!) - but Nikita and Ivan were great fill-ins.

The one with the braid talked about Flutter and the difficulties in making it build in a way that feels sane for Linux distro people. Even if you could not care less about Flutter, do watch it - she's an excellent presenter!

When listening to Arnaud's talk about Mobian, I did not know yet that I basically would not run into anyone from Mobian I got to know and like last year - hope to see you all again at another event[^5] or next FOSDEM! Back to the talk: It's amazing how few packages Mobian carries - as a person that has installed Debian on Wandboard Quad (a single board computer) while controlling the thing via serial, I firmly hope that they keep providing images, even when it's going to be even fewer packages or everything in Debian at some point in time (unless the goal is to only attract Debian die-hards with enough knowledge/perseverance).

Speaking of images, since this came up on Sunday: I think Mobile Linux Projects could reduce user frustration by providing a thumbs up/thumbs down logic on Download sites - so that people can more easily avoid images that have already failed others.

Bardia/FakeShell from Droidian was really busy: A talk and a very professional looking stand! The talk got into the details of how things work, it's quite hacky - but, to be fair, all these hacks make for a pretty good user experience and allow for features, that would take really long with mainline.

Flypig's talk about Daily blogging embedded Gecko development was amazing, too - as a person that should write more notes about how things are done, I will try to learn a lesson from it. 

That's it for the talks in this post - there was no dud, watch them all if you can, sorry for not going over every talk here - that's going to happen on the [FOSDEM2024 page](https://linmob.net/fosdem2024) later on. Wait, there's one more thing: Thanks, Guillaume, again, for that life-saving Banana! And: Ollie, you were an amazing stage manager, I am happy to help out as a Stage Manager next time!

### Day 2

Day 2 had "only" the stand, which I did not really help with. It was nice to talk to Lukasz from PINE64 EU again, and awesome to get to know TL in person. Briefly playing with a PineTab V was a nice added bonus! 

Due to not being as-impromptu, people actually showed up for my 2:00pm meetup. It was really nice - I hope, we did not miss people due to moving into one of the Cafeterias in order to avoid the drizzle. It was fun! 

It was a pleasure to talk to Alfred and Raoul from UBports - let's keep in touch! And Raffaele, all the best for you, too, it was amazing to finally meet the [competition](https://tuxphones.com/)! [^6]

The last highlight was the recording of the postmarketOS podcast - I don't want to spoil things, though, and the time to write this post is up, as Deutsche Bahn is arriving as scheduled.

I am already looking forward to FOSDEM 2024! [^7]


[^1]: I have done a lightning talk at a CCC event before, but 5 minutes non-recorded, vs. 40 minutes, full room, recorded does make a difference.

[^2]: LINMOB.net is open for everybody - I would really love to increase the bus factor this year!

[^3]: I hope to be able to do so in a later post - likely on LinuxPhoneApps.org.

[^4]: Note to self: I need to try this again on my PinePhone (the same applies to the next paragraph).

[^5]: Speaking of events: I likely will not attend [CLT ](https://chemnitzer.linux-tage.de/2024/en) and definetely not [LIT](https://www.luga.de/static/LIT-2024/) (I have another thing in my schedule, sadly). I am determined to attend [FrOSCon](https://froscon.org/) (if it's going to have a some Linux Mobile activity), and I am thinking about Mini-Guadec and Akademy: But, that said: __I gladly send out the tablecloth and a few devices to people who want to present Linux on Mobile at an event.__ I will also try to remember to translate and juice up the Augsburg 2023 flyer [I already edited for 37C3](https://framagit.org/1peter10/flyer) for that purpose - it should be moved to a code hosting platform with easier sign-ups and needs some pipelines to make PDF creation less hard.

[^6]: I am also super glad to have learned about [keyd](https://github.com/rvaiya/keyd), another tool that does things on Wayland uninformed X11 fans call impossible on Wayland!
 
[^7]: My current ideas for improvement are:
Devroom: 
- more unofficial stage managers (e.g., have a signal angel from the start),
- have a 5-10 minute start session what this room is about,
- have a wrap up session that maybe also mentions the projects that did not make it into the schedule (such as Maemo Leste or Nemo Mobile for this time) 
- have a common evening event and/or a FOSS on Mobile Birds of a Feather to get people across projects in a room to discuss things - obviously there's never enough time, but making personal connections usually makes online collaboration easier after that point.

Stand: Leaflet? Device labels?
Personal: Help out with the stand for at least two hours, get a recognisable hoodie, get a stamp with URLs and contact information or cards, get better stickers and actually place them, maybe give this blog an additional URL I do not need to spell out letter by letter, only give another long talk if the likelyhood of having the time to properly prepare it (not last minute) is actually better.
