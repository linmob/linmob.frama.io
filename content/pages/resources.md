+++
template = "single.html"
path = "resources"
title = "Resources"
updated = "2024-08-25"
[extra]
update_note = "Added Sailfish Ports list."
+++

## Hardware

This list is limited to devices reasonably well supported by a mainline kernel: 

### Smartphones
* Purism Librem 5 (mass-market release "Evergreen") or Liberty Phone (US-manufactured Librem 5 with more RAM and storage), currently supported by PureOS [default], [Mobian](https://wiki.mobian-project.org/doku.php?id=librem5&s[]=librem&s[]=5) and [postmarketOS](https://wiki.postmarketos.org/wiki/Purism_Librem5_(purism-librem5)))
* Snapdragon 845 powered devices like the Shift 6mq, OnePlus 6(T) and Xiaomi Poco F1, supported by postmarketOS, Mobian and other projects (caveat: no camera support, except soon on Poco F1),
* Google Pixel 3a (see [the postmarketOS wiki page for details](https://wiki.postmarketos.org/wiki/Google_Pixel_3a_(google-sargo\), it should be in Mobian soon, too)
* Generally everything in [postmarketOS's Community Devices section](https://wiki.postmarketos.org/wiki/Devices#Phones). If you want to get a device for daily driving and able to last you a bit, make sure to get an AArch64 device with calls support and at least 4GB of RAM; read the support matrix carefully before committing to a device.
* PINE64 PinePhone (see distributions below, [future support in some projects is not a given](https://blog.mobian.org/posts/2023/09/30/paperweight-dilemma/))
* PINE64 PinePhone Pro (see [PINE64 Wiki Software Releases](https://wiki.pine64.org/wiki/PinePhone_Pro_Software_Releases); experimental, do your research before purchase),
* Motorola Droid 4 ([Maemo Leste](https://leste.maemo.org/Motorola_Droid_4) and, to a lesser degree, [postmarketOS](https://wiki.postmarketos.org/wiki/Motorola_Droid_4_(motorola-maserati))).
* Nokia N900 (shipped in 2009 with Maemo 5, supported by [Maemo Leste](https://leste.maemo.org/Nokia_N900) and (to a lesser degree) [postmarketOS](https://wiki.postmarketos.org/wiki/Nokia_N900_(nokia-n900))),
* and many more, see the "supported devices" pages of projects like [postmarketOS](https://wiki.postmarketos.org/wiki/Devices) (if you intend to purchase a device, limit your research to main and community devices), [Mobian](https://wiki.debian.org/Mobian/Devices) and for non-mainline see [Droidian](https://devices.droidian.org/#/devices), [Ubuntu Touch](https://devices.ubuntu-touch.io/) and [Sailfish OS](https://forum.sailfishos.org/t/community-hardware-adaptations/14081).

See also: [Wikipedia: List of open-source mobile phones](https://en.wikipedia.org/wiki/List_of_open-source_mobile_phones), [postmarketOS Community devices](https://wiki.postmarketos.org/wiki/Devices), [Amos B. Batto's "Comparing Linux phones"](https://amosbbatto.wordpress.com/2020/08/25/comparing-linux-phones/).

### Tablets

* [Devices supported by postmarketOS](https://wiki.postmarketos.org/wiki/Devices#Tablets),
* [PINE64 PineTab 2](https://wiki.pine64.org/wiki/PineTab2) (WiFi/BT issues), [PineTab V](https://wiki.pine64.org/wiki/PineTab-V) (RISC-V), [PineTab (discontinued)](https://wiki.pine64.org/wiki/PineTab),
* [Cutie Pi tablet](https://cutiepi.io/) (do research on shipment status and software support before ordering),
* [Juno Tablet](https://junocomputers.com/us/product/juno-tablet/),
* [Purism Librem 11](https://puri.sm/products/librem-11/),
* [StarLabs StarLite 12.5](https://us.starlabs.systems/pages/starlite),
* Various x86_64 tablets, see [this post](https://linmob.net/gnu-linux-on-tablets-hardware/) for recommendations,
* Jingling [JingPad](https://en.jingos.com/jingpad-a1/) (runs downstream, Android-derived kernel, can run Ubuntu Touch, discontinued).

### Ultra Mobile Somethings
* Dragonbox Pyra ([Wikipedia](https://en.wikipedia.org/wiki/DragonBox_Pyra))

## Software

### Mobile Linux Distributions

The following list is mostly an adaptation of the Pine64 wiki [PinePhone Software Releases page](https://wiki.pine64.org/index.php?title=PinePhone_Software_Releases). `images` refers to PinePhone images in most cases. See also [Wikipedia: Linux for mobile devices](https://en.wikipedia.org/wiki/Linux_for_mobile_devices).

__This list is sorted alphabetically.__ 

* [Abyssal Linux](https://codeberg.org/Abyssal-Linux/abyssal-packages) (porting Void Linux to mobile devices).
* [Arch Linux ARM](https://archlinuxarm.org), [unofficial PinePhone images](https://github.com/dreemurrs-embedded/Pine64-Arch/releases), [additional PKGBUILD scripts](https://framagit.org/linmobapps/pkgbuilds), [Kupfer](https://gitlab.com/kupfer).
* Debian based
  * [Mobian](https://www.mobian-project.org), [images](https://images.mobian-project.org)
  * [Droidian](http://droidian.org/) (Debian for Android devices that don't run a a close-to-mainline kernel and use Halium instead), [images](https://github.com/droidian-images),
  * [FuriOS](https://furilabs.com/changelog/), custom, improved Droidian for the [FuriLabs FLX1](https://furilabs.com/shop/flx1/). _Just guessing here, there's definitely some personal overlap._
  * [Kali Linux](https://www.kali.org/), [official images](https://www.kali.org/get-kali/#kali-mobile), [unofficial images](https://github.com/Shubhamvis98/kali-pinephone/releases) - both with Phosh
  * [PureOS mobile (developed for the Librem 5)](https://www.pureos.net), [unoffical PinePhone build script](https://github.com/rufferson/pureos-pinephone)
  * [Maemo Leste](https://maemo-leste.github.io/), [images](http://maedevu.maemo.org/images/pinephone/), [automatically build images](https://phoenix.maemo.org/view/Images/) (Yes, it is based on Devuan, actually.)
* Fedora, [Mobility initiative](https://fedoraproject.org/wiki/Mobility), [unofficial images](https://github.com/nikhiljha/pp-fedora-sdsetup/releases/), [unofficial nightly images](ftp://pine.warpspeed.dk/nightly/pinephone/)
* Gentoo: [Wiki article](https://wiki.gentoo.org/wiki/PinePhone), [Guide](https://wiki.gentoo.org/wiki/User:Dr41nU/PinePhone)
* [LuneOS](https://pivotce.com/author/webosports/) (webOS continuation), [builds](http://build.webos-ports.org/luneos-testing/images/pinephone)
* Manjaro ARM for PinePhone (as of November 2023, only Phosh seems to be in a maintained state): 
  * [Plasma Mobile](https://github.com/manjaro-pinephone/plasma-mobile),  [unstable dev builds](https://github.com/manjaro-pinephone/plasma-mobile-dev),
  * [Phosh](https://github.com/manjaro-pinephone/phosh), [unstable dev builds](https://github.com/manjaro-pinephone/phosh-dev),
* [Nemo Mobile](https://wiki.merproject.org/wiki/Nemo), [Pine Phone builds](http://nemomobile.net/pages/Nemo_For_pinephone/), [newer Manjaro-based images](https://img.nemomobile.net/2021.10/)
* [NixOS mobile](https://mobile-nixos.github.io/mobile-nixos/), [supported devices](https://mobile-nixos.github.io/mobile-nixos/devices/index.html), [nixos-hardware: Librem 5](https://github.com/NixOS/nixos-hardware/tree/master/purism/librem/5r4).
* openMandriva: [images](https://sourceforge.net/projects/openmandriva/files/release/4.2/RC/Pinephone/)
* openSuSE/slem.os, [images](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/?C=N;O=D), [gitlab](https://gitlab.com/slem.os), [changelog](https://gitlab.com/slem.os/slem.os/-/blob/master/CHANGELOG.md), [wiki](https://en.opensuse.org/HCL:PinePhone)
* [postmarketOS](https://postmarketos.org), [images](https://images.postmarketos.org/bpo/)
  * postmarketOS based
    * [Sxmo](https://sxmo.org) (You will need to read the [user guide](https://sxmo.org/docs/USERGUIDE.html), [Sxmo build](https://git.sr.ht/%7Emil/sxmo-build) script to build it for postmarketOS, Debian or Arch.
    * AVMultiPhone (MATE Desktop on postmarketOS), [images](https://osdn.net/projects/avmultimedia/releases/73167)
    * sineware.ca [Plasma Mobile Nightly builds](https://sineware.ca/prolinux/plasma-mobile-nightly/) (for PinePhone).
* [ProLinux 2](https://sineware.ca/prolinux/), immutable OS with Plasma Mobile Nightly (Plasma 6) by Sineware.
* [Replicant](https://replicant.us/), Free Software Android. [PinePhone wiki page](https://redmine.replicant.us/projects/replicant/wiki/Pinephone), not builds yet. __GloDroid__ offers (not as free) Android (AOSP or LineageOS-based) images for [PinePhone](https://github.com/GloDroidCommunity/pine64-pinephone/releases) and [PinePhone Pro](https://github.com/GloDroidCommunity/pine64-pinephonepro/releases).
* [SHR](http://www.shr-project.org) (Stable Hybrid Release, abandoned [github](https://github.com/shr-distribution)).
* [SailfishOS](https://sailfishos.org), [unofficial flashing script for the PinePhone and PinePhone Pro](https://raw.githubusercontent.com/sailfish-on-dontbeevil/flash-it/master/flash-it.sh), [Officially supported devices](https://shop.jolla.com/), [Community supported devices](https://forum.sailfishos.org/t/community-hardware-adaptations/14081)
* Ubuntu based
  * [Ubuntu Touch](https://ubuntu-touch.io) by [UBports](https://ubports.com/), [PinePhone images](https://gitlab.com/ubports/community-ports/pinephone#how-do-i-install-ubuntu-touch-on-my-pinephone), [unofficial PinePhone Pro images](https://gitlab.com/ook37/pinephone-pro-debos/-/releases)
  * [KDE Neon](https://images.plasma-mobile.org/pinephone/) (defunct)
  * [Rhino Linux](https://rhinolinux.org/download/), rolling Ubuntu with Xfce
* [Yocto Project](https://www.yoctoproject.org/) ("creates custom embedded distributions for you")
* [PinePhone multi-distro demo image](https://xnux.eu/p-boot-demo/) (17 distributions in one)

### Mobile optimized Linux Software Lists

* [LinuxPhoneApps.org](https://linuxphoneapps.org) – merge requests welcome!
* [Mobile GNU/Linux Apps](https://mglapps.frama.io/) – the origin of LINMOBapps/LinuxPhoneApps.org.
* [Flathub on Mobile](https://github.com/tchx84/flathub-mobile), a list of mobile Flathub apps.
* [awesome floss mobile](https://github.com/zayuim/awesome-floss-mobile)
* [The Mobian Wiki has a nice list of Apps](https://wiki.mobian-project.org/doku.php?id=apps). It applies to all Phosh based distributions and has nice information including the App ID (which you will need to [scale apps properly](https://linmob.net/2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html) with `scale-to-fit`), although you might have to take a different route to get the software installed. 
* Purism has two lists, depending on whether the software is in their _PureOS_ repos: 
  * [Mobile-optimized apps](https://tracker.pureos.net/w/pureos/mobile_optimized_apps/) and
  * [Mobile-optimized apps from 3rd party repos](https://tracker.pureos.net/w/pureos/3rd-party_mobile_optimized_apps/).
* [Open-Store.io](https://open-store.io): The UBports app store.
* [OpenRepos.net](https://openrepos.net/): Collection of Mer and Sailfish apps.

### Accessibility

As mobile Linux is still nascent, accessibility is quite limited. The following is a list of links that hopefully will help in evaluating how mobile Linux can fit your accessibility requirements:
* the [librem5-accessibility wiki](https://github.com/Merivuokko/librem5-accessibility/wiki) is a resource that may help you to figure out whether mobile Linux can already fit your needs,
* the [PINE64 Wiki has a page dedicated to PinePhone accessibility](https://wiki.pine64.org/wiki/PinePhone_Accessibility), which is mostly about the accessibility of the PinePhone hardware,
* the [postmarketOS wiki also has a page about Accessibility](https://wiki.postmarketos.org/wiki/Accessibility), and more in-depth pages about [kernel features](https://wiki.postmarketos.org/wiki/Accessibility_(Kernel)) and [software packages](https://wiki.postmarketos.org/wiki/Accessibility_(Packages)) that are relevant.

## History of Linux on PDAs and Smartphones

### PDAs

* 2000: Agenda VR3 PDA ([Wikipedia](https://en.wikipedia.org/wiki/Agenda_VR3))
* 2001: GMate Yopy ([Wikipedia](https://en.wikipedia.org/wiki/Yopy)),
* 2002: Simputer ([Wikipedia](https://en.wikipedia.org/wiki/Simputer)),
* 2002: Sharp Zaurus SL-5500 ([Wikipedia](https://en.wikipedia.org/wiki/Sharp_Zaurus))
  * Sharp would continue to produce many more Linux powered PDAs till about 2005.
* 2005: Nokia 770 Internet Tablet ([Wikipedia](https://en.wikipedia.org/wiki/Nokia_770_Internet_Tablet))
  * Nokia would follow up with the [N800](https://en.wikipedia.org/wiki/Nokia_N800) and [N810](https://en.wikipedia.org/wiki/Nokia_N800) and then shift to a smaller form factor with the Nokia N900 smartphone.
* In addition to the named devices and their followups there were ports of Linux to devices like Windows CE/Windows Mobile based devices, booting via `haret`, mainly by projects like linuxtogo.org and handhelds.org. Other victims of linuxification were Psion Revo PDA and later ARM based Palm PDA. See also [The Linux Documentation Project](http://www.tldp.org/HOWTO/Infrared-HOWTO/infrared-howto-s-irda-pda.html) and the [memento of tuxmobil.org](https://web.archive.org/web/20080901121731/http://tuxmobil.org/pda_linux.html).

### Smartphones

* 2005: Motorola E680 running the Motorola EZX platform, limited availability to Asia. The later A780 model ([Wikipedia](https://en.wikipedia.org/wiki/Motorola_A780)) featuring GPS for navigation was released to Europe, too. The EZX phones, of which only the A910 ([Wikipedia](https://en.wikipedia.org/wiki/Motorola_A910) had WiFi, were target devices of the OpenEZX project ([Wikipedia](https://en.wikipedia.org/wiki/OpenEZX)), which worked on adding mainline Linux support (at the time 2.6) to the EZX family of devices.
* 2006: Trolltech Greenphone ([Wikipedia](https://en.wikipedia.org/wiki/Greenphone)), developer device
* 2007: OpenMoko/FIC Neo 1973 ([openmoko.org](http://wiki.openmoko.org/wiki/GTA01)): The first smartphone hardware made specifically with the intention to run FOSS GNU/Linux software.
* 2008: OpenMoko FreeRunner ([openmoko.org](http://wiki.openmoko.org/wiki/GTA02)): Iterative improvement on the Neo 1973, sold in larger quantities. The _not that fast_ ARMv4 processor was not the main problem, as the added graphics accelerator turned out to be a graphics decelerator as it could not handle the 480x640 pixels of the 2.6" display.
* 2008: HTC Dream/T-Mobile G1 ([Wikipedia](https://en.wikipedia.org/wiki/HTC_Dream)): first commercial Android smartphone)
* 2009: Nokia N900 ([Wikipedia](https://en.wikipedia.org/wiki/Nokia_N900)): Nokia's first Linux smartphone running Maemo 5 (Debian based with a GTK2 interface).
* 2011: Golden Delicious/Open Phoenux GTA 04 ([openphoenux.org](http://www.openphoenux.org/)): An attempt to equip the old OpenMoko hardware with newer, TI OMAP3 based boards.
* 2011: Nokia N9 ([Wikipedia](https://en.wikipedia.org/wiki/Nokia_N9)) is being launched using "Meego", which is really Maemo 6 "Harmattan". It uses a Qt 4 based GUI instead of the GTK2 based user interface of the N900.
* 2013: [Neo900](http://neo900.org/), a project to re-power the Nokia N900 with more RAM, a slightly faster chip and LTE connectivity gets announced. Their latest blog post from March 1st, 2018 thanks PIA for support supposedly helping the project to continue.
* 2017: [Purism](https://puri.sm) start a [crowdfunder for their Librem 5 smartphone](http://linmob.net/2017/08/25/librem-5-announced-crowdfunding-started.html), with an estimated ship date of January 1st. The hardware specs weren't finalized at this point, evaluation boards have the Freescale/NXP i.MX6 chip.
