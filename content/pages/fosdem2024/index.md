+++
title = "#LinuxMobile at FOSDEM 2024"
date = 2024-01-28
path = "fosdem2024"
template = "single.html"
pagenate_by = 0
+++

With [FOSDEM](https://fosdem.org/), a free event for software developers to meet, share ideas and collaborate in Brussels, Belgium, approaching (3rd & 4th February 2024), let's have a little preview on the talks and other things that are particularly exciting for #linuxmobile.

### Main track (Sunday, 15:00 	15:50)
- Agnes Crepet, Luca Weiss: [Open Source for Sustainable and Long lasting Phones](https://fosdem.org/2024/schedule/event/fosdem-2024-3362-open-source-for-sustainable-and-long-lasting-phones/) _While this is likely more about Fairphone in general, it's great to have something about FOSS on mobile in the main track!_

### The ["FOSS on Mobile Devices"](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/) Track/Developer Room (Saturday)

Just like last two years, there's a specific room for the topic this blog is mostly about.

All talks of the track happen in Room [H.1309 (Van Rijn)](https://fosdem.org/2024/schedule/room/h1309/) on Saturday, February 4th from 10:30 to 18:15, which is an upgrade from the half-a-day dev room last week. Every talk in this track is 25 to 40 minutes long.

#### Talks

_TBD_
Make sure to check the Talk pages for links to chat rooms (Matrix) and live streams!

### Other Tracks

_TBD_

Of course, there's a lot more, make sure to browse the full schedule [on the Web](https://fosdem.org/2024/schedule/) or in an app like [Kongress](https://linuxphoneapps.org/apps/org.kde.kongress/) or [Confy](https://linuxphoneapps.org/apps/net.kirgroup.confy/).

_Suggestions beyond the obvious? Please get in touch and tell :D_

#### Sunday
Aside from [the Fairphone talk that made the main track](https://fosdem.org/2024/schedule/event/fosdem-2024-3362-open-source-for-sustainable-and-long-lasting-phones/), here are some suggestions if you don't know what do to, listed in order of appearance:

* [Comprehensible Open Hardware: Building the Open Book](https://fosdem.org/2024/schedule/event/fosdem-2024-2913-comprehensible-open-hardware-building-the-open-book/)
* [Breathing Life into Legacy: An Open-Source Emulator of Legacy Apple Devices](https://fosdem.org/2024/schedule/event/fosdem-2024-2826-breathing-life-into-legacy-an-open-source-emulator-of-legacy-apple-devices/)
* [Self-hosting and autonomy using guix-forge](https://fosdem.org/2024/schedule/event/fosdem-2024-2560-self-hosting-and-autonomy-using-guix-forge/)
* [The state of video offloading on the Linux Desktop](https://fosdem.org/2024/schedule/event/fosdem-2024-3557-the-state-of-video-offloading-on-the-linux-desktop/)
* [Linux Matchmaking: Helping devices and drivers find each other](https://fosdem.org/2024/schedule/event/fosdem-2024-3222-linux-matchmaking-helping-devices-and-drivers-find-each-other/)
* [Modos: Building an Ecosystem of Open-Hardware E Ink Devices](https://fosdem.org/2024/schedule/event/fosdem-2024-3052-modos-building-an-ecosystem-of-open-hardware-e-ink-devices/)
* [The Basic Economics behind Open Source Funding in 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-3160-the-basic-economics-behind-open-source-funding-in-2024/)
* [Desktop Linux, as easy as a smartphone! Just in a Snap!](https://fosdem.org/2024/schedule/event/fosdem-2024-1860-desktop-linux-as-easy-as-a-smartphone-just-in-a-snap-/)
* [Firefox power profiling: a powerful visualization of web sustainability](https://fosdem.org/2024/schedule/event/fosdem-2024-2716-firefox-power-profiling-a-powerful-visualization-of-web-sustainability/)
* [MatrixRTC: The Future of Matrix Calls](https://fosdem.org/2024/schedule/event/fosdem-2024-2876-matrixrtc-the-future-of-matrix-calls/)

But please don't forget about [the meetup](#let-s-meet-and-maybe-get-a-sticker)!

### Stands

Now let's continue with what's mostly interesting for those who can make it to Brussels like me. [Stands](https://fosdem.org/2024/stands/), where projects can showcase their stuff!

* Droidian Linux + CalyxOS
* [Linux on Mobile (Mobian, Sailfish OS, PureOS, UBports, MNT Research, postmarketOS) + Ubports](https://linuxonmobile.net/#). _Both in building AW on Level 1, I may hang out there a lot. Last year, this building was way calmer than the place of the Linux on Mobile stand, which is good for hanging out, but may lead to less passers-by._

* PINE64 is in building K on Level 1

Also, let's not forget what FOSDEM is for: The hallway track, talking to people - that's what makes this better than a virtual conference.

### Let's meet and (maybe) get a sticker

<figure>
    <img src="/media/2023/pile-of-phones.jpg" alt="A pile of different phones running different Linux distros" title="A whole lot of Linux phones" style="height:auto; margin:0;">
    <figcaption>A whole lot of Linux phones.</figcaption>
</figure>

I (Peter) will be there, and if I am lucky, the stickers I forgot at home will be with me then, which I'll happily hand out :D
For most of the Saturday, I'll be in the dev room.

I would like to hold a short meetup on Sunday, 2:00pm, at roughly [this place outdoors](https://www.openstreetmap.org/?mlat=50.81373&mlon=4.38212#map=19/50.81373/4.38212). _It would be nice to meet some people I only know from the internet, so please come by!




