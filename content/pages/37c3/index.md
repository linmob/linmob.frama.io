+++
title = "Hey 37c3, have you heard of the Linux Mobile Revolution?"
path = "37c3" 
template = "single.html"
pagenate_by = 0
+++

<mark>Thank you for coming by! Congress was a blast, despite the organizational mishap!</mark>

Hello 37c3! If you haven't heard of us, we're a collection of individual
developers and self-organised projects working towards the end-goal of FREEING
YOUR PHONE.

<mark>Due to miscommunication and a lack of tables, the assembly is not to be found on c3nav, check [the assembly page](https://events.ccc.de/congress/2023/hub/en/assembly/linux-on-mobile/) for a hopefully up-to-date postion or ring one of us on DECT (9788 or 5253) if you want to meet up!</mark>

We're sick of our needs not being served, of [our phones being hostile towards
us](https://archive.fosdem.org/2022/schedule/event/mobile_social_dilemma/), and
of the eternal profit motive driving planned obselence and insecurity.

We believe that your phone should work for you; it shouldn't [sap your attention
with dark patterns](https://transcend.io/blog/dark-patterns-cpra), nor should it
stop receiving security updates or replacement parts while still more than
capable of serving your every day needs.

To this end, we work to enable support for our devices in the upstream Linux
kernel, replacing the [barely Linux](https://not.mainline.space/) vendor kernel
(which is usually barely functional without proprietary Android-only userspace
components).

We work on making Linux [more
convergent](https://tuxphones.com/convergent-linux-phone-apps/) so that as we
continue to grow, we can provide more and more apps which are actually usable.
As well as building [many](https://plasma-mobile.org/), [different](https://phosh.mobi/), [kinds](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/) of [user interfaces](https://sxmo.org/).

<figure>
    <img src="/media/2023/pile-of-phones.jpg" alt="A pile of different phones running different Linux distros" title="A whole lot of Linux phones" style="height:auto; margin:0;">
    <figcaption>A whole lot of Linux phones.</figcaption>
</figure>

## Get involved

We need your help! Whether it's testing your next Linux app in a mobile form
factor, working on support for a new device ([or reviving an old
one](https://wiki.postmarketos.org/wiki/Devices)), [testing our
code](https://postmarketos.org/blog/2023/05/21/call-for-testers/), [making
awesome
wallpapers](https://gitlab.com/postmarketOS/artwork/-/merge_requests/25), or
something else entirely, together we can build a truly user-owned ecosystem.

## Hardware vendors, hit us up

Whilst we always prefer to avoid building more future-landfill, if it's going to
be done then it should be done properly. If you're trying to build a phone or tablet that truly respects its users, get in touch! You can contact Caleb (postmarketOS developer) on Matrix `@caleb:postmarketos.org`.

## Join us

* <https://postmarketos.org/> - A real Linux distribution for phones
* <https://mobian.org/> - A debian derivative for mobile devices
* <https://matrix.to/#/#FOSSMobile:matrix.org> - The FOSS on Mobile Matrix space

## Other peoples' stuff: Relevant Sessions at 37C3

* Day 4, 12:30 - 13:00, Room E: [Smartphones freedom status in 2023](https://events.ccc.de/congress/2023/hub/en/event/smartphones-freedom-status-in-2023/)
