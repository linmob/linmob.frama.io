+++
title = "About" 
path = "about" 
template = "single.html"
pagenate_by = 0
+++

### What is LINMOB.net?

LINMOB.net is a blog on LINux on MOBile devices, that has been around for more than 10 years - on and off. Now, with new exciting products like the Pine64 PinePhone and Purisms Librem 5 scheduled to be shipped later this year, it has come back to report on all things GNU/Linux on mobile devices such as smartphones, tablets and smaller notebooks.

This site is currently hosted on [Framagit](https://framagit.org/linmob/linmob.frama.io). 

Who writes and creates all this? Check our [Authors page](https://linmob.net/authors/).

### More Content

#### Shortform
You can follow updates and shortform related content that is not worth a blog post on the [Fediverse](https://fosstodon.org/@linmob) `@linmob@fosstodon.org`. We also have accounts on [bluesky](https://bsky.app/profile/linmob.net), [Xitter](https://twitter.com/linmobblog) `@linmobblog` and Nostr `npub1llxd4l8zzc5dta4cnatk4r3tmt86pcvj4fcgkk8wfdlufjpwll4qyku4l3` that mostly announce new blog posts. All accounts are run and monitored by [Peter](https://linmob.net/authors/peter/).

#### Videos
If you like visual content, make sure to check out our [video content](https://linmob.net/videos/). Sadly, due to time constraints, it's unlikely that additional video content is going to be published. If you want to publish a relevant video on our channels, please get in touch.

### How can I contribute?

#### Content

If you have an idea for an article or have actually written something, you are welcome to send it to me via [email](mailto:articles@linmob.net) or submit it via Merge Request on [framagit](https://framagit.org/linmob/linmob.frama.io). We can't offer payment, but you'll be credited. We're going to get back to you as soon as we can. 

If you want to submit a proper markdown post with all bells and whistles, you can use our [post composer](https://linmob.net/zola-post-composer.html) - it should make things easier.

It's also perfectly fine to submit a teaser for a relevant post you have published somewhere else, e.g., on your own blog!

#### I don't have time, but I have money

If you want to contribute money, please support the people that develop software for the PinePhone and other Platforms, e.g.

* [UBports, who build Ubuntu Touch](https://ubports.com/donate),
* [Ondřej Jirman (Megi)](https://xnux.eu/contribute.html#toc-donations), who works on the PinePhone kernel,
* [Mobian, a great Debian-based distribution](https://liberapay.com/mobian/donate),
* [postmarketOS, the real Linux distribution for phones](https://postmarketos.org/donate.html), that runs (well, boots) on more phones every day,
* [Phosh/Guido Guenther](https://honk.sigxcpu.org/piki/donations/), the developer of Peter's favorite Mobile Shell,
* [Plasma Mobile](https://www.plasma-mobile.org/findyourway/#!/rootgroup/outreach), who build a great GUI for phones,
* [Lune OS](https://pivotce.com/author/webosports/), who are keeping webOS alive, and
* individual app developers!

If we've missed a great project that has a donation link and should be added here in your opinion, please tell us about it!

#### App List: LinuxPhoneApps.org

If you want to help with LinuxPhoneApps, please check the [README](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/README.md) and the open [issues](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) on that project and [its subprojects](https://framagit.org/linuxphoneapps).

####  Otherwise...

Please lend your time to the projects you like, try to communicate in a friendly way and be helpful. There are a million ways to contribute to opensource, by reporting, triaging bugs, by improving documentation or translations. Thanks!


