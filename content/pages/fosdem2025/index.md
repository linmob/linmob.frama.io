+++
title = "#LinuxMobile at FOSDEM 2025"
date = 2024-01-19
path = "fosdem2025"
template = "single.html"
pagenate_by = 0
+++

<mark>Thanks for all the kind and nice interactions at FOSDEM, especially at the Linux on Mobile stand!</mark>

With [FOSDEM](https://fosdem.org/), a free event for software developers to meet, share ideas and collaborate in Brussels, Belgium, approaching (1st & 2nd February 2025), let's have a little preview on the talks and other things that are particularly exciting for #LinuxMobile.

### The ["FOSS on Mobile Devices"](https://fosdem.org/2025/schedule/track/mobile/) Track/Developer Room.

Like the last three years, there's a specific room for 'FOSS on Mobile' - just half a day this time, starting at 14:55 and closing at 19:00.

All talks of the track happen in Room [H.2214](https://nav.fosdem.org/l/h2214/) ([Map](https://nav.fosdem.org/l/h2214/)) on Saturday afternoon, February 1st, 2025 - the first half of the day you can attend talks about the [Android Open Source Project](https://fosdem.org/2025/schedule/track/aosp/) in the same room. Every talk in the FOSS on Mobile Devices track is 10 to 20 minutes short.

Make sure to check the Talk pages for links to chat rooms (Matrix) and live streams:

- [Mainline vs libhybris: Technicalities, down to the buffer](https://fosdem.org/2025/schedule/event/fosdem-2025-4917-mainline-vs-libhybris-technicalities-down-to-the-buffer/) | Alfred Neumayer | 14:55-15:15
- [Kernel support for Mobile Linux: The missing 20%](https://fosdem.org/2025/schedule/event/fosdem-2025-4836-kernel-support-for-mobile-linux-the-missing-20-/) | Luca Weiss | 15:20-15:40
- [Weather and emergency alerts](https://fosdem.org/2025/schedule/event/fosdem-2025-5102-weather-and-emergency-alerts/) | Volker Krause, Nucleus | 15:45-16:05
- [Bringing Oniro to Mobile: Challenges in Hardware Enablement](https://fosdem.org/2025/schedule/event/fosdem-2025-4684-bringing-oniro-to-mobile-challenges-in-hardware-enablement/) | Francesco Pham | 16:10-16:20 _I hope this will further my understanding of Oniro beyond [buzzwords](https://oniroproject.org/#about-us)!_
- [Sxmo: A mobile UI for hackers](https://fosdem.org/2025/schedule/event/fosdem-2025-5926-sxmo-a-mobile-ui-for-hackers/) | Marten van Gompel (proycon) | 16:20-16:30
- [Mirror Hall: Building virtual network displays to bridge mobile and desktop](https://fosdem.org/2024/schedule/event/fosdem-2025-4527-mirror-hall-building-virtual-network-displays-to-bridge-mobile-and-desktop/) | Raffaele Tranquillini | 16:35-16:55
- [OpenAGPS - Open source GNSS Assistance](https://fosdem.org/2025/schedule/event/fosdem-2025-6739-openagps-open-source-gnss-assistance/) | Alexander Richards | 17:00-17:20
- [Mobile Browsers: the Best of Times, the Worst of Times](https://fosdem.org/2025/schedule/event/fosdem-2025-6411-mobile-browsers-the-best-of-times-the-worst-of-times/) | David Llewellyn-Jones | 17:25-17:45
- [libobscura: Cameras are STILL difficult](https://fosdem.org/2025/schedule/event/fosdem-2025-6184-libobscura-cameras-are-still-difficult/) | dcz | 17:50-18:10
- [phosh: Yet another year around the sun!](https://fosdem.org/2025/schedule/event/fosdem-2025-6323-phosh-yet-another-year-around-the-sun-/) | Evangelos Ribeiro Tzaras | 18:15-18:35
- [postmarketOS: what is it and what's new?](https://fosdem.org/2025/schedule/event/fosdem-2025-4654-postmarketos-what-is-it-and-what-s-new-/) | Oliver Smith | 18:40-19:00

- [CANCELED - Mobian, "upstream first", and the Greater Good](https://fosdem.org/2025/schedule/event/fosdem-2025-4129-canceled-mobian-upstream-first-and-the-greater-good/) _Sadly, Arnaud can't make it to FOSDEM this year!_

### Other Tracks

#### Saturday

10:00

- [Automated testing for mobile images using GNOME](https://fosdem.org/2025/schedule/event/fosdem-2025-5649-automated-testing-for-mobile-images-using-gnome/) | Sam Thursfield | 10:30-10:45 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)
- [Homebrew on ARM64 Linux](https://fosdem.org/2025/schedule/event/fosdem-2025-6373-homebrew-on-arm64-linux/) | Ruoyu Zhong | 11:10-11:25 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)
- [Flutter for all the desktops and beyond](https://fosdem.org/2025/schedule/event/fosdem-2025-5682-flutter-for-all-the-desktops-and-beyond/) | Saviq | 11:30-11:45 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)
- [Exploring Open Source Dual A/B Update Solutions for Embedded Linux](https://fosdem.org/2025/schedule/event/fosdem-2025-6299-exploring-open-source-dual-a-b-update-solutions-for-embedded-linux/) | Leon Anavi | 11:30-11:55 | [H.1302](https://nav.fosdem.org/l/h1302/)

12:00

- [Ladybird - a new independent browser written from scratch](https://fosdem.org/2025/schedule/event/fosdem-2025-4132-ladybird-a-new-independent-browser-written-from-scratch/) | Jelle Raaijmakers | 12:30-12:45 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)
- [Booting blobs between U-Boot and Linux](https://fosdem.org/2025/schedule/event/fosdem-2025-6084-booting-blobs-between-u-boot-and-linux/) | Marek Vasut | 12:30-12:55 | [H.1302](https://nav.fosdem.org/l/h1302/)
- [Mozilla Contributors: Newcomers and Old timers Meetup](https://fosdem.org/2025/schedule/event/fosdem-2025-6755-mozilla-contributors-newcomers-and-old-timers-meetup/) | Danny Colin | 13:00-14:00 | [H.3244](https://fosdem.org/2025/schedule/room/h3244/) 
- [Towards a purely open AOSP: Adding Android-like functionality to AOSP](https://fosdem.org/2025/schedule/event/fosdem-2025-4106-towards-a-purely-open-aosp-adding-android-like-functionality-to-aosp/) | Bernhard "bero" Rosenkränzer | 13:00-13:30 | [H.2214](https://nav.fosdem.org/l/h2214/)
- [Second chance: Upgrading devices from Android 9 to Android 14](https://fosdem.org/2025/schedule/event/fosdem-2025-5515-second-chance-upgrading-devices-from-android-9-to-android-14/) | Igor Kalkov-Streitz | 13:30-14:00 | [H.2214](https://nav.fosdem.org/l/h2214/)


14:00

- [LibreOffice on mobile with the Collabora Office app](https://fosdem.org/2025/schedule/event/fosdem-2025-5093-libreoffice-on-mobile-with-the-collabora-office-app/) | Skyler Grey | 14:05-14:15 | [H.2213](https://nav.fosdem.org/l/h2213/) 
- [LoRaMesher library for LoRa mesh networks](https://fosdem.org/2025/schedule/event/fosdem-2025-4476-loramesher-library-for-lora-mesh-networks/) | Felix Freitag, Joan Miquel Solé | 14:50-15:05 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)

16:00

- [Open-Hardware E Ink Devices with Modos: Disscussions & Demos](https://fosdem.org/2025/schedule/event/fosdem-2025-6753-open-hardware-e-ink-devices-with-modos-discussion-demos/) | Alexander Soto | 16:00-16:30 | [H.3244](https://nav.fosdem.org/l/h3244/)
- [Samsung Camera to Mastodon Bridge](https://fosdem.org/2025/schedule/event/fosdem-2025-5026-samsung-camera-to-mastodon-bridge/) | Georg Lukas | 16:00-16:10 | [H.1302](https://nav.fosdem.org/l/h1302/)
- [Introduction to pmbootstrap](https://fosdem.org/2025/schedule/event/fosdem-2025-6187-introduction-to-pmbootstrap/) | Anjan Momi | 16:10-16:20 | [H.1302](https://nav.fosdem.org/l/h1302/)
- [FOSDEM 2025 - Patchouli: Open-Source EMR Drawing Tablet](https://fosdem.org/2025/schedule/event/fosdem-2025-6754-patchouli-open-source-emr-drawing-tablet/) | Alexander Soto | 16:30-17:00 | [H.3244](https://nav.fosdem.org/l/h3244/)
- [Status of CJK input system in Wayland](https://fosdem.org/2025/schedule/event/fosdem-2025-6533-status-of-cjk-input-system-in-wayland/) | Sungjoon Moon | 16:30-16:45 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)

18:00

- [Using embedded Rust to build an unattended, battery-powered device](https://fosdem.org/2025/schedule/event/fosdem-2025-6300-using-embedded-rust-to-build-an-unattended-battery-powered-device/) | Xabier Crespo Álvarez | 18:30-18:55 | [H.1302](https://nav.fosdem.org/l/h1302/)

#### Sunday

Also on Sunday: [Meet up at 14:00](#let-s-meet-and-get-a-sticker)!

- [FSFE Upcycling Android Workshop BOF](https://fosdem.org/2025/schedule/event/fosdem-2025-4277-fsfe-upcycling-android-workshop-bof/) | Darragh Elliott | 09:00-10:00 | [AW 1.121](https://nav.fosdem.org/l/aw1121/)
- [What FLOSS Means in the AI World](https://fosdem.org/2025/schedule/event/fosdem-2025-5495-what-floss-means-in-the-ai-world/) | Mitchell Baker | 09:00-09:50 | [Janson](https://nav.fosdem.org/l/janson/)
- [Next Generation Internet 2025: where next?](https://fosdem.org/2025/schedule/event/fosdem-2025-6508-next-generation-internet-2025-where-next-/) | Michiel Leenaars | 09:00-09:50 | [K.1.105](https://nav.fosdem.org/l/k1105/)
- [ParticleOS: Can we make Lennart Poettering run an image based distribution?!](https://fosdem.org/2025/schedule/event/fosdem-2025-4057-particleos-can-we-make-lennart-poettering-run-an-image-based-distribution-/) | Daan De Meyer | 09:30-10:00 | [UB4.136](https://nav.fosdem.org/l/ub4136/) 

10:00

- [FOSS apps on Android BoF](https://fosdem.org/2025/schedule/event/fosdem-2025-6219-foss-apps-on-android-bof/) | Michael Opdenacker | 10:00-11:00 | [AW 1.121](https://nav.fosdem.org/l/aw1121/)
- [WebExtensions BoF](https://fosdem.org/2025/schedule/event/fosdem-2025-5916-webextensions-bof/) | Rob Wu, Danny Colin | 11:00-12:00 | [AW 1.121](https://nav.fosdem.org/l/aw1121/)
- [Sailfish OS Community BoF](https://fosdem.org/2025/schedule/event/fosdem-2025-6773-sailfish-os-community-bof/) | David Llewellyn-Jones, Raine Mäkeläinen | 11:30-12:30 | [H.3244](https://fosdem.org/2025/schedule/room/h3244/)
- [Funding the FOSS Ecosystem](https://fosdem.org/2025/schedule/event/fosdem-2025-6397-funding-the-foss-ecosystem/) | Emmy Tsang | 11:50-12:20 | [K.3.601](https://nav.fosdem.org/l/k3601/)

12:00

- [The Road to Mainstream Matrix](https://fosdem.org/2025/schedule/event/fosdem-2025-6274-the-road-to-mainstream-matrix/) | Matthew Hodgson | 12:50 | [K.1.105](https://nav.fosdem.org/l/k1105/)
- [NGI Zero network meetup BOF](https://fosdem.org/2025/schedule/event/fosdem-2025-6561-ngi-zero-network-meetup-bof/) | Ronny Lam | 13:00-14:00 | [AW 1.121](https://nav.fosdem.org/l/aw1121/)
- [When is it Right to Say No to Funding?](https://fosdem.org/2025/schedule/event/fosdem-2025-6481-when-is-it-right-to-say-no-to-funding-/) | Karen Sandler | 13:50-14:20 | [K.3.601](https://nav.fosdem.org/l/k3601/)

14:00 ([see also](#let-s-meet-and-get-a-sticker))

- [How do we get the European Union to invest in FOSS maintenance and security?](https://fosdem.org/2025/schedule/event/fosdem-2025-6133-how-do-we-get-the-european-union-to-invest-in-foss-maintenance-and-security-/) | Nicholas Gates, Felix Reda | 14:30-15:00 | [K.3.601](https://nav.fosdem.org/l/k3601/)
- [Open source should have an answer to Teams](https://fosdem.org/2025/schedule/event/fosdem-2025-4514-open-source-should-have-an-answer-to-teams/) | Jos Poortvliet | 14:30-14:45 | [H.2215 (Ferrer)](https://nav.fosdem.org/l/h2215/)
- [Rust for Linux: an overview](https://fosdem.org/2025/schedule/event/fosdem-2025-5875-rust-for-linux-an-overview/)
- [Nostr, notes and other stuff transmitted by relays](https://fosdem.org/2025/schedule/event/fosdem-2025-6452-nostr-notes-and-other-stuff-transmitted-by-relays/) | Wouter Constant | 15:00-15:50 | [K.1.105](https://nav.fosdem.org/l/k1105/)
- [Small seeds - why funding new ideas matters](https://fosdem.org/2025/schedule/event/fosdem-2025-5211-small-seeds-why-funding-new-ideas-matters/) | Marie Kreil, Marie-Lena Wiese | 15:10-15:40 | [K.3.601](https://nav.fosdem.org/l/k3601/)
- [Rhino Linux and Pacstall: Towards a Rolling Ubuntu](https://fosdem.org/2025/schedule/event/fosdem-2025-4398-rhino-linux-and-pacstall-towards-a-rolling-ubuntu/) | Oren Klopfer, A. Salt | 15:30-16:00 | [H.1302](https://nav.fosdem.org/l/h1302/)
- [Towards Open Source-Compatible Standards](https://fosdem.org/2025/schedule/event/fosdem-2025-6111-towards-open-source-compatible-standards/) | Tobie Langel | 15:40-15:50 | [AW 1.120](https://nav.fosdem.org/l/aw1120/)

16:00

- [Robrix: a pure Rust multi-platform Matrix Client and more](https://fosdem.org/2025/schedule/event/fosdem-2025-5841-robrix-a-pure-rust-multi-platform-matrix-client-and-more/) | Kevin Boos | 16:00-16:30 | [K.4.201](https://nav.fosdem.org/l/k4201/)
- [How we are defending Software Freedom against Apple at the EU's highest court](https://fosdem.org/2025/schedule/event/fosdem-2025-5084-how-we-are-defending-software-freedom-against-apple-at-the-eu-s-highest-court/) | Lucas Lasota | 16:00-16:50 | [Janson](https://nav.fosdem.org/l/janson/)
- [Resurrecting the minimalistic Dillo web browser](https://fosdem.org/2025/schedule/event/fosdem-2025-4100-resurrecting-the-minimalistic-dillo-web-browser/) | Rodrigo Arias Mallo | 16:10-16:30 | [H.1308](https://nav.fosdem.org/l/h1308/) 
- [State of Firefox Add-ons](https://fosdem.org/2025/schedule/event/fosdem-2025-6619-state-of-firefox-add-ons/) | Simeon Vincent | 16:15-16:35 | [UB5.230](https://fosdem.org/2025/schedule/room/ub5230/)
- [Open source funding: you’re doing it wrong](https://fosdem.org/2025/schedule/event/fosdem-2025-5576-open-source-funding-you-re-doing-it-wrong/) | Andrew Nesbitt, Benjamin Nickolls | 16:30-17:00 | [K.3.601](https://nav.fosdem.org/l/k3601/)
- [GNOME Maps meets Transitous meets MOTIS](https://fosdem.org/2025/schedule/event/fosdem-2025-4105-gnome-maps-meets-transitous-meets-motis/) | Felix Gündling, Marcus Lundblad, Jonah Brüchert | 16:30-16:50 | [K.4.601](https://nav.fosdem.org/l/k4601/)


### Other Talk recommendations

Jean-Luc from CNX-Software has shared a nice list:

- [FOSDEM 2025 schedule - Embedded, Open Hardware, RISC-V, Edge AI, and more - CNX Software](https://www.cnx-software.com/2025/01/07/fosdem-2025-schedule-embedded-open-hardware-risc-v-edge-ai-and-more/)

Of course, there's a lot more, make sure to browse the full schedule [on the Web](https://fosdem.org/2025/schedule/) or in an app like [Kongress](https://linuxphoneapps.org/apps/org.kde.kongress/) or [Confy](https://linuxphoneapps.org/apps/net.kirgroup.confy/).

_Suggestions beyond the obvious? Please get in touch and tell :D_

### Stands

Now let's continue with what's mostly interesting for those who can make it to Burssels. [Stands](https://fosdem.org/2025/stands/), where projects can showcase their stuff - and where you can go to and see or discuss things :-)

In building K, level 1 we'll have:

* [Linux on Mobile (Mobian, Sailfish OS, PureOS, MNT Research)](https://linuxonmobile.net/#), [Location](https://nav.fosdem.org/l/k1-c-6/@1,132.35,291.33,5). _Will feature LINMOB and LinuxPhoneApps.org stickers!_ -
* [postmarketOS](https://postmarketos.org), [Location](https://nav.fosdem.org/l/k1-c-7/@1,130.31,291.3,5)

And in building AW, level 1 (at the opposite end of campus), you can find:

* [Furi Labs](https://furilabs.com/), [Location](https://nav.fosdem.org/l/aw-7/@1,356.83,88.99,5), likely showcasing their FLX1.

While you are in AW, make sure to check out the stands of KDE and GNOME, which may or may not show-case their mobile variants.

PINE64 does not have a stand this year, see [January Update: Thinking Out Of The Vox - PINE64](https://pine64.org/2025/01/11/january_2025/). 

Also, if you attend in person, let's not forget what FOSDEM is for: The hallway track, talking to people, making connections, having your mind blown :-)

### Evening Events

FOSDEM always has dinner to party style events in the evenings. That said, not being part of the official schedule, they can be hard to find, meaning: I have no idea, yet, and likely not enough mental bandwidth to update this page during the event.
The one event I know about is this one:

- [Sailfish Community Meetup 1st February 2025 Brussels - Announcements - Sailfish OS Forum](https://forum.sailfishos.org/t/sailfish-community-meetup-1st-february-2025-brussels/21765/9)


### Let's meet and get a sticker

I (Peter) will attend again. You'll find me at the stands (not during the time the FOSS on Mobile Dev Room takes place, and not all the time, but I plan to be there), where you can also grab [a sticker or two](https://fosstodon.org/@linmob/113902433316646092).

As the stands have been crowded in the past, I would like to hold a small, calm meetup on Sunday, Febuary 2nd, 14:00 / 2:00pm, meeting at roughly [this place outdoors (OSM)](https://www.openstreetmap.org/?mlat=50.81373&mlon=4.38212#map=19/50.81373/4.38212) / [nav](https://nav.fosdem.org/l/c:1:252.29:251.57/) and then finding a place inside the cafeteria. _It would be nice to meet some people I only know from the internet, so please come by!_

