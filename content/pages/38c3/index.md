+++
title = "Hey 38c3, have you heard of the Linux Mobile Revolution?"
path = "38c3" 
template = "single.html"
pagenate_by = 0
+++


Hello 38c3! If you haven't heard of us, we're a collection of individual
developers and self-organised projects working towards the end-goal of FREEING
YOUR PHONE.

<center>
Check <a href="https://events.ccc.de/congress/2024/hub/en/assembly/linmob/">the assembly page</a> to find out more - or check <a href="https://38c3.c3nav.de/l/linmob/">c3nav</a> to find us!</a>

Join the Session on Day 2, 4 pm: [Linux on Mobile - 38C3](https://events.ccc.de/congress/2024/hub/en/event/linux-on-mobile/)

Feel free to join our [Matrix room](https://matrix.to/#/#linmob-38c3:erebion.eu) to get in touch!

Like PDF? [Get something here!](https://framagit.org/1peter10/flyer/-/blob/master/build/flyer.pdf?ref_type=heads)
</center>

We're sick of our needs not being served, of [our phones being hostile towards
us](https://archive.fosdem.org/2022/schedule/event/mobile_social_dilemma/), and
of the eternal profit motive driving planned obselence and insecurity.

We believe that your phone should work for you; it shouldn't [sap your attention
with dark patterns](https://transcend.io/blog/dark-patterns-cpra), nor should it
stop receiving security updates or replacement parts while still more than
capable of serving your every day needs.

To this end, we work to enable support for our devices in the upstream Linux
kernel, replacing the [barely Linux](https://downstream.calebs.dev/) vendor kernel
(which is usually barely functional without proprietary Android-only userspace
components).

We work on making Linux [more
convergent](https://tuxphones.com/convergent-linux-phone-apps/) so that as we
continue to grow, we can provide more and more apps which are actually usable.
As well as building [many](https://plasma-mobile.org/), [different](https://phosh.mobi/), [kinds](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/) of [user interfaces](https://sxmo.org/).

<figure>
<img src="/media/2023/pile-of-phones.jpg" alt="A pile of different phones running different Linux distros" title="A whole lot of Linux phones" style="height:auto; margin:0;">
    <figcaption>A whole lot of Linux phones.</figcaption>
</figure>

## Get involved

We need your help! Whether it's testing your next Linux app in a mobile form
factor, working on support for a new device ([or reviving an old
one](https://wiki.postmarketos.org/wiki/Devices)), [testing our
code](https://postmarketos.org/blog/2023/05/21/call-for-testers/), [making
awesome
wallpapers](https://gitlab.com/postmarketOS/artwork/-/merge_requests/25), or
something else entirely, together we can build a truly user-owned ecosystem.

## Hardware vendors, hit us up

Whilst we always prefer to avoid building more future-landfill, if it's going to
be done then it should be done properly. If you're trying to build a phone or tablet that truly respects its users, get in touch! You can contact Caleb (postmarketOS developer) on Matrix `@caleb:postmarketos.org`.

## Join us

* <https://postmarketos.org/> - A real Linux distribution for phones
* <https://mobian.org/> - A debian derivative for mobile devices
* <https://matrix.to/#/#FOSSMobile:matrix.org> - The FOSS on Mobile Matrix space

## Other peoples' stuff: Relevant Talks and Sessions at 38C3

* Day 1, 11:00 - 11:40, GLITCH: [libobscura: Cameras are difficult :: 38C3 :: pretalx](https://fahrplan.events.ccc.de/congress/2024/fahrplan/talk/QSC7YF/)
* Day 2, 16:00 - 16:30, at standing tables near [Exploit Bar](https://38c3.c3nav.de/l/exploit-bar/@3,194.27,148.94,4.15): [Linux on Mobile - 38C3](https://events.ccc.de/congress/2024/hub/en/event/linux-on-mobile/)


## Things to check out

- Day 1: [Tüdeln - A rant on mobile devices - 38C3](https://events.ccc.de/congress/2024/hub/en/event/tudeln/)
- Day 2: [Tüdeln - Part 2 - 38C3](https://events.ccc.de/congress/2024/hub/en/event/tudeln-part-2/). _Should be an artsy installation of multiple phones._

