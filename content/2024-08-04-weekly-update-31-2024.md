+++
title = "Weekly GNU-like Mobile Linux Update (31/2024): Ubuntu Touch 20.04 OTA 5 and Jolla C2 news"
date = "2024-08-04T21:28:33Z"
draft = false
[taxonomies]
tags = ["Phosh", "Sxmo", "Sailfish OS", "Ubuntu Touch", "Jolla C2", "FuriLabs FLX1",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

This Week in Mobile Linux: A Ubuntu Touch OTA and a Q&A, Jolla shares details on the C2 and Sailfish 5, and much more, as improvements coming to Phosh, a FLX1 user report, and ... and ...

<!-- more -->
_Commentary in italics._


### Worth Reading
- What Should You Eat: [Down with the duopoly](https://whatshouldyoueat.com/2024/07/30/flx1/). _FLX1 Enthusiasm!_ 
- Downtown Doug Brown: [Upgrading my Chumby 8 kernel part 13: the end](https://www.downtowndougbrown.com/2024/08/upgrading-my-chumby-8-kernel-part-13-the-end/). _This is educational and inspiring!_
- Mobian Blog: [The European Union must keep funding free software](https://blog.mobian.org/posts/2024/07/19/open-letter-eu/)
- Phoronix: [Sovereign Tech Fund Preparing Fellowship Program For Open-Source Maintainers](https://www.phoronix.com/news/STF-Fellowship-Program)
- Ergaster: [Why are open source nonprofits so weird?](https://ergaster.org/posts/2024/07/31-open-source-nonprofits-weird/)
- Purism: [A Secure Supply Chain and Your Privacy: Smartphone Components](https://puri.sm/posts/a-secure-supply-chain-and-your-privacy-smartphone-components/)

### Worth Watching
- UBports: [Ubuntu Touch Q&A 146](https://www.youtube.com/watch?v=aVFGtLSE8do)
- Jolla: [Jolla C2 Community Phone: First look & Sailfish OS 5.0 preview #sailfishos #privacyfromfinland](https://www.youtube.com/watch?v=OVyOmaVySZc)
- qkall: [Death-scrolling on the Pinephone Pro (docked) - August 2024](https://www.youtube.com/watch?v=Sa783HQ5ZUU)
- Continuum Gaming: [Continuum Gaming E427: SeaPrint – Printing with your SFOS Smartphone](https://www.youtube.com/watch?v=fGoYyKjfbJU)

### Worth Noting
- [Lupo: "HackberryPi Zero: A handheld Linux terminal using Raspberry Pi Zero 2W as core with 4" 720X720 TFT display…"](https://tilde.zone/@lupo/112903826558798223)
- [David Heidelberg: "I finished the code for accelerated #OpenCL clEnqueueFillBuffer and also #OpenGL  ARB_clear_buffer_object for #Qualcomm #GPU Adreno 6xx and 7xx series.…"](https://floss.social/@okias/112896226786518687)
- [Cédric Bellegarde: "I'm now part of the @droidian …"](https://floss.social/@gnumdk/112883047881277165)
- [Matt Lewis: "I bought a second-hand Cosmo Communicator a few weeks ago for miscellaneous tinkering.…](https://mastodon.online/@semiprime/112903354565241802)
- [minute: "amazing: i can just flash the …"](https://mastodon.social/@mntmn/112870152396247846)
- [Genode: "The #Genode project will exhibit at @FrOSCon August 17/18.…"](https://floss.social/@genode/112879574574408348)
- PINE64official (Reddit): [Thoughts on the future of Pinephone Pro](https://www.reddit.com/r/PINE64official/comments/1efie1n/thoughts_on_the_future_of_pinephone_pro/)
- Purism community: [Will the Librem 5 be upgraded to the specs of the Liberty Phone at some point?](https://forums.puri.sm/t/will-the-librem-5-be-upgraded-to-the-specs-of-the-liberty-phone-at-some-point/24219)
- Lemmy - postmarketOS: [Anyone here using the Pixel 3a xl model?](https://lemmy.ml/post/18721101)
- Lemmy - linuxphones: [We need to support Linux Phones development more now than ever based on recent news](https://lemmy.ml/post/18711198)
- #LinuxMobile: [All set up for #FOSSY24!Come talk to us about #postmarketOS, #linuxmobile, or #systemd!](https://freeradical.zone/@craftyguy/112888062136233179)
- #LinuxMobile: [@greytheearthling &gt; I think we're *almost* reaching the point where #LinuxMobile is becoming an attractive non-hostile alternative for people who would otherwise choose a non-smart phone.For many people, #LinuxMobile has been a viable option for a long time already.For many others, it remains out of reach.The distinguishing factor I see so often is: The *platforms* aren't ready. Because the platform corporations are better served by a non-free app store, a non-free surveillance maze, a non-free refusal to interoperate with tools that better serve us.We have good operating systems for these devices, right now. What we need is a fair playing field, to let #FreeSoftware apps interoperate with where people are.And that can only be had by stripping #monopoly power from the platform corporations.@left_adjoint #ProtocolsNotPlatforms](https://theblower.au/@bignose/112867698020799787)

### More Software News

#### Gnome Ecosystem
- This Week in GNOME: [#159 Mounting Disks](https://thisweek.gnome.org/posts/2024/08/twig-159/)
- Planet GNOME: [Ptyxis in Fedora 40](https://blogs.gnome.org/chergert/2024/07/31/ptyxis-in-fedora-40/)

#### Phosh
- Guido Günther: [@arunmani and myself worked a bit on making Wi-Fi networks easier to select in #phosh&apos;s quick setting status page by giving the Wi-Fi list more vertical space and making each list box row a bit wider thus making it easier to select via touch (the same applies to other status pages like Bluetooth device selection):#LinuxMobile #gtk](https://social.librem.one/@agx/112887423843689351)
- Guido Günther: […and here are my #FreeSoftware bits for July:https://honk.sigxcpu.org/con/Free_Software_Activities_July_2024.html#LinuxMobile #phosh #debian #gnome](https://social.librem.one/@agx/112886183633653895)
- #phosh: [I have been daily driving FLX1 from Furilabs for few weeks now. Believe me this is the phone which will finally gain some market share to linux mobile. It has all the best achievements of the linux mobile community.It has been in market for month and it is allready better than any other linux phone that I ever had (I basicly have/had them all - starting from the BQ Aquaris) Feel free to ask questions I can try answer. #flx1 #furilabs #linuxmobile #mobilelinux #phosh @furilabs](https://fosstodon.org/@alaraajavamma/112888470762671650)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: Discover and more](https://pointieststick.com/2024/08/02/this-week-in-kde-discover-and-more/)
- kdab.com: [More Ways to Rust](https://www.kdab.com/more-ways-to-rust/)
- redstrate: [My work in KDE for July 2024](https://redstrate.com/blog/2024/07/my-work-in-kde-for-july-2024/)

#### Sxmo
- [Leaving of one of us — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3CD36IP3JDVPZH.H7Z7RW1PIZSI@willowbarraco.fr%3E)
- #sxmo: [the pinephone pro works pretty well when docked - https://youtu.be/Sa783HQ5ZUU#pinephonepro #linuxphones @linuxphones @linmob @linux #sxmo #sway #waybar #arch](https://mastodon.social/@qkall/112889297984356826)
- #sxmo: [I am thinking about resurrecting my #pinephone .  Does anyone use #sxmo as a daily driver?  It does look *very* interesting.The lack of password for unlock is a bit disturbing though.](https://emacs.ch/@graywolf/112880820639797584)

#### Sailfish OS
- Community News - Sailfish OS Forum: [Sailfish Community News, 1st August 2024 - Jolla C2](https://forum.sailfishos.org/t/sailfish-community-news-1st-august-2024-jolla-c2/19547)
- adampigg: [Sailfish and Ofono - Day 28 - Upgrading to 2.9](https://www.piggz.co.uk/sites/pgz/blog/sailfish-ofono-28) 
- flypig's Gecko log: [Day 308](https://www.flypig.co.uk/list?list_id=1166&list=gecko)
- flypig's Gecko log: [Day 307](https://www.flypig.co.uk/list?list_id=1165&list=gecko)
- flypig's Gecko log: [Day 306](https://www.flypig.co.uk/list?list_id=1164&list=gecko)
- flypig's Gecko log: [Day 305](https://www.flypig.co.uk/list?list_id=1163&list=gecko)
- flypig's Gecko log: [Day 304](https://www.flypig.co.uk/list?list_id=1162&list=gecko)
- flypig's Gecko log: [Day 303](https://www.flypig.co.uk/list?list_id=1161&list=gecko)
- flypig's Gecko log: [Day 302](https://www.flypig.co.uk/list?list_id=1160&list=gecko)
- #sailfishOS: [Get a closer look at the Jolla C2 Community Phone and a sneak peek at Sailfish OS 5.0! We're highlighting new features like the powerful camera, landscape/portrait mode, and expandable storage. See why the C2 is the ultimate Sailfish experience: https://youtu.be/OVyOmaVySZc?feature=shared#jollac2 #SailfishOS #privacyfromfinland](https://techhub.social/@jolla/112873937549190549)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-5 Focal Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-5-focal-release-3933)
- Ubuntu Touch Forums News: [Ubuntu Touch Q&amp;A 146 call for questions](https://forums.ubports.com/topic/10290/ubuntu-touch-q-a-146-call-for-questions)

#### Distributions
- postmarketOS pmaports issues: [Constant btrfs corruption under higher I/O load on PinePhone sd card independent of vendor (tested lexar and sandisk)](https://gitlab.com/postmarketOS/pmaports/-/issues/3058)
- postmarketOS pmaports Merge Requests: [enable EFI on sdm845 phones](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5369)
- Phoronix: [Immutable Version Of Arch-Based Manjaro Linux Available For Testing](https://www.phoronix.com/news/Immutable-Manjaro-Linux)

#### Apps
- [Jan Lukas Gernert: "Newsflash 3.3.3 (and .4) This …" - Mastodon](https://mastodon.social/@jangernert/112875804110777944)
- [Ryan "lofenyy" Medeiros: "Hey everyone. I've been getting a lot of questions asking about the feature set the sleep tracking will have upon completion.…"](https://social.linux.pizza/@lofenyy/112897457300144254)
- [Tobias Frisch: "Got some spare time testing around with RC1 of #Godot 3.6 which comes around with arm32 and arm64 export templates for #Linux now.…"](https://wehavecookies.social/@thejackimonster/112871735115271800)
- LinuxPhoneApps.org: Apps: [Alpaca](https://linuxphoneapps.org/apps/com.jeffser.alpaca/)
- LinuxPhoneApps.org: Apps: [Pulp](https://linuxphoneapps.org/apps/org.gnome.gitlab.cheywood.pulp/)

#### Kernel
- Phoronix: [Etnaviv NPU Driver Support Working Well For The NXP i.MX 8M Plus SoC](https://www.phoronix.com/news/Etnaviv-NPU-Performing-iMX8MP)
- Phoronix: [ARM Linux Maintainer Drafts Deprecation Timeline For Old Boards/Features](https://www.phoronix.com/news/ARM-Linux-Post-2024-Deprecation)
- phone-devel: [[PATCH 0/2] arm64: dts: qcom: msm8916-samsung-j3ltetw: Add initial device tree](http://lore.kernel.org/phone-devel/20240802080701.3643-1-linmengbo06890@proton.me/)
- phone-devel: [[PATCH v3 00/11] F(x)tec Pro1X feature expansion](http://lore.kernel.org/phone-devel/20240731-qx1050-feature-expansion-v3-0-b945527fa5d2@riseup.net/)

#### Stack
- Phoronix: [Blumenkrantz Continues On Big Mesa Code Refactoring](https://www.phoronix.com/news/Mesa-Juiciest-Refactor-Part-1)

#### Matrix
- Matrix.org: [This Week in Matrix 2024-08-02](https://matrix.org/blog/2024/08/02/this-week-in-matrix-2024-08-02/)
- Matrix.org: [The Matrix Conference Has an Exciting Lineup](https://matrix.org/blog/2024/08/matrix-conf-schedule/)


### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

