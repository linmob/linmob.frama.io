+++
title = "How about a more libre PinePhone with atk9k_htc?"
date = "2022-11-27T21:30:00Z"
updated = "2022-11-28T20:01:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "kill-switches", "libre firmware","postmarketOS","ath9k_htc",]
categories = ["projects", "howto",]
authors = ["peter"]
[extra]
edit_post_with_images = true
update_note = "Added footnotes 2 and 3, edited 'In real world use'."
+++

When you have look at Martijn Braam's excellent [mainline.space](https://mainline.espace/) you'll quickly note that the PinePhone with [megi's kernel](https://github.com/megous/linux) has a comparatively large number of changes from mainline. You may be shocked. But don't worry, it's mostly (~91% of added lines) the non-mainlined driver for the PinePhone's Realtek RTL8723CS 802.11 b/g/n WiFi with Bluetooth 4.0 chip.
<!-- more -->

### Evil firmware

Now, to make matters worse from the perspective of a libre computing mainline fanboy, this specific chip also requires some proprietary firmware, or dare I say, potentially evil proprietary firmware. This fact has been called out in a number of threads in forums, reddit, and likely the fediverse, too. Fortunately, you can use one of the PinePhone's tiny kill switches to disable this mess. But then what?

No WiFi?[^1] Only using your (depending on your region) more or less data allowance, that you have to pay some dodgy mobile carrier for through the nose? That's not really a good tradeoff for most of us.

### Hardware: USB Dongle + Adapters

Fortunately, I had some Atheros AR9271 USB WiFi dongles supported by the ATH9K_HTC driver, which support free firmware. They are made by [Azurewave and named AW-NU138](https://www.ebay.de/itm/192126210905), I got them on ebay a few years ago in order to never have to use a cable again (and probably a USB LAN dongle) when having downloaded the Debian ISO without proprietary firmware.[^2]

In order to connect them to my PinePhone easily, I looked for suitable adapters and finally settled on [these](https://www.amazon.de/-/en/gp/product/B08R8NDQ98/) [two](https://www.amazon.de/-/en/gp/product/B09TKL8STP/), which work quite well when combined. This is how the resulting dongle and the PinePhone look:

{{ threepicgallery() }}

### Software

As I run postmarketOS 22.06 with Phosh on my PinePhone these days, I sadly had to notice that [it does not come](https://gitlab.com/postmarketOS/pmaports/-/blob/master/device/main/linux-postmarketos-allwinner/config-postmarketos-allwinner.aarch64#L1973) with the necessary kernel module by default as I noticed that installing the firmware via

~~~~
sudo apk add linux-firmware-atk9k_htc
~~~~

was not enough to make things work.

But, fortunately, as I have been building a few postmarketOS packages of my own anyway and am familiar with the process of re-building a package, too - see my instructions on [linmobapps/apkbuilds](https://framagit.org/linmobapps/apkbuilds).

After a one-line change of line 1973 to

~~~~
CONFIG_ATH9K_HTC=m
~~~~

it was just a

~~~~
pmbootstrap build --force --strict --arch=aarch64 linux-postmarketos-allwinner
~~~~

to build the kernel package and finally, a

~~~~
pmbootstrap sideload --host HOST --user USER --arch ARCH --install-key linux-postmarketos-allwinner
~~~~

in order to install it. A reboot, and ... done! You can flip that WiFi/BT kill-switch to off now.[^3]

### In real world use

It works fine, but connecting the dongle definitely sips some extra power and reduces the screens' brightness, in some cases (depending on the brightness you have set) so much that the screen will go fully dark when you connect the dongle. It's also really annoying to forget the dongle, of course, and while general handling is okay, I have doubts that it is a good idea to not disconnect the dongle when pocketing the PinePhone. USB-C ports wear out eventually too, which thanks to the availability of spare parts for the PinePhone is not too much of a concern.

Also, there are upsides besides "more libre" and "if you alter the kernel patchset more than outlined above, you're closer to mainline": I had better WiFi performance with this setup.

### Conclusion

Is this worth it? I don't really know. This "dongle approach" definitely can increase privacy (you'll actually feel whether WiFi is on or not), and you'll use less proprietary firmware. But, when you forget to take your dongle with you, you are stuck with using cellular only - that is, until you turn off your PinePhone, take off the back cover and flip the WiFi/BT kill-switch back to on.


[^1]: Also, you'll lose Bluetooth, but assuming you're also a privacy nut, you won't have that enabled anyway, right? (And if so, there are these Cambridge Silicon Radio dongles, e.g. with the CSR8510 chipset, that just work out of the box - on postmarketOS at least.)

[^2]: This one of the few USB WiFi chipsets according to [this list on Wikipedia](https://en.wikipedia.org/wiki/Comparison_of_open-source_wireless_drivers#Status) that work without non-free firmware, and one of two chipsets that support WiFi B/G/N - check [Wireless Cat](https://wikidevi.wi-cat.ru/Ath9k_htc#Supported_.28probably.29_devices) for a list of hardware supported by the driver. If you don't have a AR9271 WiFi dongle, you may want to look into [hardware built around the AR9170 chipset (supported by the carl9170 driver)](https://wikidevi.wi-cat.ru/Carl9170), as it supports also the A band - or 5 GHz WiFi.

[^3]: There may be a simpler way to compile an additional kernel module on postmarketOS, but ... I stuck to what I already knew here.
