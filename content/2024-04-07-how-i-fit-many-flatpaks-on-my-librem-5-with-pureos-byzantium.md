+++
title = "How I fit many flatpaks on my Librem 5 with PureOS Byzantium"
date = "2024-04-07T11:07:45Z"
draft = false
[taxonomies]
tags = ["PureOS Byzantium", "PureOS", "flatpak", "Librem 5", "space management", "daily driving", "Debian"]
categories = ["howto"]
authors = ["Peter"]
+++

Earlier today, I came across [a post on Mastodon](https://fosstodon.org/@linmob/112228764489049639) about the 32GB eMMC in the Librem 5 is not enough for flatpaks.
Turns out, I have made it work for me, as `$ flatpak list | wc -l` (the line-count of the output of flatpak list, which lists all installed flatpaks after a one line heading): `117` and I want to share how I did this here and now.

<!-- more -->

### The Backstory (skip this if you're just looking for the solution)

I am still running PureOS Byzantium on my Librem 5, and I did so earlier when I figured this out. Back then Byzantium (or rather its Debian equivalent, Bullseye), was not yet `oldstable`, but it was perfectly clear, that when I wanted to have many mobile apps, I, despite the Purism' teams best effort, would have to use flatpak. 

Unfortunately, I relatively soonish noticed, that flatpak was using up space. And it seemingly ate more space then it did for me on other devices, which at that time likely was a PinePhone running postmarketOS stable or DanctNIX Arch Linux ARM. There's some folder with AppStream metadata (`/var/lib/flatpak/appstream`) growing like ... I don't want to write cancer, but you get it, right?

So I did some good old internet research, and figured out that this was a known bug:

[2032528 – Flatpak using excessive space in /var/lib/flatpak/appstream](https://bugzilla.redhat.com/show_bug.cgi?id=2032528) 

Not only was it a known bug, it was also fixed! Now how would I get this onto my PureOS? Compile this? Learn Debian packaging? Fortunately, before I embarked on such endeavours, I remembered that Debian has this thing called backports, and luckily, `bullseye-backports` has a newer, fixed flatpak for us!


### The fix

Not being a big Debian buff, I tried to figure out how to correctly install a package (and its dependencies) from backports. I figured, I could just use the approach I had used for the PureOS octarine repo, [(not) documented in an issue](https://source.puri.sm/Librem5/developer.puri.sm/-/issues/193). It has worked well for me since July 29, 2023, so I feel comfortable sharing it: 

As root, just paste this:

~~~
cat <<EOF >/etc/apt/sources.list.d/bullseye-backports.list
deb https://deb.debian.org/debian bullseye-backports main
EOF
~~~

Alternatively, using a text editor, you can do it like this (replace `vim` with your favorite text editor):

~~~
sudo vim /etc/apt/sources.list.d/bullseye-backports.list
~~~

Then write/paste:

~~~
deb https://deb.debian.org/debian bullseye-backports main
~~~

and save and exit.

To install flatpak from backports, you now have to run:

~~~
sudo apt update
sudo apt install -t bullseye-backports flatpak
~~~

Now feel free to delete the contents of `/var/lib/flatpak/appstream`, it did not have negative effects on my  install at least. With this change, it will store some files soon, but it won't grow like before.

(Obviously, the alternative would be to run another distribution: Be it Mobian, which ships Debian Trixie (current `testing`) or Bookworm (`stable`), or postmarketOS (23.12 (stable), or edge), which all ship newer releases of flatpak that don't have this (ancient) bug.)

### Other things I do to live with 32GB of space on PureOS

1. I regularily move the `.dng` (raw images) files from `~/Pictures` to my computer, where I could in theory use rawtherapee, darktable, or ansel to expertly develop them into jpgs (if I knew how to properly use these programs). They are roughly `~10 MB` each, so if you take a bunch of photos, they add up.

2. I have a "script" (that could also be an alias) called `save-space.sh` in `~/.local/bin`, which I run whenever I get warnings that storage is low:
~~~~
#!/bin/bash
sudo rm -rf /var/lib/flatpak/.removed/
sudo rm -rf /var/tmp/flatpak-cache-*/
sudo flatpak repair
sudo journalctl --vacuum-size=100M
~~~~

3. I also update my system with another "script" `update.sh` (that just as well should be an alias):
~~~~
#!/bin/bash
sudo apt clean
sudo apt autoremove
sudo flatpak update
sudo flatpak uninstall --unused
sudo apt update
sudo apt full-upgrade
~~~~

I hope this helps! Feedback is very welcome via email or replying to the [announcing post on the fediverse](https://fosstodon.org/@linmob/112229141559957708)!



