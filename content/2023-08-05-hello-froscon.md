+++
title = "Hello, FrOSCon 2023!"
date = "2023-08-05T05:55:00Z"
updated = "2023-08-05T16:40:00Z"
draft = false 
[taxonomies]
tags = ["FrOSCon", "FrOSCon 2023","travel", ]
categories = ["events", "personal",]
[extra]
author = "Peter"
update_note = "Added 'Looking back' section."
+++

On August 5th and 6th, FrOSCon, an annual Free and Open Source Software Conference in Sankt Augustin, Germany, will take place. 
<!-- more -->

This year, FrOSCon has a FOSS on Mobile dev room (C 117):
- [FrOSCon - Devrooms](https://froscon.org/en/program/project-rooms/)
- [Matrix room](https://matrix.to/#/#froscon-linux-mobile:matrix.org)
- [Pad](https://mensuel.framapad.org/p/froscon23-mobile)
- [Room Schedule – 2023-08-05](https://programm.froscon.org/2023/schedule/1.html#1))
- [Room Schedule – 2023-08-06](https://programm.froscon.org/2023/schedule/2.html#1)

You can also look forward one more talk:
- [Lecture: The year of Linux On Desktop^WMobile | Saturday | Schedule FrOSCon 2023](https://programm.froscon.org/2023/events/2950.html) by devrtz.

I, Peter, will be attending FrOSCon today, Saturday, August 5th, and likely hang out in the dev room mostly. If you're there, come by and please say hi!

_Sorry for posting about this so late, but due to a high day job workload, I only made the decision to go yesterday - it's also the reason for not attending both days._

### Looking back: Thank you all, this was great!

_It feels easier to just add to this post._
It's always fun to meet people, I had some great interactions, met some people I had not met IRL before. As always, there's the feeling I could have talked to even more people, but, hey, it's almost to much to summarize, still.

I enjoyed [Evangelos talk](https://media.ccc.de/v/froscon2023-2950-the_year_of_linux_on_desktop_wmobile) a lot - make sure to watch it. The lightning talks were great, we had (as far as I remember)

- a talk about XMPP,
- one about OpenWRT travel routers and how they can be used for blob free WiFi, 
- Guido talked about how to track how well apps fit the screen[^1],
- a talk about the history and challenges of LinuxPhoneApps.org,
- and another one about something something phone/tablet as a replacement for proper embeded control units.

I also have to say that seeing affenull's work on bringing postmarketOS to KaiOS phones was a blast - really, really well done!

I will not go through the contents of personal one-on-one interactions, but there were some important bits for this blog and LinuxPhoneApps.org. I'll list a few, so that I don't forget:

We need to come up
- with a way to track common gapps in the app ecosystem,
- a better way to track nascent projects, in order to reduce duplicate efforts (we now have [Apps to be added | LinuxPhoneApps](https://linuxphoneapps.org/lists/apps-to-be-added/), but it's not very useful and hard to contrbute to)
and ... and ... and...


#### Conclusion

It was a blast, I definitely will try to attend FrOSCon next year, hopefully both days then.

PS: If you talked to me (and also if you did not manage to), and would like to stay in touch or discuss things we did not get to discuss due to timing or me looking busy, please feel free to send an email or get in touch via Matrix or the fediverse. :-)


[^1]: I should have followed that one more closely, as I imagine the demoed way could be useful for LinuxPhoneApps in more than one way, but I got sidetracked by finishing my [slides](https://media.linmob.net/pdf/impromptu-talk_pub.pdf).