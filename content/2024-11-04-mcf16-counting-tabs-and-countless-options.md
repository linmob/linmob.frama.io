+++
title = "MCF16: Counting tabs and countless options"
date = "2024-11-04T12:47:39Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox"]
categories = ["projects"]
authors = ["Peter"]
+++

Since September I have a bit more time available (thanks to [§ 9a TzBfG](https://www.gesetze-im-internet.de/tzbfg/__9a.html)). Some of that time recently got put into [mobile-config-firefox](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox). And, guess what, it was quite productive.

<!-- more -->

The main unlock for productivity (aside from having had a period of three-day-weekends was a simple discovery, that I [made while evaluating an app](https://flathub.org/apps/dev.qwery.AddWater) for LinuxPhoneApps.org: 

It's possible to check for (custom) `about:config` values in CSS media queries. This means: It's possible to add in features and make more bold choices that users can then disable if they don't like the choice.

### Pending changes

The following Merge Requests have already landed: 
- [Make URLbar on the bottom optional through about:config (!56) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/56)
- [mobile-config-prefs.js: declare touch density (!57) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/57). _All credits for this one go to gnumdk, who's [pending MR](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/53) had been closed by the postmarketOS gitlab move._ 
- [userChrome/tabmenu: Add option to give more place to tabs (!58) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/58). _Again, credit goes to gnumdk!_

These Merge Requests are still pending:

- [userChrome/urlbar: Hide tracking container (!59) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/59)
- [mobile-config-autoconfig.js: set browser.urlbar.trimHttps to true (!60) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/60) _Just changing a default and document how to undo the change._
- [mobile-config-autoconfig.js: Use xdg-desktop-portal file-picker (!61) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/61)
- [tabcounter: Optionality, styling, documentation (!62) · Merge requests · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/merge_requests/62) _All credit goes to Emma / user0 !_

### Other fun stuff

Also, I've cooked up a [branch with minimal userChrome changes](https://gitlab.postmarketos.org/1peter10/mobile-config-firefox/-/tree/experiment/minimal-userchrome?ref_type=heads) and [opened an issue](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/issues/71), to highlight a thing that might be relevant for the [nlnet-funded upstreaming of changes](https://postmarketos.org/blog/2024/11/04/pmOS-update-2024-10/#nlnet-grants-accepted) to make Firefox itself more mobile-friendly.

### What's next?

Not much is planned, but here are two things that are obvious candidates to tackle next:

1. I want to look into cleaning up code. Everything that was necessary for Firefox before 128 (current ESR) should go once another release has been made. Also, it really annoys me that extensions don't use the full vertical space - an issue that's not present in my [branch with minimal userChrome changes](https://gitlab.postmarketos.org/1peter10/mobile-config-firefox/-/tree/experiment/minimal-userchrome?ref_type=heads), so removing stuff may be good, period!

2. [Menu bar moves to top when rotating to landscape (#37) · Issues · postmarketOS / mobile-config-firefox · GitLab](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/issues/37) is an interesting issue - Emma did a lot of work to make landscape really good, and for landscape it IMHO makes a lot of sense to bring the true-mobile-mode changes by default, as vertical space is just so scarce. This is a big change, as it requires changing the entire current media-query set up, but I bet PinePhone Keyboard users would love it.

That's it - thank you for reading! Please help test the new changes :-)

