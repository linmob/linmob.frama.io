+++
title = "KDE Neon User Edition on the Microsoft Surface Go 2"
date = "2022-07-18T12:10:00Z"
draft = true
[taxonomies]
tags = ["PinePhone","Community Editions","LinuxPhoneApps","PineTalk",]
categories = ["impressions"]
authors = ["peter"]
+++

Lately, I have aquired yet another tablet, a Microsoft Surface Go 2.[^1] As I wanted to try Plasma's latest release 5.25 and it's tablet features for TechnikTechnik and knew that KDE Neon would play well with Secure Boot, it was my first choice.
<!-- more -->

### Installation

Booting and installing KDE Neon User Edition was easy. I had used Windows' Disk Management util to free some space for it, than booted the USB drive with Neon on it and just breezed through the installer - thanks to the Type Cover, that I had also purchased. Without it, entering text would have proven painful, as Maliit is not installed by default.

### Getting set up

After that, I wanted to make it so that I could use this device without the Type Cover, too. Knowing a bit about Plasma Mobile and having followed Plasma development, too, I knew that I should go with the Wayland session and install a virtual keyboard.
So I opened Konsole and ran
~~~
$ apt search maliit
~~~
which returned a few results. Attempting to install multiple of them
~~~
sudo apt install maliit-keyboard maliit-inputcontext-gtk2 maliit-inputcontext-gtk3
~~~
sadly failed, but I managed to install the following packages
~~~~
sudo apt install maliit-framework maliit-keyboard
~~~~
successfully. After a restart, I had a working keyboard in the login manager (where the "virtual keyboard" button now worked), and since I was to impatient to find the proper setting in Plasma's cleaned up, but still somewhat confusing System Settings app, I just followed the [instructions in a blog post](https://blog.martin-graesslin.com/blog/2021/03/using-maliit-keyboard-in-a-plasma-wayland-session/) to enable the virtual keyboard in Plasma, too.

Now, since I am a Firefox guy, I wanted to get that keyboard to work in Firefox too, which it somehow didn't. Fortunately I noticed that Firefox looked a bit blurry, and realized that I would need to add a line to `/etc/environment`:
~~~~
MOZ_ENABLE_WAYLAND=1
~~~~
and boom, that was fixed.

Now, I have a tablet that runs Plasma Desktop, has a working virtual keyboard, screen rotation works, battery life is at least fine (standby is excellent, IMHO).

### Remaining issues

What does not work yet?

Well, the cameras don't. I could fix this with the [surface-linux kernel](https://github.com/linux-surface/linux-surface) (which I have installed) and by [following some more instructions](https://github.com/linux-surface/linux-surface/wiki/Camera-Support#build-libcamera-from-the-latest-git-source), but at this point I just can't be bothered to do so.

There's another problem, that I would like to see fixed: Copying and pasting is pretty broken on Plasma Wayland. It's fine between Angelfish and Kate (or other KDE native applications), but its broken for, e.g., Firefox. As Firefox is my favorite browser and has at least [one extension](https://addons.mozilla.org/en-US/firefox/addon/copy-as-markdown/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) that my Weekly Update workflow depends upon, this is unfortunate, but not to bad as writing is something best done with the Type Cover attached and thus running the X11 session for a bit is a non-issue.

Oh, and while we're talking web browsers: Chromium does not work with Maliit, whether the Wayland backend is enabled or not.

### Conclusions

TBW



[^1]: Oddly enough, the main purpose of buying this tablet was that I wanted to have a device to run Windows (especially 11) on, as I have to support Windows users for my job. While my work machine is already running Windows, I wanted to have a device which is not hampered by corporate IT's decisions. Since I always had a soft spot for the Surface Go line (tiny, fanless, ...) I looked for a used device on eBay and ended up buying a Surface Go 2, Intel Core m3-8100Y, 8GB Ram, 128 GB SSD, without LTE.
The [Surface Go 2](https://en.wikipedia.org/wiki/Surface_Go_2) is no longer the latest gen, Microsoft have replaced by the [Surface Go 3](https://en.wikipedia.org/wiki/Surface_Go_3). While the chips in the Go 3 may seem a lot newer (Pentium Gold [6500Y](https://ark.intel.com/content/www/us/en/ark/products/213357/intel-pentium-gold-6500y-processor-4m-cache-up-to-3-40-ghz.html) vs [4425Y](https://ark.intel.com/content/www/us/en/ark/products/192786/intel-pentium-gold-processor-4425y-2m-cache-1-70-ghz.html); Core [i3-10100Y](https://ark.intel.com/content/www/us/en/ark/products/213356/intel-core-i310100y-processor-4m-cache-up-to-3-90-ghz.html) vs Core [m3-8100Y](https://ark.intel.com/content/www/us/en/ark/products/185282/intel-core-m38100y-processor-4m-cache-up-to-3-40-ghz.html)), they are all based on Intel's 14nm Amber Lake Y architecture - in other words: The Go 3 is not enough of an improvement to pay its higher prices for me.
