+++
title = "MCF03: Overdue failures"
date = "2024-03-17T08:27:17Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox", "CSS hacks"]
categories = ["projects"]
authors = ["Peter"]
+++

Last time things were looking great, this time... I should have blogged more, and tried less maybe. Staying focussed is key to success, they say.

<!-- more -->

There's one good bit of news I can share: Yes, Firefox Nightly (and likely beta) is available on aarch64 through Snap! It works!

Other than that, I only have a bunch of failed attempts to share. Turns out, the "urlbar is all covered by buttons" issue is not yet fully solved, as there are yet more buttons that can appear. Namely the button that is huge and shows up when autoplay is blocked, and I wonder how useful the "Protections" button actually is.
See for yourself: 

<figure>
    <img src="tagesschau.png" alt="tagesschau.de and a packed URL bar" title="tagesschau.de and a packed URL bar" style="height:auto; margin:0;">
    <figcaption>tagesschau.de and a packed URL bar.</figcaption>
</figure>

Unfortunately, as they don't show on the debug page, [I resorted to guessing the CSS IDs](https://codeberg.org/1peter10/mobile-config-firefox/src/branch/more-urlbar-cleanup), so far unsuccessfully. There must be a better way to find this stuff out, right?

Before not being successful at this (I only found out last night), I tried to fix some menus that do not show up properly (e.g., the ones behind the urlbar buttons named above). I [tried](https://codeberg.org/1peter10/mobile-config-firefox/commit/d42fb8966d9f749429dbaab9efabd6c75faadf1b)  to do it the dumb and supposedly quick way by copy and pasting stuff from the fenix branch over, and rightfully failed. I will have to spend more time, carefully reading through user0's customizations. Another thing (working only on ESR, at least in postmarketOS) is [basically ready](https://codeberg.org/1peter10/mobile-config-firefox/commit/f9dc97147179b09f455e4d0732f588359c32b357), but likely needs discussion - some people may want translation popups.

(At least that's not all I've been [spending time](https://github.com/1peter10/abridge) on.)

Read you soon, likely on Tuesday.
