+++
title = "All Authors"
description = "Who writes and information about these persons."
draft = false
sort_by = "title"
# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.author_pages]
"peter" = "authors/peter.md"
"plata" = "authors/plata.md"
+++
