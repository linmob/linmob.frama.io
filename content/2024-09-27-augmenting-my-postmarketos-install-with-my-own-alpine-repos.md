+++
title = "Augmenting my postmarketOS installs with my own Alpine repos"
date = 2024-09-27T22:17:54Z
updated = 2024-09-28T06:19:57Z
draft = false
[taxonomies]
tags = ["contributing", "alpine", "postmarketOS", "own repo"]
categories = ["impressions", "software"]
authors = ["Peter"]
[extra]
update_note = "Typo fixes, mention pmbootrstrap more properly."
+++

_To finally have a post that isn't the weekly MobilePOSIX fun ride, let's share some experiences. This is likely quite wrong and not the right way, so don't take this as a how-to._

They say you should always learn the tools first, as it makes things easier. This is true. But in my reality I often find myself at a point where I am in a "I want to do this now" or "I've got 30 minutes, how far can I get in that time?" situation, and I have to decide between: "Can I half-ass this somehow, or is it just impossible right now?".

<!-- more -->.

This post is the result of many such situations. Now, [I was no stranger to APKBUILD-files](https://framagit.org/linmobapps/apkbuilds) (the recipes to build [Alpine Linux](https://alpinelinux.org/) [packages](https://gitlab.alpinelinux.org/alpine/aports)), that I began tinkering with after I had had some success with playing with [Arch Linux's PKGBUILDs](https://framagit.org/linmobapps/pkgbuilds). I won't go into the differences, let's just say they are more different than they seem at first glance.[^1] That said, both are relatively easy to understand if you've ever edited some files in `etc` and understand English: It's a file, you edit it, you try to build it, you fail, you try again (or you read `CMakeLists.txt` or `meson.build` or the dependency list of a PKGBUILD or rpm specfile from another distribution and look up the matching package names using `apk search`).

I started doing this many years ago using only the postmarketOS tools, as the [README.md](https://framagit.org/linmobapps/apkbuilds/-/blob/master/README.md?ref_type=heads#um--what-do-i-do-with-this) shows. It works well, and [pmbootstrap](https://wiki.postmarketos.org/wiki/Pmbootstrap) is the tool you should definitely spend time with first, it's wonderful.[^2] Since then I have read and worked through [Creating an Alpine package - Alpine Linux Wiki](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package).

At first I just used it to build some APKBUILDs for myself and for testing on my local stable install, so that I could install software that's stuck in testing on my stable install.
But of course it did not stay that way. These days I have two distrobox installs of Alpine on my NixOS aarch64 fruit company laptop, one stable (v3.20 as of this writing), the other edge.[^3] Both are used to play with my own APKBUILD before [I submit it to Alpine](https://gitlab.alpinelinux.org/1peter10), and also to get the additional packages I want. I use them with folders called `edge-extra`, `3.20-extra` which contain packages built on the respective base. My phones have the generated public key in `etc/apk/keys` and the repo address is on a cheap VPS to which I simply rsync my `~/packages` folder.

One of these is [Notorious](https://gitlab.gnome.org/GabMus/ntorious), which is still one of my favourite applications for managing my markdown notes (which all live in a folder called notorious, sorted by date), as it always puts the most recent notes on top. I really should try to get it into Alpine, despite it's last commit in January 2021.

I've also been experimenting with patched packages of Phosh, to test MRs in real life, or more recently to try out the mobile patch for dolphin (for this, a pmbootstrap setup is more than enough - don't set up your own repo for things you only want to try on one device).

So, all in all, if you are using postmarketOS and have played a lot with pmboostrap or Alpine, be sure to read and follow [Creating an Alpine package - Alpine Linux Wiki](https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package) if you ever want to play with unpackaged software at scale. It's fun!


[^1]: If you must insist, just go read [★ My First APKBUILD | not just serendipity](https://www.perrotta.dev/2022/01/my-first-apkbuild/)

[^2]: For many, if not all, use cases (basically, if you don't want to have custom packages on all your devices relatively easily, don't set up a repo), it's more than enough. You don't have to spend time setting up cross-compilation - it's really great!

[^3]: You can create these with ` distrobox create --image alpine:latest --name alpine-edge --unshare-group` - don't forget the `--unshare-group` part. If you don't have another `arch64 machine besides your phone (a Raspberry Pi 4 (+) system or similar should do), don't want to use an aarch64 VPS, and insist on using your phone to do something similar, make sure you find a way to cool it somehow.
