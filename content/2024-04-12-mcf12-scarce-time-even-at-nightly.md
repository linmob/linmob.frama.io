+++
title = "MCF12: Scarce Time, Even at Night(ly)"
date = "2024-04-12T19:34:28Z"
draft = false
[taxonomies]
tags = ["mobile-config-firefox", "Firefox Nightly", "Firefox", "Snap"]
categories = ["projects"]
authors = ["Peter"]
+++

There's not been much progress since my last post. Other things cluttered my agenda, and ... I also need my sleep lately, so I could not make up for day-time by using night-time.

<!-- more -->

### Snapd

Which brings me to my topic: I installed Firefox Nightly - as a [Snap](https://snapcraft.io/firefox)! The route to that was harder than you may think. First of all, Snaps, which I don't love, either, can not be installed on Alpine/postmarketOS. You can install snapd on PureOS Byzantium, but the snapd release in Byzantium/Bullseye is too old to successfully complete `snap install --edge firefox`. 

I then dabbled with Mobian, and after having downloaded the third image and failed to boot it from microSDXC on my Librem 5, I realized that I might just have to flash it to eMMC. So I flashed it to my secondary Librem 5, and ... not only is it delightful (and [ships the new release of mobile-config-firefox](https://tracker.debian.org/pkg/firefox-esr-mobile-config)), but I could finally install not only snapd, but also Firefox Nightly.

This led me to a setback:

[FireFox snap no longer reads /usr/lib/firefox for customization - snap - snapcraft.io](https://forum.snapcraft.io/t/firefox-snap-no-longer-reads-usr-lib-firefox-for-customization/29713/4)

Turns out: The snap does not read the customization from where `mobile-config-firefox` usually puts it. Now, of course, I can just put stuff where the Snap expects it, meaning, "policies stuff" in `etc/firefox/policies`, and userChrome/userContent somewhere else but ... It will be annoying, annoying enough to come up with a patch to the [Makefile](gitlab.com/postmarketOS/mobile-config-firefox/-/blob/master/Makefile), which would be another can of worms I do not necessarily want to deal with.

### Alternatives (and good news)

To find out whether I had missed an alternative to the Snap, I joined the [Firefox Nightly Matrix room](https://matrix.to/#/#nightly:mozilla.org) to ask.

Currently, it boils down to compiling it from source

* [Building Firefox On Linux — Firefox Source Docs documentation](https://firefox-source-docs.mozilla.org/setup/linux_build.html)
* [Cross-compilation — Firefox Source Docs documentation](https://firefox-source-docs.mozilla.org/build/buildsystem/cross-compile.html)

which is apparently less bad then it sounds - to quote Danny Colin: 

> "You can also pretty easily cross-compile one with the artifact mode (so you don't compile everything because parts of it is done on Mozilla infra)."

But that's not all: There's been some good news on the long standing issue regarding official ARM64/aarch64 ([1678342 - Please provide an official linux ARM64 build](https://bugzilla.mozilla.org/show_bug.cgi?id=1678342)) - just read comment 14 from (as of writing this post) 2 days ago.

_Read you again, hopefully sooner!_

