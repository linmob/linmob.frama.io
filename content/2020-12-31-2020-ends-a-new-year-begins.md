+++
title = "2020 Ends, a New Year Begins"
aliases = ["2020/12/31/2020-ends-a-new-year-begins.html"]
author = "Peter"
date = "2020-12-31T21:30:00Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Librem 5", "Snapdragon", "SoMainline", "Ubuntu Touch", "New Year"]
categories = ["impressions","personal",]
authors = ["peter"]
+++

2020 was a hard year for many, the pandemic made many societal problems more pronounced. But: It was also a great year, including (but not limited to) for Linux Phones.
<!-- more -->

The PinePhone shipped, the Librem 5 started shipping their mass-production batch, many new mobile Linux distributions spawned, Ubuntu Touch and Sailfish OS saw more development and running mainline Linux on [Snapdragon 835](https://twitter.com/kholk/status/1342128667552587777#m) [and 845](https://lbry.tv/@PortingLinuxPhones:5/Qualcomm-SoC-upstreaming-adventures-in-2020-xu-3CmRefvc:9) got really close.
I bet that 2021 is going to be an even greater year for the topics this blog is about. I am really looking forward to it!

I want to thank you all for reading this blog, interacting with my videos toots and tweets, sending me emails and so on &mdash; it made my 2020 a pretty good year! 

I wish you all a great new year 2021, read you soon!

Cheers!
