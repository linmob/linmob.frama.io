+++
title = "State of the Droidian Project"
date = "2023-07-19T15:00:00Z"
updated = "2023-07-19T15:00:00Z"
draft = false
[taxonomies]
tags = ["convergence", "Droidian", "Debian", "Halium", "libhybris"]
categories = ["guest post", "software",]
authors = ["FakeShell"]
[extra]
edit_post_with_images = true
update_note = ""
+++

<div style="padding-top:10px; padding-bottom:10px;">
<strong>Droidian</strong> is a Linux distribution based on <strong>Debian</strong> that is meant to run on Android-based devices. To accomplish this, it employs technologies such as <strong>Halium</strong> and <strong>libhybris</strong>.
</div>

<!-- more -->

<div style="text-align: center;">
    <img src="droidian-confirmed.jpg" alt="Droidian confirmed" style="height:auto; margin:0;">
</div>

<div style="padding-top:10px; padding-bottom:10px;">
There are a few Linux distributions available for Android devices right now, but they all have drawbacks that Droidian addresses. For example, <strong>Ubuntu Touch</strong> and <strong>SailfishOS</strong> feature a lot of old software in their stack. While mainline distributions like <strong>Mobian</strong> and <strong>PostmarketOS</strong> exist, they aren't very useable, with many of them lacking essential features on recent/modern consumer devices. Except for a few mainline phones, this is the current situation of mainline distributions and devices. Droidian resolves this issue by modifying the downstream linux kernel given by the device's vendor. In most circumstances, security fixes from kernel.org or other sources can still be applied to downstream kernels.
</div>

<div style="padding-top:10px; padding-bottom:10px;">
Droidian supports <strong>Phosh Shell</strong> (made by Purism), as well as <strong>Cutie Shell</strong>, which is being developed as part of a community effort. The project has progressed to the point where most phone functions are available and operational, including capabilities such as camera support, NFC, and fingerprint readers, which are rarely functioning on mainstream Linux phones. The current stable images are built on Debian bookworm, with Trixie hosting a development branch.
</div>

<div style="padding-top:10px; padding-bottom:10px;">
More info about the project, list of devices, update blog posts and our social media groups can be found <a href="https://droidian.org/">in our website</a>
</div>

<div style="display: flex; justify-content: space-between;">
    <img src="droidian-apps1.png" alt="Droidian apps" style="width:33%;height:auto;margin:0;">
    <img src="droidian-apps2.png" alt="Droidian apps" style="width:33%;height:auto;margin:0;">
    <img src="droidian-rain.png" alt="Droidian in the rain" style="width:33%;height:auto;margin:0;">
</div>

<div style="padding-top:10px; padding-bottom:10px;">
The project's purpose is to provide a current and useable Debian system pre-installed or user flashable on phones and tablets. While Droidian is officially available for 18 devices and another 7 unofficially, community members have booted Droidian on a large number of devices. Droidian porting is a simple yet time-consuming procedure that is primarily described by maintainers and contributors in our <strong>Telegram</strong> and <strong>Matrix</strong> communities.
</div>

<div style="display: flex; justify-content: space-between; padding-top:10px; padding-bottom:10px;">
    <video src="https://media.linmob.net/2023-07-19-droidian-project/droidian-fpd.mp4" alt="Droidian fingerprint" style="width:33%;height:auto;margin:0" controls></video>
    <video src="https://media.linmob.net/2023-07-19-droidian-project/droidian-usage.mp4" alt="Droidian usage" style="width:33%;height:auto;margin:0" controls></video>
    <video src="https://media.linmob.net/2023-07-19-droidian-project/droidian-mtp.mp4" alt="Droidian MTP" style="width:33%;height:auto;margin:0" controls></video>
</div>

<div style="padding-top:10px; padding-bottom:10px;">
Consider joining our community and installing or porting your device! We also invite Linux desktop users to join the project.
</div>
