+++
title = "Sad news: A micro-laptop dream is ending (for now)"
date = "2024-03-30T11:13:44Z"
updated = 2024-04-02
draft = false
[taxonomies]
tags = ["PinePhone Pro", "PinePhone Keyboard"]
categories = ["hardware", "shortform"]
authors = ["Peter"]
+++

This is just a quick follow-up on [Using the PinePhone Pro daily, despite having given up on it](https://linmob.net/using-the-pinephone-pro-daily-despite-having-given-up-on-it/):

<!-- more -->

Even with a third mainboard, I can't get my PinePhone Keyboard to work even somewhat reliably with my PinePhone Pro anymore. I noticed a few cracks when examining case, and hope that this is causing the problem (meaning, that it's not my PinePhone Pro having a (more expensive) issue). I would buy a new PinePhone Keyboard, but sadly enough, it's currently out-of-stock at both [pine64.com](https://pine64.com/product/pinephone-pinephone-pro-keyboard-case/) and [pine64eu.com](https://pine64eu.com/product/pinephone-pinephone-pro-keyboard-case/). Once it's available again, I'll likely get one, though (despite all the flakiness).

I've been wondering what alternatives I have to return to Micro Laptop Lifestyle: I have [another phone-ish keyboard](https://www.youtube.com/watch?v=C4RC3Miuo2A), that apparently [other people have converted into a bluetooth keyboard](https://jbmorley.co.uk/posts/2020-01-24-assembling-the-keyboard/), which would be convenient for use with all those phones that are (at least on mobile linux) USB clients only and can't be the host. It would also be great to outfit the PinePhone Keyboard with a different board that makes it a bluetooth or USB device; I vaguely recall having seen a project on some code forge where someone shared code for that, but I can't seem to find it now. _Hints (and even ideas) are very welcome, please send an email or [reply on the fediverse](https://fosstodon.org/@linmob/112184117182088781)!_

__Update, 2024-04-02:__ That seemingly broken PinePhone Keyboard works just fine with my PinePhone (it's still flaky with my PinePhone Pro). I'll try and find a way to fix this, wish me luck - and: _Suggestions welcome!_
