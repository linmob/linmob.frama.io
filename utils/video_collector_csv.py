#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse, urlunparse
import time
import csv
import os

# Define the custom user-agent to mimic a common web browser
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
}

def fetch_video_links(url, retries=3):
    # Retry mechanism
    for attempt in range(retries):
        print(f"Fetching videos from: {url} (Attempt {attempt + 1}/{retries})")
        response = requests.get(url, headers=headers)
        
        if response.status_code == 200:
            # Parse the HTML content of the page
            soup = BeautifulSoup(response.content, 'html.parser')

            # Find all the video sections
            video_sections = soup.find_all('div', class_='pure-u-1 pure-u-md-1-4')
            
            if not video_sections:
                print(f"No video sections found on {url}")  # Debugging line
            
            # Set to store the video entries (ensures unique entries)
            video_entries = set()

            for video in video_sections:
                # Extract the title
                title_tag = video.find('div', class_='video-card-row').find('a')
                if not title_tag:
                    print("Title tag not found, skipping this video.")  # Debugging line
                    continue
                
                title = title_tag.text.strip()
                
                # Extract the channel name
                channel_tag = video.find('p', class_='channel-name')
                if not channel_tag:
                    print("Channel tag not found, skipping this video.")  # Debugging line
                    continue
                
                channel_name = channel_tag.text.strip()
                
                # Extract the relative link
                video_link = title_tag['href']
                
                # Replace the domain part with youtube.com
                parsed_url = urlparse('https://www.youtube.com' + video_link)
                youtube_link = urlunparse(parsed_url._replace(netloc='www.youtube.com'))
                
                # Construct the video entry
                video_entry = {
                    'title': title,
                    'url': youtube_link,
                    'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    'source': channel_name,
                    'category': 'Video'
                }
                
                # Add to the set (automatically handles duplicates)
                video_entries.add(tuple(video_entry.values()))
            
            return video_entries

        elif response.status_code == 429:
            print(f"Rate limited. Waiting before retrying...")
            time.sleep(10 * (attempt + 1))  # Exponential backoff delay
        else:
            print(f"Failed to fetch {url} - Status code: {response.status_code}")
            break
    
    return set()

def append_to_csv(urls, csv_filename="entries.csv"):
    # Set to store all video entries (ensures unique entries)
    all_entries = set()
    
    for url in urls:
        # Fetch video entries for each URL
        entries = fetch_video_links(url)
        # Add fetched entries to the main set
        all_entries.update(entries)
    
    # Check if the file exists to determine if we need to write the header
    file_exists = os.path.isfile(csv_filename)
    
    # Append the data to the CSV file
    with open(csv_filename, 'a', newline='', encoding='utf-8') as csvfile:
        fieldnames = ['title', 'url', 'timestamp', 'source', 'category']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        
        # Write the header if the file doesn't exist
        if not file_exists:
            writer.writeheader()
        
        for entry in all_entries:
            title, url, timestamp, source, category = entry
            if ',' in title:
                title = f'"{title}"'  # Ensure the title is quoted if it contains a comma
            writer.writerow({
                'title': title,
                'url': url,
                'timestamp': timestamp,
                'source': source,
                'category': category
            })
    
    print(f"Video entries have been appended to {csv_filename}")

# List of URLs to scrape
urls = [
    "https://invidious.nerdvpn.de/search?q=postmarketos&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=postmarket%20os&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=ubuntu%20touch&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=ubports&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=maemo%20leste&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=sailfish%20os&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=sailfishos&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=jolla&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=pinephone&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=librem%205&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=librem5&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=droidian&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=furilabs&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=flx1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=danctnix&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=cyberdeck&page=1&date=week&type=all&duration=medium&sort=date",
    "https://invidious.nerdvpn.de/search?q=nemomobile&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=mutantc&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=luneos&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=capyloon&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=oniro&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=liberux&page=1&date=week&type=all&duration=none&sort=date",
    "https://invidious.nerdvpn.de/search?q=maemo&page=1&date=week&type=all&duration=none&sort=date",
]

# Fetch and append the video entries to the CSV file
append_to_csv(urls)

