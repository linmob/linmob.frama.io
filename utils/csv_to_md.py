#!/usr/bin/env python3

from common import *

import argparse
from dataclass_csv import DataclassReader
from datetime import datetime


def article_version() -> str:
    import feedparser
    import re

    feed = feedparser.parse('https://linmob.net/feed.xml')
    for feed_entry in feed.entries:
        version = re.match(r'Weekly.*\((\d\d)/(20\d\d)\).*', feed_entry.title)
        if version:
            if int(version.group(2)) == datetime.now().year:
                return str(int(version.group(1)) + 1).zfill(2)
            else:
                return '01'

def current_utc_time():
    return datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

def main():
    current_date = current_utc_time()[:10]
    default_output = '{}-weekly-update-{}-{}.md'.format(current_date, article_version(), datetime.now().year)
    parser = argparse.ArgumentParser(description='Convert .csv to .md.')
    parser.add_argument('--input', help='input filename (default: entries.csv)', default='entries.csv')
    parser.add_argument('--output', help='output filename (default: {})'.format(default_output), default=default_output)
    parser.add_argument('--author', help="author's name (default: auto)", default='auto')    

    args = parser.parse_args()

    entries_by_category = {}
    with open(args.input, 'r') as f:
        entries = DataclassReader(f, Entry)

        for entry in entries:
            if entry.category not in entries_by_category:
                entries_by_category[entry.category] = []
            entries_by_category[entry.category].append(entry)
    
    # List of allowed categories for tags
    allowed_categories_for_tags = ['Sailfish OS', 'Ubuntu Touch', 'Maemo Leste', 'Nemo Mobile', 'Maui Project', 'Capyloon', 'Sxmo', 'Phosh']

    # Create an automated list of category tags filtered by the allowed categories, join it into a string
    category_tags = ['"{}"'.format(category.replace('_', ' ')) for category in entries_by_category.keys() if category in allowed_categories_for_tags]

    category_tags_str = ', '.join(category_tags)
 
    with open(args.output, 'w') as f:
        f.write(
'''+++
title = "Weekly GNU-like Mobile Linux Update ({}/{})"
date = "{}"
draft = true
[taxonomies]
tags = [{}]
categories = ["weekly update"]
authors = ["{}",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Short teaser goes here

<!-- more -->
_Commentary in italics._

'''.format(article_version(), datetime.now().year, current_utc_time(), category_tags_str, args.author)
        )

        current_category = Category.NONE
        # Worth Reading 
        if Category.MISC.value in entries_by_category:
            misc_entries = entries_by_category[Category.MISC.value]
            f.write('\n### Worth Reading\n')
            for entry in misc_entries:
                f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        # Worth listening
        if Category.PODCASTS.value in entries_by_category:
            podcast_entries = entries_by_category[Category.PODCASTS.value]
            f.write('\n### Worth Listening\n')
            for entry in podcast_entries:
                f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        # Worth watching at the end 
        if Category.VIDEO.value in entries_by_category:
            video_entries = entries_by_category[Category.VIDEO.value]
            f.write('\n### Worth Watching\n')
            for entry in video_entries:
                f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        # Worth Noting 
        if Category.WORTHNOTING.value in entries_by_category:
            noteworthy_entries = entries_by_category[Category.WORTHNOTING.value]
            f.write('\n### Worth Noting\n')
            for entry in noteworthy_entries:
                f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        # the rest
        f.write('\n### More Software News\n')
        for category, entries in entries_by_category.items():
            if category not in [Category.MISC.value, Category.WORTHNOTING.value, Category.PODCASTS.value, Category.VIDEO.value]:
                if category != current_category:
                    f.write('\n#### ' + category + '\n')
                    current_category = category
                for entry in entries:
                    f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        
        f.write(
'''

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

''')

if __name__ == "__main__":
    main()
