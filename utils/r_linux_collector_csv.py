#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
from datetime import datetime
from urllib.parse import urlparse, urlunparse
import time
import csv
import os

# Define the custom user-agent to mimic a common web browser
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
}


def fetch_reddit_posts(url, retries=3):
    # Retry mechanism
    for attempt in range(retries):
        print(f"Fetching search results from: {url} (Attempt {attempt + 1}/{retries})")
        response = requests.get(url, headers=headers)

        if response.status_code == 200:
            # Parse the HTML content of the page
            soup = BeautifulSoup(response.content, "html.parser")

            # Find all posts
            posts = soup.find_all("div", class_="post")

            if not posts:
                print(f"No posts found on {url}")  # Debugging line

            # Set to store posts (ensures unique entries)
            reddit_posts = set()

            for post in posts:
                # Extract the title
                title_tag = (
                    post.find("h2", class_="post_title").find("a").find_next("a")
                )
                if not title_tag:
                    print("Title tag not found, skipping this post.")  # Debugging line
                    continue

                title = title_tag.text.strip()

                # Extract the relative link
                post_link = "https://old.reddit.com" + title_tag["href"]

                # Extract the date
                time_tag = post.find("span", class_="created")
                post_date_str = time_tag["title"]
                post_date = datetime.strptime(
                    post_date_str, "%b %d %Y, %H:%M:%S %Z"
                ).strftime("%Y-%m-%d %H:%M:%S")

                # Construct the video entry
                reddit_post = {
                    "title": title,
                    "url": post_link,
                    "timestamp": post_date,
                    "source": "r/linux",
                    "category": "Worth Noting",
                }

                # Add to the set (automatically handles duplicates)
                reddit_posts.add(tuple(reddit_post.values()))

            return reddit_posts

        elif response.status_code == 429:
            print(f"Rate limited. Waiting before retrying...")
            time.sleep(10 * (attempt + 1))  # Exponential backoff delay
        else:
            print(f"Failed to fetch {url} - Status code: {response.status_code}")
            break

    return set()


def append_to_csv(urls, csv_filename="entries.csv"):
    # Set to store all video entries (ensures unique entries)
    all_entries = set()

    for url in urls:
        # Fetch video entries for each URL
        entries = fetch_reddit_posts(url)
        # Add fetched entries to the main set
        all_entries.update(entries)

    # Check if the file exists to determine if we need to write the header
    file_exists = os.path.isfile(csv_filename)

    # Append the data to the CSV file
    with open(csv_filename, "a", newline="", encoding="utf-8") as csvfile:
        fieldnames = ["title", "url", "timestamp", "source", "category"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        # Write the header if the file doesn't exist
        if not file_exists:
            writer.writeheader()

        for entry in all_entries:
            title, url, timestamp, source, category = entry
            if "," in title:
                title = (
                    f'"{title}"'  # Ensure the title is quoted if it contains a comma
                )
            writer.writerow(
                {
                    "title": title,
                    "url": url,
                    "timestamp": timestamp,
                    "source": source,
                    "category": category,
                }
            )

    print(f"sub-reddit posts entries have been appended to {csv_filename}")


# List of URLs to scrape
urls = [
    "https://rl.bloat.cat/r/linux/search?q=flair_name%3A%22Mobile+Linux%22&restrict_sr=on&sort=new&t=week",
]

# Fetch and append the video entries to the CSV file
append_to_csv(urls)
