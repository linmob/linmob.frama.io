#!/usr/bin/env python3

import csv

# Assuming urls_to_remove is a list of URLs you want to remove
urls_to_remove = [
    "https://daltondur.st/about/",
    "http://lowendlibre.github.io/about/",
    "https://carlschwan.eu/1/01/01/kde-websites-and-documentation-news-december-2020-march-2021/",
    "https://carlschwan.eu/1/01/01/working-with-qtwidgets/",
    "https://justsoup321.codeberg.page/about/",
    "https://www.cais.de/create_swap/",
]  # Replace with actual URLs or read from a file

title_keywords_to_remove = [
    "intel",
    "pinecil",
    "rockpro",
    "rock pro",
    "web review",
    "krita",
    "kdenlive",
]  # Add more keywords as needed


def should_remove_line(source, seen_urls, title):
    lower_title = title.lower()
    lower_source = source.lower()

    # Check if title contains any of the specified keywords
    if any(keyword in lower_title for keyword in title_keywords_to_remove):
        return True

    # Check for duplicate URL and specific sources
    if lower_url in seen_urls:
        return True

    return False


with open("entries.csv", "r") as in_file, open(
    "entries-dedup.csv", "w", newline=""
) as out_file:
    csv_reader = csv.reader(in_file)
    csv_writer = csv.writer(out_file)

    seen_urls = set()

    for row in csv_reader:
        if len(row) < 5:
            continue  # Skip lines that don't have enough columns

        title, url, timestamp, source, category = row

        lower_url = url.lower()

        # Remove specific URLs
        if lower_url in urls_to_remove:
            continue

        # Check if the line should be removed based on title, URL, and source
        if should_remove_line(source, seen_urls, title):
            continue

        seen_urls.add(lower_url)
        csv_writer.writerow(row)
