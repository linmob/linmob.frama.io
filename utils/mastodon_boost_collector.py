from mastodon import Mastodon
from datetime import datetime, timedelta, timezone
import csv
from bs4 import BeautifulSoup, NavigableString

# Step 1: Load the access token from a file
def load_access_token(file_path):
    with open(file_path, 'r') as file:
        return file.read().strip()

# Load the access token
access_token = load_access_token('mastodon_access_token')

# Step 2: Authenticate
mastodon = Mastodon(
    access_token=access_token,
    api_base_url='https://fosstodon.org'  # Replace with your Mastodon instance URL
)

# Step 3: Strip HTML and ensure spaces around links
def strip_html(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    
    # Handle <a> tags specifically
    for a in soup.find_all('a'):
        if a.string:  # If the link has text
            # Add space before the link if not present
            if a.previous_sibling and isinstance(a.previous_sibling, NavigableString):
                if not a.previous_sibling.string.endswith(' '):
                    a.insert_before(' ')
            elif not a.previous_sibling:  # If there's no previous sibling, it's the first element
                a.insert_before(' ')
            
            # Add space after the link if not present
            if a.next_sibling and isinstance(a.next_sibling, NavigableString):
                if not a.next_sibling.string.startswith(' '):
                    a.insert_after(' ')
            elif not a.next_sibling:  # If there's no next sibling, it's the last element
                a.insert_after(' ')
    
    return soup.get_text()

# Step 4: Fetch the Re-tooted/Boosted Posts
def fetch_boosted_posts(mastodon, max_id=None, since_date=None):
    boosted_posts = []
    timeline = mastodon.account_statuses(mastodon.me()['id'], limit=40, max_id=max_id)
    
    while timeline:
        for status in timeline:
            if status['reblog'] is not None:  # Check if the post is a boost
                boosted_post_date = status['created_at']  # Already a datetime object with timezone
                if boosted_post_date >= since_date:
                    boosted_posts.append(status)
                else:
                    return boosted_posts  # Stop if the post is older than the since_date

        max_id = timeline[-1]['id'] - 1
        timeline = mastodon.account_statuses(mastodon.me()['id'], limit=40, max_id=max_id)
    
    return boosted_posts

# Step 5: Set the time limit (past week)
one_week_ago = datetime.now(timezone.utc) - timedelta(days=7)  # Make one_week_ago timezone-aware

# Fetch all boosted posts within the past week
boosted_posts = fetch_boosted_posts(mastodon, since_date=one_week_ago)

# Step 6: Prepare data for CSV
csv_data = []
for post in boosted_posts:
    # Strip HTML and ensure spaces around links
    title = strip_html(post['reblog']['content'])
    
    # Escape title if it contains a comma
    if ',' in title:
        title = f'"{title}"'
    
    url = post['reblog']['url']
    timestamp = post['created_at'].strftime('%Y-%m-%d %H:%M:%S')
    source = post['reblog']['account']['username']
    category = "Worth Noting"
    
    csv_data.append([title, url, timestamp, source, category])

# Step 7: Read the existing CSV file
csv_file_path = 'entries.csv'  # Replace with your actual file path

with open(csv_file_path, mode='r', newline='', encoding='utf-8') as file:
    reader = list(csv.reader(file))
    header = reader[0]
    existing_data = reader[1:]

# Step 8: Combine the new data with the existing data, inserting after the header
combined_data = [header] + csv_data + existing_data

# Step 9: Write the combined data back to the CSV file
with open(csv_file_path, mode='w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)
    writer.writerows(combined_data)

print(f"Inserted {len(csv_data)} boosted posts into {csv_file_path} starting from the second line.")

