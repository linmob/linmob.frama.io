#!/usr/bin/env python3

from common import *

import argparse
from dataclass_csv import DataclassWriter
from datetime import datetime
import feedparser
from time import mktime
import re
import urllib.parse

keywords = {
    Category.TRASH: [
        "amd",
        "intel",
        "nvidia",
        "via",
        "loongson",
        "spam",
        "valve",
        "web review",
        "linux am dienstag",
        "radeon",
        "krita",
        "kali",
        "termux",
        "web review",
        "wine",
        "directx",
        "pinebook",
        "pinecil",
        "rockpro",
        "rock64",
        "quartz64",
        "rockpro64",
        "pinecil",
        "restaurant",
        "microsoft",
        "unlock",
        "pubg mobile",
        "godnemo",
    ],
    Category.GNOME: ["gnome", "gtk", "clutter", "mutter", "libadwaita"],
    Category.RELEASES: ["release"],
    Category.MAUI: ["maui"],
    Category.NEMO_MOBILE: ["nemo"],
    Category.MAEMO_LESTE: ["maemo", "hildon"],
    Category.CAPYLOON: ["capyloon", "firefoxos"],
    Category.PHOSH: ["phoc", "phosh"],
    Category.PLASMA: ["kde", "plasma", "qt"],
    Category.SAILFISH_OS: ["sailfish"],
    Category.UBUNTU_TOUCH: ["ubuntu", "ubports"],
    Category.KERNEL: ["kernel", "megi"],
    Category.APPS: [],
    Category.STACK: [
        "pipewire",
        "wayland",
        "freedreno",
        "hantro",
        "cedrus",
        "mesa",
        "lima",
        "panfrost",
    ],
    Category.MATRIX: ["matrix", "fluffychat", "nheko", "hydrogen", "element"],
    Category.DISTRIBUTIONS: [
        "arch",
        "fedora",
        "glodroid",
        "maemo",
        "manjaro",
        "mobian",
        "postmarket",
        "sailfish",
        "opensuse",
        "openmandriva",
        "gentoo",
        "nixos",
        "debian",
        "devuan",
    ],
    Category.WORTHNOTING: [],
    Category.PODCASTS: [],
    Category.VIDEO: [],
    Category.MISC: [],
}

video_filter_keywords = [
    "pre-market",
    "pubg",
    "PUGB",
    "stockmarket",
    "Grizmann",
    "Harry Cane",
    "Game Breaker",
    "Antututu",
    "crdroid",
    "android 13",
]

sources = [
    # Software releases
    Source(
        "biktorgj Modem Firmware",
        SourceType.RSS,
        "https://github.com/the-modem-distro/pinephone_modem_sdk/releases.atom",
        Category.RELEASES,
    ),
    Source(
        "Tow-Boot",
        SourceType.RSS,
        "https://github.com/Tow-Boot/Tow-Boot/releases.atom",
        Category.RELEASES,
    ),
    Source(
        "libcamera",
        SourceType.RSS,
        "https://github.com/libcamera-org/libcamera/releases.atom",
        Category.RELEASES,
    ),
    # Kernel
    Source(
        "phone-devel",
        SourceType.RSS,
        "https://lore.kernel.org/phone-devel/new.atom",
        Category.KERNEL,
    ),
    # Distributions
    Source(
        "Droidian Blog",
        SourceType.RSS,
        "https://droidian.org/blog/index.xml",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Droidian (Mastodon)",
        SourceType.MSTDN,
        "https://fosstodon.org/@droidian.rss",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Lemmy - postmarketOS",
        SourceType.RSS,
        "https://lemmy.ml/feeds/c/postmarketos.xml?sort=Active",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian Blog",
        SourceType.RSS,
        "https://blog.mobian-project.org/index.xml",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian Wiki",
        SourceType.RSS,
        "https://wiki.mobian-project.org/feed.php",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian (Mastodon)",
        SourceType.MSTDN,
        "https://fosstodon.org/@mobian.rss",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobile NixOS news",
        SourceType.RSS,
        "https://mobile.nixos.org/index.xml",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Nitrux",
        SourceType.RSS,
        "https://nxos.org/feed/",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS Blog",
        SourceType.RSS,
        "https://postmarketos.org/blog/feed.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Breaking updates in pmOS edge",
        SourceType.RSS,
        "https://postmarketos.org/edge/feed.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS (Mastodon)",
        SourceType.MSTDN,
        "https://fosstodon.org/@postmarketOS.rss",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS pmaports issues",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/pmaports/-/issues.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS pmaports Merge Requests",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/pmaports/-/merge_requests.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS pmbootstrap issues",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/issues.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS pmbootstrap Merge Requests",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/merge_requests.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS Wiki New Pages",
        SourceType.RSS,
        "https://wiki.postmarketos.org/index.php?title=Special:NewPages&feed=atom&hideredirs=2",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS pmbootstrap tags",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/pmbootstrap/-/tags?feed_token=glft-0b7d745c15a64fef3ea42ff33503a08d589c33747066e3560562879ba4748db3-108&format=atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS mobile-config-firefox tags",
        SourceType.RSS,
        "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/tags?feed_token=glft-3080298580f7816f16a022ba9432955364948da7420ef1f564e1e9de19f986af-108&format=atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "postmarketOS Wiki Recent Changes",
        SourceType.RSS,
        "https://wiki.postmarketos.org/index.php?title=Special:RecentChanges&feed=atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian mobian-recipes Merge Requests",
        SourceType.RSS,
        "https://salsa.debian.org/Mobian-team/mobian-recipes/-/merge_requests.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian mobian-recipes Issues",
        SourceType.RSS,
        "https://salsa.debian.org/Mobian-team/mobian-recipes/-/issues.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Mobian Wiki",
        SourceType.RSS,
        "https://wiki.debian.org/Mobian?diffs=1&show_att=1&action=rss_rc&unique=0&page=Mobian&ddiffs=1",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "GloDroid for PinePhone",
        SourceType.RSS,
        "https://github.com/GloDroidCommunity/pine64-pinephone/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "GloDroid for PinePhone Pro",
        SourceType.RSS,
        "https://github.com/GloDroidCommunity/pine64-pinephonepro/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Pine64-Arch",
        SourceType.RSS,
        "https://github.com/dreemurrs-embedded/Pine64-Arch/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "DanctNIX (Mastodon)",
        SourceType.MSTDN,
        "https://fosstodon.org/@danctnix.rss",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Kupfer",
        SourceType.RSS,
        "https://gitlab.com/kupfer/kupfer.gitlab.io/-/commits/main?format=atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "RhinoLinux",
        SourceType.RSS,
        "https://github.com/rhino-linux/website/commits/main.rss",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Manjaro PinePhone Plasma Mobile",
        SourceType.RSS,
        "https://github.com/manjaro-pinephone/plasma-mobile/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "PinePhone Fedora Image Generation Script ",
        SourceType.RSS,
        "https://github.com/nikhiljha/pp-fedora-sdsetup/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Manjaro PinePhone Phosh",
        SourceType.RSS,
        "https://github.com/manjaro-pinephone/phosh/releases.atom",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Manjaro Blog",
        SourceType.RSS,
        "https://blog.manjaro.org/feed/",
        Category.DISTRIBUTIONS,
    ),
    Source(
        "Yocto Project Blogs",
        SourceType.RSS,
        "https://www.yoctoproject.org/blogs/feed/",
        Category.DISTRIBUTIONS,
    ),
    #### Apps
    Source(
        "Purism forums: List of Apps that fit and function well",
        SourceType.MSTDN,
        "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361.rss",
        Category.APPS,
    ),
    Source(
        "LinuxPhoneApps.org: Apps",
        SourceType.RSS,
        "https://linuxphoneapps.org/apps/atom.xml",
        Category.APPS,
    ),
    Source(
        "LinuxPhoneApps.org: Games",
        SourceType.RSS,
        "https://linuxphoneapps.org/games/atom.xml",
        Category.APPS,
    ),
    Source(
        "postmarketOS: Apps by Category",
        SourceType.RSS,
        "https://wiki.postmarketos.org/index.php?title=Applications_by_category&feed=atom&action=history",
        Category.APPS,
    ),
    #### Worth noting
    Source(
        "@linmob@fosstodon.org",
        SourceType.MSTDN,
        "https://fosstodon.org/users/linmob.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "r/MobileLinux",
        SourceType.RSS,
        "https://www.reddit.com/r/mobilelinux/.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "r/postmarketOS",
        SourceType.RSS,
        "https://www.reddit.com/r/postmarketos/.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "PinePhoneOfficial (Reddit)",
        SourceType.RSS,
        "https://www.reddit.com/r/pinephoneofficial.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "PinePhone (Reddit)",
        SourceType.RSS,
        "https://www.reddit.com/r/pinephone.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "PINE64official (Reddit)",
        SourceType.RSS,
        "https://www.reddit.com/r/pine64official/new/.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "Purism (Reddit)",
        SourceType.RSS,
        "https://www.reddit.com/r/purism/new/.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "Purism community",
        SourceType.RSS,
        "https://forums.puri.sm/c/librem/phones/11.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "@cas@treehouse.systems",
        SourceType.MSTDN,
        "https://treehouse.systems/users/cas.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "@dos@librem.one",
        SourceType.MSTDN,
        "https://social.librem.one/users/dos.rss",
        Category.WORTHNOTING,
    ),
    #### Gnome Ecosystem
    Source(
        "This Week in GNOME",
        SourceType.RSS,
        "https://thisweek.gnome.org/index.xml",
        Category.GNOME,
    ),
    Source(
        "GNOME Shell & Mutter",
        SourceType.RSS,
        "https://blogs.gnome.org/shell-dev/feed/",
        Category.GNOME,
    ),
    Source(
        "Planet GNOME",
        SourceType.RSS,
        "https://planet.gnome.org/atom.xml",
        Category.GNOME,
    ),
    #### Phosh
    Source(
        "Phosh.mobi blog",
        SourceType.RSS,
        "https://phosh.mobi/posts/index.xml",
        Category.PHOSH,
    ),
    Source(
        "Phosh.mobi",
        SourceType.RSS,
        "https://phosh.mobi/releases/index.xml",
        Category.PHOSH,
    ),
    Source(
        "Guido Günther",
        SourceType.MSTDN,
        "https://social.librem.one/users/agx.rss",
        Category.PHOSH,
    ),
    Source(
        "Phosh (Mastodon)",
        SourceType.MSTDN,
        "https://fosstodon.org/users/phosh.rss",
        Category.PHOSH,
    ),
    Source(
        "#phosh",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/phosh.rss",
        Category.PHOSH,
    ),
    #### Plasma Ecosystem
    Source(
        "Nate Graham",
        SourceType.RSS,
        "https://blogs.kde.org/categories/this-week-in-plasma/index.xml",
        Category.PLASMA,
    ),
    Source(
        "Carl Schwan et al.",
        SourceType.RSS,
        "https://blogs.kde.org/categories/this-week-in-kde-apps/index.xml",
        Category.PLASMA,
    ),
    Source(
        "KDE Announcements",
        SourceType.RSS,
        "https://kde.org/index.xml",
        Category.PLASMA,
    ),
    Source(
        "KDE e.V. News",
        SourceType.RSS,
        "https://ev.kde.org/index.xml",
        Category.PLASMA,
    ),
    Source(
        "KDE Mentorship",
        SourceType.RSS,
        "https://mentorship.kde.org/index.xml",
        Category.PLASMA,
    ),
    Source(
        "Bhushan Shah",
        SourceType.RSS,
        "https://blog.bshah.in/feed.xml",
        Category.PLASMA,
    ),
    Source("Devin Lin", SourceType.RSS, "https://espi.dev/index.xml", Category.PLASMA),
    Source(
        "Volker Krause",
        SourceType.RSS,
        "https://www.volkerkrause.eu/feed.xml",
        Category.PLASMA,
    ),
    Source(
        "Carl Schwan",
        SourceType.RSS,
        "https://carlschwan.eu/index.xml",
        Category.PLASMA,
    ),
    Source(
        "Planet KDE",
        SourceType.RSS,
        "https://planet.kde.org/atom.xml",
        Category.PLASMA,
    ),
    Source(
        "#plasmamobile",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/plasmamobile.rss",
        Category.PLASMA,
    ),
    #### Maui
    Source(
        "@mauiproject@floss.social",
        SourceType.MSTDN,
        "https://floss.social/users/mauiproject.rss",
        Category.MAUI,
    ),
    Source(
        "MauiKit blog", 
	SourceType.RSS, 
	"https://mauikit.org/blog/feed/", 
	Category.MAUI,
    ),
    #### Sxmo 
    Source(
        "#sxmo",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/sxmo.rss",
        Category.SXMO,
    ),
    Source(
        "@pocketvj@fosstodon.org",
        SourceType.MSTDN,
        "https://fosstodon.org/users/pocketvj.rss",
        Category.SXMO,
    ),
    #### Sailfish OS
    Source(
        "Community News - Sailfish OS Forum",
        SourceType.RSS,
        "https://forum.sailfishos.org/c/community-news/25.rss",
        Category.SAILFISH_OS,
    ),
    Source(
        "Jolla Blog",
        SourceType.RSS,
        "https://blog.jolla.com/feed/",
        Category.SAILFISH_OS,
    ),
    Source(
        "adampigg",
        SourceType.MSTDN,
        "https://fosstodon.org/users/piggz.rss",
        Category.SAILFISH_OS,
    ),
    Source(
        "adampigg on twitter",
        SourceType.RSS,
        "https://nitter.poast.org/adampigg/rss",
        Category.SAILFISH_OS,
    ),
    Source(
        "flypig's Gecko log",
        SourceType.RSS,
        "https://www.flypig.co.uk/rss.php?list=gecko",
        Category.SAILFISH_OS,
    ),
    Source(
        "#sailfishOS",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/sailfishos.rss",
        Category.SAILFISH_OS,
    ),
    #### Ubuntu Touch
    Source(
        "UBports News",
        SourceType.RSS,
        "https://ubports.com/blog/ubports-news-1/feed",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "Unofficial Ubuntu Touch for PinePhone (Pro)",
        SourceType.RSS,
        "https://gitlab.com/ook37/pinephone-pro-debos/-/tags?format=atom",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "Ubuntu Touch Forums News",
        SourceType.RSS,
        "https://forums.ubports.com/category/41.rss",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "UBports - Development - Issues",
        SourceType.RSS,
        "https://gitlab.com/groups/ubports/development/-/issues.atom",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "UBports - Development - Merge Requests",
        SourceType.RSS,
        "https://gitlab.com/groups/ubports/development/-/merge_requests.atom",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "UBports - Mastodon",
        SourceType.MSTDN,
        "https://mastodon.social/users/ubports.rss",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "fredldotme",
        SourceType.MSTDN,
        "https://mastodon.social/users/fredldotme.rss",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "fredldotme on twitter",
        SourceType.RSS,
        "https://nitter.poast.org/fredldotme/rss",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "rubenlcarneiro on twitter",
        SourceType.RSS,
        "https://nitter.poast.org/rubenlcarneiro/rss",
        Category.UBUNTU_TOUCH,
    ),
    Source(
        "#UbuntuTouch",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/ubuntutouch.rss",
        Category.UBUNTU_TOUCH,
    ),
    #### Maemo Leste
    Source(
        "Maemo Leste",
        SourceType.RSS,
        "https://maemo-leste.github.io/feeds/all.atom.xml",
        Category.MAEMO_LESTE,
    ),
    Source(
        "Maemo Leste on twitter",
        SourceType.RSS,
        "https://nitter.poast.org/maemoleste/rss",
        Category.MAEMO_LESTE,
    ),
    Source(
        "#MaemoLeste",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/maemoleste.rss",
        Category.MAEMO_LESTE,
    ),
    #### Nemo Mobile
    Source(
        "Nemo Mobile UX team",
        SourceType.RSS,
        "https://nemomobile.net/feed.xml",
        Category.NEMO_MOBILE,
    ),
    Source(
        "Jozef Mlich",
        SourceType.RSS,
        "https://blog.mlich.cz/feed/",
        Category.NEMO_MOBILE,
    ),
    Source(
        "Jozef Mlich on Fosstodon",
        SourceType.MSTDN,
        "https://fosstodon.org/@jmlich.rss",
        Category.NEMO_MOBILE,
    ),
    # Source('neochapay on twitter', SourceType.RSS, 'https://nederland.unofficialbird.com/neochapay/rss', Category.NEMO_MOBILE),
    Source(
        "neochapay on twitter",
        SourceType.RSS,
        "https://nitter.poast.org/neochapay/rss",
        Category.NEMO_MOBILE,
    ),
    Source(
        "#NemoMobile",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/nemomobile.rss",
        Category.NEMO_MOBILE,
    ),
    #### Capyloon
    Source(
        "Capyloon",
        SourceType.RSS,
        "https://capyloon.org/releases.xml",
        Category.CAPYLOON,
    ),
    Source(
        "#capyloon",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/capyloon.rss",
        Category.CAPYLOON,
    ),
    #### Matrix
    Source(
        "Matrix.org",
        SourceType.RSS,
        "https://matrix.org/blog/feed",
        Category.MATRIX,
    ),
    Source(
        "nheko (Matrix)",
        SourceType.RSS,
        "https://github.com/Nheko-Reborn/nheko/releases.atom",
        Category.MATRIX,
    ),
    ### Worth listening
    Source(
        "postmarketOS Podcast",
        SourceType.RSS,
        "https://cast.postmarketos.org/feed.rss",
        Category.PODCASTS,
        MimeType.AUDIO,
    ),
    Source(
        "PineTalk Podcast",
        SourceType.RSS,
        "https://www.pine64.org/feed/mp3/",
        Category.PODCASTS,
        MimeType.AUDIO,
    ),
    ### Worth Watching
    Source(
        "RTP Tech Tips",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UChVCEXzi39_YEpUQhqmEFrQ",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Martijn Braam",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC4UXU2ZkeAwEFlLv1Yt1UMQ",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Luca Weiss",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCzTg_UdcLZiC90CMFBhTEQg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "qkall",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCk57oVYvmgb7OVArsebQs6A",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Linux Stuff",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCMuvWFkR4TwQxezCNiGINlA",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "kajuz6",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCo-EI_j0QKSEBlXeB9AF8lA",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Scientific Perspective",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=-wAa5PM9vcqueYlzHVhWvg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Purism",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC64-PJ-yoF7aJ9pIHWEbrTQ",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "PINE64",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCs6A_0Jm21SIvpdKyg9Gmxw",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "danct12",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC_X7lTAbqxqH5bAG3NQO1Yg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "sadfwesv",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCJ7gli38Q2HWD8YV2wVEUyA",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "neochapay",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCTANqaXptsPlXembpTqnH2w",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Maemo Leste",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCgAWiHvWSuJAg5hjk7JYn1w",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "WolfFurProgramming",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCzepblha09x_3Duz3VHSPWg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Bitter Epic Productions",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCjYndYbDLQYoWVO98iMQc1Q",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Avisando",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCb6Ukps5C3-cDOmjNRI0h8A",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Niko",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCZa3sLKv5XHhnAEsDH9-tHg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Continuum Gaming",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCz1YG8Bdpv2zy19NbnPiwnQ",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "UBports",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCDGJ7jdEoOx6_o_GbkjFaBg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Anino Ni Kugi",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCvk902pWNPZhAuh7IenUP0Q",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "CyberPunkEdu",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UCcaXhMb95tY4CrnW9Z89npg",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "totodesbois100",
        SourceType.RSS,
        "https://www.youtube.com/feeds/videos.xml?channel_id=UC2pkf58pylzTU56Nkl8wYGA",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Dylan Van Assche",
        SourceType.RSS,
        "https://tube.tchncs.de/feeds/videos.atom?videoChannelId=3457",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "Phalio",
        SourceType.RSS,
        "https://tube.tchncs.de/feeds/videos.atom?videoChannelId=5823",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "fabrixxm",
        SourceType.RSS,
        "https://peertube.uno/feeds/videos.atom?videoChannelId=8689",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "LINMOB.net",
        SourceType.RSS,
        "https://tilvids.com/feeds/videos.xml?videoChannelId=2944",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22pinephone%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22librem%205%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22ubuntu%20touch%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22ubports%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22SailfishOS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22Sailfish%20OS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22postmarketOS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22postmarket%20OS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22Maemo%20Leste%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22nemo%20mobile%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22droidian%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    Source(
        "YouTube",
        SourceType.RSS,
        "https://searx.zapashcanon.fr/search?q=%22mobian%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss",
        Category.VIDEO,
        MimeType.VIDEO,
    ),
    # misc
    Source("adamd's place", SourceType.RSS, "http://adamd.sdf.org/feed.xml"),
    Source("Aaron's Blog", SourceType.RSS, "https://ahoneybun.github.io/feed.xml"),
    Source(
        "Drew DeVaults blog", SourceType.RSS, "https://drewdevault.com/blog/index.xml"
    ),
    Source("Gamey", SourceType.RSS, "https://gamey.tech/index.xml"),
    Source(
        "Marius Welt", SourceType.RSS, "https://marius.bloggt-in-braunschweig.de/feed/"
    ),
    Source("Anjan Momi Homepage", SourceType.RSS, "https://momi.ca/feed.xml"),
    Source("avery.cafe", SourceType.RSS, "https://avery.cafe/index.xml"),
    Source("Blogger Bust", SourceType.RSS, "https://bloggerbust.ca/index.xml"),
    Source("CAIS", SourceType.RSS, "https://www.cais.de/index.xml"),
    Source("JFs Dev Blog", SourceType.RSS, "https://codingfield.com/index.xml"),
    Source("JustSoup321 Development Blog", SourceType.RSS, "https://justsoup321.codeberg.page/index.xml"),
    Source("Martijn Braam", SourceType.RSS, "https://blog.brixit.nl/rss/"),
    Source("Velvet Reindeer", SourceType.RSS, "https://blanketfort.blog/velvetreindeer/feed/"),
    Source("Julian Fairfax", SourceType.RSS, "https://julianfairfax.ch/feed.xml"),
    Source("Bart Ribbers", SourceType.RSS, "https://fam-ribbers.com/blog/index.xml"),
    Source("David Heidelberg", SourceType.RSS, "https://ixit.cz/blog/feed"),
    Source("Luigi311", SourceType.RSS, "https://blog.luigi311.com/atom.xml"),
    Source("Jonas Dressler", SourceType.RSS, "https://blogs.gnome.org/jdressler/feed/"),
    Source(
        "Jan Wagemakers", SourceType.RSS, "https://www.janwagemakers.be/jekyll/feed.xml"
    ),
    Source("Caleb Connnolly", SourceType.RSS, "https://connolly.tech/index.xml"),
    Source("craftyguy", SourceType.RSS, "https://blog.craftyguy.net/atom.xml"),
    Source(
        "eighty-twenty news", SourceType.RSS, "https://eighty-twenty.org/index.atom"
    ),
    Source(
        "FOSSingularity", SourceType.RSS, "https://fossingularity.wordpress.com/feed/"
    ),
    Source("FuriLabs", SourceType.RSS, "https://furilabs.com/feed/"),
    Source("GabMuss Dev Log", SourceType.RSS, "https://gabmus.org/index.xml"),
    Source("Hamblingreens Blog", SourceType.RSS, "https://hamblingreen.com/atom.xml"),
    Source(
        "Pablo Yoyoista", SourceType.RSS, "https://blogs.gnome.org/pabloyoyoista/feed/"
    ),
    Source("James Westmans blog", SourceType.RSS, "https://www.jwestman.net/feed.xml"),
    Source("Jason123santas blog", SourceType.RSS, "https://jasonsanta.xyz/rss.xml"),
    Source("LinuxPhoneApps.org", SourceType.RSS, "https://linuxphoneapps.org/blog/atom.xml"),
    Source(
        "Lemmy - linuxphones",
        SourceType.RSS,
        "https://lemmy.ml/feeds/c/linuxphones.xml?sort=Active",
        Category.WORTHNOTING,
    ),
    Source(
        "#LinuxMobile",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/linuxmobile.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "#MobileLinux",
        SourceType.MSTDN,
        "https://fosstodon.org/tags/mobilelinux.rss",
        Category.WORTHNOTING,
    ),
    Source(
        "@luigi311", 
        SourceType.MSTDN, 
        "https://mastodon.social/users/luigi311.rss",
        Category.WORTHNOTING,
    ),
    Source("LINux on MOBile", SourceType.RSS, "https://linmob.net/feed.xml"),
    Source(
        "linux phone on Bacardis cave",
        SourceType.RSS,
        "https://bacardi55.io/categories/linux-phone/index.xml",
    ),
    Source("Neil Brown", SourceType.RSS, "https://neilzone.co.uk/feed/rss"),
    Source("Phoronix", SourceType.RSS, "https://www.phoronix.com/rss.php"),
    Source("PINE64", SourceType.RSS, "https://www.pine64.org/feed/"),
    Source("PineGuild", SourceType.RSS, "https://pineguild.com/feed/"),
    Source(
        "project-insanity.org",
        SourceType.RSS,
        "https://blog.project-insanity.org/feed/",
    ),
    Source("Purism", SourceType.RSS, "https://puri.sm/feed/"),
    Source("amosbbatto", SourceType.RSS, "https://amosbbatto.wordpress.com/feed/"),
    Source(
        "fossphones.com",
        SourceType.RSS,
        "https://fossphones.com/feed.xml",
    ),
    Source("Spaetzblog", SourceType.RSS, "https://sspaeth.de/feed/"),
    Source(
        "TuxPhones - Linux phones, tablets and portable devices",
        SourceType.RSS,
        "https://tuxphones.com/rss/",
    ),
    Source("xnux.eu", SourceType.RSS, "https://xnux.eu/rss.xml"),
    Source(
        "megis PinePhone Development Log",
        SourceType.MEGI,
        "https://xnux.eu/log/rss.xml",
    ),
    Source("lowendlibre", SourceType.RSS, "https://lowendlibre.github.io/index.xml"),
]

entries = []


def remove_html_tags(text):
    clean = re.compile("<.*?>")
    return re.sub(clean, "", text)


def clean_url(url):
    parsed_url = urllib.parse.urlparse(url)
    query_params = urllib.parse.parse_qs(parsed_url.query)

    # Remove parameters that start with 'utm_'
    query_params = {k: v for k, v in query_params.items() if not k.startswith("utm_")}

    # Rebuild the query string without 'utm_' parameters
    cleaned_query = urllib.parse.urlencode(query_params, doseq=True)

    # Reconstruct the URL with the cleaned query parameters
    cleaned_url = parsed_url._replace(query=cleaned_query).geturl()
    return cleaned_url


def filter_video_category(entry, feed_entry):
    title = feed_entry.title.lower() if "title" in feed_entry else ""
    full_content = ""
    if "content" in feed_entry:
        for part in feed_entry.content:
            full_content = full_content + part.value

    return not any(
        keyword.lower() in title or keyword.lower() in full_content.lower()
        for keyword in video_filter_keywords
    )


def determine_category(entry, feed_entry, source):
    if entry.category == Category.NONE.value:
        full_content = ""
        if "content" in feed_entry:
            for part in feed_entry.content:
                full_content = full_content + part.value
        for category, words in keywords.items():
            title = feed_entry.title.lower() if "title" in feed_entry else ""
            if any(w in title for w in words) or any(
                w in full_content.lower() for w in words
            ):
                entry.category = category.value
                break
        if entry.category == Category.NONE.value:
            entry.category = Category.MISC.value
    return entry


def parse_sources(begin, end):
    for source in sources:
        if source.type == SourceType.RSS:
            feed = feedparser.parse(source.url)
            for feed_entry in feed.entries:
                timestamp = begin
                try:
                    if "published_parsed" in feed_entry and feed_entry.published_parsed:
                        timestamp = datetime.fromtimestamp(
                            mktime(feed_entry.published_parsed)
                        )
                    elif "updated_parsed" in feed_entry and feed_entry.updated_parsed:
                        timestamp = datetime.fromtimestamp(
                            mktime(feed_entry.updated_parsed)
                        )
                except ValueError:
                    pass
                if timestamp < begin or timestamp > end:
                    continue
                # if no title set, fallback to link
                if not "title" in feed_entry:
                    feed_entry.title = feed_entry.link
                # Clean utm-crap
                post_url = clean_url(feed_entry.link)

                # Check if the 'link' attribute exists, set a default value if it doesn't
                if "link" not in feed_entry:
                    feed_entry.link = "error: bad feed"

                entry = Entry(
                    feed_entry.title,
                    post_url,
                    timestamp,
                    source.name,
                    source.category.value,
                )
                # determine category based on keywords if none set
                entry = determine_category(entry, feed_entry, source)
                # Apply video category filter if necessary
                if (
                    entry.category == Category.VIDEO.value
                    and not filter_video_category(entry, feed_entry)
                ):
                    continue
                entries.append(entry)

        if source.type == SourceType.MSTDN:
            feed = feedparser.parse(source.url)
            for feed_entry in feed.entries:
                timestamp = begin
                try:
                    if "published_parsed" in feed_entry and feed_entry.published_parsed:
                        timestamp = datetime.fromtimestamp(
                            mktime(feed_entry.published_parsed)
                        )
                    elif "updated_parsed" in feed_entry and feed_entry.updated_parsed:
                        timestamp = datetime.fromtimestamp(
                            mktime(feed_entry.updated_parsed)
                        )
                except ValueError:
                    pass
                if timestamp < begin or timestamp > end:
                    continue

                clean_summary = remove_html_tags(feed_entry.summary)

                entry = Entry(
                    clean_summary,
                    feed_entry.link,
                    timestamp,
                    source.name,
                    source.category.value,
                )
                # determine category based on keywords if none set
                entry = determine_category(entry, feed_entry, source)
                entries.append(entry)

        if source.type == SourceType.MEGI:
            feed = feedparser.parse(source.url)
            for feed_entry in feed.entries:
                # Extract date from title
                date_match = re.match(r"(\d{4}–\d{2}–\d{2}):", feed_entry.title)
                if date_match:
                    timestamp = datetime.strptime(date_match.group(1), "%Y–%m–%d")
                else:
                    continue

                if timestamp < begin or timestamp > end:
                    continue

                clean_title = feed_entry.title[len(date_match.group(0)) :].strip()

                entry = Entry(
                    clean_title,
                    feed_entry.link,
                    timestamp,
                    source.name,
                    source.category.value,
                )
                # determine category based on keywords if none set
                entry = determine_category(entry, feed_entry, source)
                entries.append(entry)


def main():
    global entries

    parser = argparse.ArgumentParser(description="Aggregate sources in one file.")
    parser.add_argument(
        "--output", help="output filename (default: entries.csv)", default="entries.csv"
    )
    parser.add_argument(
        "--begin",
        type=lambda s: datetime.strptime(s, "%Y-%m-%d"),
        help="begin date (YYYY-MM-DD)",
    )
    parser.add_argument(
        "--end",
        type=lambda s: datetime.strptime(s, "%Y-%m-%d"),
        help="end date (YYYY-MM-DD)",
    )

    args = parser.parse_args()

    parse_sources(args.begin, args.end)

    with open(args.output, "w") as f:
        w = DataclassWriter(f, entries, Entry)
        w.write()


if __name__ == "__main__":
    main()
